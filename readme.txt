﻿Build tools:
Apache Maven 3.5.0
http://apache.forthnet.gr/maven/maven-3/3.5.0/binaries/apache-maven-3.5.0-bin.tar.gz

Server/Container:
Apache TomEE Plus 7.0.2
http://repo.maven.apache.org/maven2/org/apache/tomee/apache-tomee/7.0.2/apache-tomee-7.0.2-plus.tar.gz

or drop in war version using a docker platform
Apache TomEE Plus 7.0.2 drop-in war
http://repo.maven.apache.org/maven2/org/apache/tomee/tomee-plus-webapp/7.0.2/tomee-plus-webapp-7.0.2.war
(Use drop-in war file in docker)
See https://github.com/tomitribe/docker-tomee

Using Maven

Prerequisites:
• Set “JAVA_HOME” path variable
• Add Maven to system’s path variables: 
<installation path>/apache-maven-3.5.0/bin/

Clean and build webapp usage:
mvn clean install (inside application root path)

Running app using tomee-embedded server:
mvn tomee-embedded:run (inside application root path)


