package org.atlas.applications.modules.jsonapi;

import java.util.List;
import javax.ejb.Local;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.JSONApiLink;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationContent;

/**
 * Defines methods to create {@link JSONApiDocument} objects from single and/or collection POJO instances.
 * @author ic
 * @param <T> the type of data to read from
 * @param <D> the type of data  to transform to
 */
@Local
public interface IJsonApiDocumentCreator<T extends Object, D extends Object>
{             
    /**
     * Returns the custom-defined self link string for given application id and version.
     * @param serverContextPath
     * @param appId
     * @param versionDownloadString
     * @return 
     */
    public String getAppVersionSelfUrl(String serverContextPath, long appId, String versionDownloadString);
    
    /**
     * Creates a single-resource JSONApiDocument with given Application and ApplicationContent instances.
     * @param application
     * @param content
     * @param toplevelselfUr The string to use as top-level resource links object.
     * @return 
     */
    @Deprecated
    public JSONApiDocument asSingle(Application application, ApplicationContent content, String toplevelselfUr);
    
    /**
     * Creates a single-resource JSONApiDocument with given Application and ApplicationContent collection instances.
     * @param application
     * @param contents
     * @param toplevelselfUr The string to use as top-level resource links object.
     * @return 
     */
    @Deprecated
    public JSONApiDocument asSingle(Application application, List<ApplicationContent> contents, String toplevelselfUr);
    
    /**
     * Creates a many-resources JSONApiDocument with given Application collection.
     * @param applications
     * @param toplevelselfUr The string to use as top-level resource links object.
     * @return 
     */
    @Deprecated
    public JSONApiDocument asArray(List<Application> applications, String toplevelselfUr);
    
    /**
     * Creates a many-resources JSONApiDocument with given Application collection.
     * @param applications
     * @param topLevelLinks
     * @return 
     */
    @Deprecated
    public JSONApiDocument asArray(List<Application> applications, JSONApiLink topLevelLinks);    
    
    /**
     * Creates a single resource JSONApiDocument with given instance.
     * @param object the source data
     * @param topLevelLinks top-level links field to include
     * @return 
     */
    public JSONApiDocument createDocument(T object, JSONApiLink topLevelLinks);
    
    /**
     * Creates a many-resources JSONApiDocument with given collection.
     * @param object the source data
     * @param topLevelLinks top-level links field to include
     * @return 
     */
    public JSONApiDocument createDocument(List<T> object, JSONApiLink topLevelLinks);
    
    public org.joko.core.jsonapi.JSONApiDocument createDocumentExt(T object, org.joko.core.jsonapi.JSONApiLink topLevelLinks);
}
