/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.jsonapi;

import java.util.List;
import javax.ejb.Local;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.extensions.JSONApiIncludeType;
import org.atlas.applications.core.jsonapi.helpers.TopLevelLinksProperties;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;

/**
 * Abstract base interface for JSONApiDocument creators.
 * @author ic
 * @param <E> the type of source object to read from
 */
@Local
public abstract interface IBaseJSONApiCreator<E extends Object>
{
    public abstract <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationIncludingData(E source, Class<T> targetDTOClass, JSONApiIncludeType includeType) throws InstantiationException, IllegalAccessException;
    
    public abstract <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationWithVersion(E source, String version, Class<T> targetDTOClass) throws InstantiationException, IllegalAccessException;
    
    public abstract <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationsWithLatestVersion(List<E> source, Class<T> targetDTOClass, TopLevelLinksProperties properties) throws InstantiationException, IllegalAccessException;
    
    public abstract <T extends IJsonApiDocumentCreator> JSONApiDocument setStatus(E source, String version, ApplicationStatus status, Class<T> targetDTOClass) throws InstantiationException, IllegalAccessException;
    
    public abstract <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationOnStatusSetIncludingVersions(E source, String version, boolean includeDeletedVersion, Class<T> targetDTOClass) throws InstantiationException, IllegalAccessException;    
    
}
