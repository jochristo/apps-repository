package org.atlas.applications.modules.jsonapi;

import java.util.List;
import javax.ejb.Local;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.extensions.JSONApiIncludeType;
import org.atlas.applications.core.jsonapi.helpers.TopLevelLinksProperties;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;

/**
 * Creates {@link JSONApiDocument} instances from {@link Application} objects.
 * @author ic
 */
@Local
public interface IApplicationJSONApiCreator extends IBaseJSONApiCreator<Application>
{
    @Override
    public <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationIncludingData(Application application, Class<T> targetDTOClass, JSONApiIncludeType includeType) throws InstantiationException, IllegalAccessException;    
    
    public <T extends IJsonApiDocumentCreator> org.joko.core.jsonapi.JSONApiDocument getDataDocument(Application application, Class<T> targetDTOClass, JSONApiIncludeType includeType) throws InstantiationException, IllegalAccessException;    

    @Override
    public <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationWithVersion(Application application, String version, Class<T> targetDTOClass) throws InstantiationException, IllegalAccessException;    

    @Override
    public <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationsWithLatestVersion(List<Application> applications, Class<T> targetDTOClass, TopLevelLinksProperties properties) throws InstantiationException, IllegalAccessException;    

    @Override
    public <T extends IJsonApiDocumentCreator> JSONApiDocument setStatus(Application application, String version, ApplicationStatus status, Class<T> targetDTOClass) throws InstantiationException, IllegalAccessException;    

    @Override
    public <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationOnStatusSetIncludingVersions(Application application, String version, boolean includeDeletedVersion, Class<T> targetDTOClass) throws InstantiationException, IllegalAccessException;    
    
}
