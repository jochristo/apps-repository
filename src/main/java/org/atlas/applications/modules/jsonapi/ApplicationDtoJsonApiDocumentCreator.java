/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.jsonapi;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.Response;
import org.atlas.applications.api.ApiEndpoint;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.JSONApiLink;
import org.atlas.applications.core.jsonapi.extensions.JSONApiLinkNoPaging;
import org.atlas.applications.core.utilities.Utilities;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationContent;
import org.atlas.applications.modules.repo.domain.ApplicationTransaction;
import org.atlas.applications.modules.repo.domain.SemVerComparator;
import org.atlas.applications.modules.repo.dto.ApplicationContentDto;
import org.atlas.applications.modules.repo.dto.ApplicationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates JSONApiDocument objects using custom data transfer objects.
 * Used in API V1. 
 * @author ic
 */
@LocalBean
@Singleton
public class ApplicationDtoJsonApiDocumentCreator implements IJsonApiDocumentCreator<Application, ApplicationDto>
{       
    private static final Logger logger = LoggerFactory.getLogger(ApplicationDtoJsonApiDocumentCreator.class);         
    
    protected Object getContextParamValue(String paramName)
    {
        InitialContext initialContext;
        try {
            initialContext = new javax.naming.InitialContext();
            Object value = (String) initialContext.lookup("java:comp/env/" + paramName);
            return value;
        }
        catch(NamingException ex)
        {
            logger.error(ApplicationDtoJsonApiDocumentCreator.class.getName(), ex);
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }    
    
    @Override
    public String getAppVersionSelfUrl(String serverContextPath, long appId, String version)
    {
        StringBuilder sb = new StringBuilder(serverContextPath);
        sb.append(ApiEndpoint.API_PATH_V1);
        sb.append(ApiEndpoint.APPLICATIONS);
        sb.append("/");
        sb.append(appId);
        sb.append("/download?version=");        
        sb.append(version);
        return sb.toString();              
    }         

    @Override
    public JSONApiDocument asSingle(Application application, ApplicationContent content, String toplevelselfUr)
    {
        // construct dto
        ApplicationDto appDto;        
        
        // return empty doc
        if (application == null){           
            appDto = null;
            return new JSONApiDocument(appDto);             
        }
        
        appDto = new ApplicationDto();
        appDto.setId(application.getId());
        appDto.setRepository(application.getRepository());
        appDto.setAppname(application.getName());
        appDto.setLanguage(application.getLanguage());
        appDto.setStatus(application.getApplicationStatus());

        // no versions found, return empty relationships
        if (Utilities.isEmpty(content)) {
            JSONApiLinkNoPaging topLevelLinks = new JSONApiLinkNoPaging();
            topLevelLinks.setSelf(toplevelselfUr);
            JSONApiDocument document = new JSONApiDocument(appDto);
            document.setLinks(topLevelLinks);
            return document;
        }
            
        appDto.setVersion(content.getVersion());
        appDto.setChecksum(content.getChecksum());
        appDto.setMimeType(content.getMimeType().getMime());
        appDto.setMain(content.getMain());
        appDto.setVersionStatus(content.getStatus());

        // set self href
        JSONApiLinkNoPaging topLevelLinks = new JSONApiLinkNoPaging();
        topLevelLinks.setSelf(toplevelselfUr);
        appDto.setLink(content.getLink());            
        JSONApiDocument document = new JSONApiDocument(appDto);                
        document.setLinks(topLevelLinks);
        return document; 
    }        

    @Override
    public JSONApiDocument asSingle(Application application, List<ApplicationContent> contents, String toplevelselfUr)
    {
        // construct dto
        ApplicationDto appDto;      
        
        // initialize relationships list
        List<ApplicationContentDto> contentDtos = new ArrayList();        
        
        // return empty doc
        if (application == null){           
            appDto = null;
            return new JSONApiDocument(appDto);             
        }
        
        appDto = new ApplicationDto();
        appDto.setId(application.getId());
        appDto.setRepository(application.getRepository());
        appDto.setAppname(application.getName());
        appDto.setLanguage(application.getLanguage());
        appDto.setStatus(application.getApplicationStatus());

        // no versions found, return empty relationships
        if (contents.isEmpty()) {
            JSONApiLinkNoPaging topLevelLinks = new JSONApiLinkNoPaging();
            topLevelLinks.setSelf(toplevelselfUr);
            JSONApiDocument document = new JSONApiDocument(appDto);
            document.setLinks(topLevelLinks);
            return document;
        }
        
        // sort content by version: latest first
        contents.sort(new SemVerComparator());
        // include relationships
        contents.forEach((item) -> {
            ApplicationContentDto acd = new ApplicationContentDto();
            acd.setId(String.valueOf(item.getId()));            
            acd.setIsExecutable(item.getIsExecutable());
            acd.setSize(item.getSize());
            acd.setStatus(item.getStatus());
            acd.setFilename(item.getFilename());
            acd.setVersion(item.getVersion());
            acd.setMain(item.getMain());
            acd.setMimeType(item.getMimeType().getMime());
            acd.setChecksum(item.getChecksum());

            // create resource-level links per version item...                                                            
            acd.setLink(item.getLink());

            // add to list
            contentDtos.add(acd);       
        });
        
        // set self href
        JSONApiLinkNoPaging topLevelLinks = new JSONApiLinkNoPaging();
        topLevelLinks.setSelf(toplevelselfUr);        
        JSONApiDocument document = new JSONApiDocument(appDto);        
        document.appendIncludedDataCollection(contentDtos);
        document.setLinks(topLevelLinks);
        return document; 
    }        

    @Override
    public JSONApiDocument asArray(List<Application> applications, String toplevelselfUr)
    {
        List<ApplicationDto> applicationDtos = new ArrayList();
        JSONApiDocument document = new JSONApiDocument(applicationDtos); // initialize createDocument
        
        // return null data
        if(Utilities.isEmpty(applications)){
            JSONApiDocument doc = new JSONApiDocument();
            doc.setData(null);
            return doc;
        }
        applications.forEach((application) -> {
                        
            ApplicationDto appDto = new ApplicationDto();
            appDto.setId(application.getId());
            appDto.setRepository(application.getRepository());
            appDto.setAppname(application.getName());            
            appDto.setLanguage(application.getLanguage()); 
            appDto.setStatus(application.getApplicationStatus());
            
            // get properties of latest version if exists one            
            List<ApplicationContent> contents = application.getApplicationContents(); 
            if (contents.isEmpty() == false) {
                
                // sort content by version: latest first
                contents.sort(new SemVerComparator());
                List<ApplicationContent> list = new ArrayList();
                list.addAll(contents);
                list.forEach((content)->{
                    appDto.setVersion(content.getVersion());
                    appDto.setChecksum(content.getChecksum());
                    appDto.setMain(content.getMain());
                    appDto.setMimeType(content.getMimeType().getMime());
                    appDto.setVersionStatus(content.getStatus());

                    // set self href
                    appDto.setLink(content.getLink());
                });
            }
            
            //add to list
            applicationDtos.add(appDto);
        });      
        
        JSONApiLink topLevelLinks = new JSONApiLink();
        topLevelLinks.setSelf(toplevelselfUr);        
        document = new JSONApiDocument(applicationDtos);
        document.setLinks(topLevelLinks);
        return document;   
    }  

    @Override
    public JSONApiDocument asArray(List<Application> applications, JSONApiLink topLevelLinks) {
        List<ApplicationDto> applicationDtos = new ArrayList();
        JSONApiDocument document = new JSONApiDocument(applicationDtos); // initialize createDocument
        
        // return null data
        if(Utilities.isEmpty(applications)){
            JSONApiDocument doc = new JSONApiDocument();
            doc.setData(null);
            return doc;
        }
        applications.forEach((application) -> {
                        
            ApplicationDto appDto = new ApplicationDto();
            appDto.setId(application.getId());
            appDto.setRepository(application.getRepository());
            appDto.setAppname(application.getName());            
            appDto.setLanguage(application.getLanguage()); 
            appDto.setStatus(application.getApplicationStatus());
            
            // get properties of latest version if exists one            
            List<ApplicationContent> contents = application.getApplicationContents(); 
            if (contents.isEmpty() == false) {
                
                // sort content by version: latest first
                contents.sort(new SemVerComparator());
                List<ApplicationContent> list = new ArrayList();
                list.addAll(contents);
                list.forEach((content)->{
                    appDto.setVersion(content.getVersion());
                    appDto.setChecksum(content.getChecksum());
                    appDto.setMain(content.getMain());
                    appDto.setMimeType(content.getMimeType().getMime());
                    appDto.setVersionStatus(content.getStatus());

                    // set self href
                    appDto.setLink(content.getLink());
                });
            }
            
            //add to list
            applicationDtos.add(appDto);
        });             

        document = new JSONApiDocument(applicationDtos);
        document.setLinks(topLevelLinks);
        return document;  
    }

    @Override
    public JSONApiDocument createDocument(Application application, JSONApiLink links)
    {
        // construct dto
        ApplicationDto appDto;      
        
        // initialize relationships list
        List<ApplicationContentDto> contentDtos = new ArrayList();        
        
        // return empty doc
        if (application == null){           
            appDto = null;
            return new JSONApiDocument(appDto);             
        }
        
        appDto = new ApplicationDto();
        appDto.setId(application.getId());
        appDto.setRepository(application.getRepository());
        appDto.setAppname(application.getName());
        appDto.setLanguage(application.getLanguage());
        appDto.setStatus(application.getApplicationStatus());               
        
        // set toplevel links
        appDto.setLink(links);

        // get relationships data if any
        List<ApplicationContent> contents = application.getApplicationContents();               
        List<ApplicationTransaction> transactions = application.getApplicationTransactions();        
        
        // empty lists if null        
        if(contents == null)
        {
            contents = new ArrayList();
        }
        if(transactions == null)
        {
            transactions = new ArrayList();
        }
        
        // empty documents
        if (contents.isEmpty() && transactions.isEmpty()) {            
            appDto.setLink(links);
            return new JSONApiDocument(appDto);                        
        }
        
        // attach contents
        if (contents.isEmpty() == false)
        {
            // sort content by version: latest first
            contents.sort(new SemVerComparator());

            if(contents.size() > 1)
            {
                // include relationships
                contents.forEach((item) -> {
                    ApplicationContentDto acd = new ApplicationContentDto();
                    acd.setId(String.valueOf(item.getId()));            
                    acd.setIsExecutable(item.getIsExecutable());
                    acd.setSize(item.getSize());
                    acd.setStatus(item.getStatus());
                    acd.setFilename(item.getFilename());
                    acd.setVersion(item.getVersion());
                    acd.setMain(item.getMain());
                    acd.setMimeType(item.getMimeType().getMime());
                    acd.setChecksum(item.getChecksum());

                    // create resource-level links per version item...                                                            
                    acd.setLink(item.getLink());

                    // add to list
                    contentDtos.add(acd);       
                });

                // set contents relationships data
                appDto.setContents(contentDtos);
            }
            else
            {
                //get single object
                ApplicationContent content = contents.get(0);                
                appDto.setVersion(content.getVersion());
                appDto.setChecksum(content.getChecksum());
                appDto.setMain(content.getMain());
                appDto.setMimeType(content.getMimeType().getMime());
                appDto.setVersionStatus(content.getStatus());                
                appDto.setSize(content.getSize());                
                
                // set self href
                appDto.setLink(content.getLink());              
                
                // clear relationships data
                appDto.setContents(new ArrayList());                
            }
        }
        
        // attach transactions
        if(transactions.isEmpty() == false)
        {
            // set transactions relationships data
            appDto.setApplicationTransactions(transactions);
        }
                
        // set self href
        appDto.setLink(links);
        return new JSONApiDocument(appDto); 
    }

    @Override
    public JSONApiDocument createDocument(List<Application> applications, JSONApiLink links)
    {
        List<ApplicationDto> applicationDtos = new ArrayList();
        
        // return null data
        if(Utilities.isEmpty(applications)){
            JSONApiDocument doc = new JSONApiDocument(applicationDtos);
            //doc.setData(null);
            return doc;
        }
        applications.forEach((application) -> {
                        
            ApplicationDto appDto = new ApplicationDto();
            appDto.setId(application.getId());
            appDto.setRepository(application.getRepository());
            appDto.setAppname(application.getName());            
            appDto.setLanguage(application.getLanguage()); 
            appDto.setStatus(application.getApplicationStatus());
            
            // get properties of latest version if exists one            
            List<ApplicationContent> contents = application.getApplicationContents(); 
            if (contents.isEmpty() == false) {
                
                // sort content by version: latest first
                contents.sort(new SemVerComparator());
                List<ApplicationContent> list = new ArrayList();
                list.addAll(contents);
                list.forEach((content)->{
                    appDto.setVersion(content.getVersion());
                    appDto.setChecksum(content.getChecksum());
                    appDto.setMain(content.getMain());
                    appDto.setMimeType(content.getMimeType().getMime());
                    appDto.setVersionStatus(content.getStatus());

                    // set self href
                    appDto.setLink(content.getLink());
                });
            }
            
            //add to list
            applicationDtos.add(appDto);
        });             

        JSONApiDocument document = new JSONApiDocument(applicationDtos);
        document.setLinks(links);
        return document; 
    }

    @Override
    public org.joko.core.jsonapi.JSONApiDocument createDocumentExt(Application application, org.joko.core.jsonapi.JSONApiLink links)
    {
        // construct dto
        ApplicationDto appDto;      
        
        // initialize relationships list
        List<ApplicationContentDto> contentDtos = new ArrayList();        
        
        // return empty doc
        if (application == null){           
            appDto = null;
            return new org.joko.core.jsonapi.JSONApiDocument(appDto);             
        }
        
        appDto = new ApplicationDto();
        appDto.setId(application.getId());
        appDto.setRepository(application.getRepository());
        appDto.setAppname(application.getName());
        appDto.setLanguage(application.getLanguage());
        appDto.setStatus(application.getApplicationStatus());               
        
        // set toplevel links
        //appDto.setLink(links);

        // get relationships data if any
        List<ApplicationContent> contents = application.getApplicationContents();               
        List<ApplicationTransaction> transactions = application.getApplicationTransactions();        
        
        // empty lists if null        
        if(contents == null)
        {
            contents = new ArrayList();
        }
        if(transactions == null)
        {
            transactions = new ArrayList();
        }
        
        // empty documents
        if (contents.isEmpty() && transactions.isEmpty()) {            
            //appDto.setLink(links);
            return new org.joko.core.jsonapi.JSONApiDocument(appDto);                        
        }
        
        // attach contents
        if (contents.isEmpty() == false)
        {
            // sort content by version: latest first
            contents.sort(new SemVerComparator());

            if(contents.size() > 1)
            {
                // include relationships
                contents.forEach((item) -> {
                    ApplicationContentDto acd = new ApplicationContentDto();
                    acd.setId(String.valueOf(item.getId()));            
                    acd.setIsExecutable(item.getIsExecutable());
                    acd.setSize(item.getSize());
                    acd.setStatus(item.getStatus());
                    acd.setFilename(item.getFilename());
                    acd.setVersion(item.getVersion());
                    acd.setMain(item.getMain());
                    acd.setMimeType(item.getMimeType().getMime());
                    acd.setChecksum(item.getChecksum());

                    // create resource-level links per version item...                                                            
                    acd.setLink(item.getLink());

                    // add to list
                    contentDtos.add(acd);       
                });

                // set contents relationships data
                appDto.setContents(contentDtos);
            }
            else
            {
                //get single object
                ApplicationContent content = contents.get(0);                
                appDto.setVersion(content.getVersion());
                appDto.setChecksum(content.getChecksum());
                appDto.setMain(content.getMain());
                appDto.setMimeType(content.getMimeType().getMime());
                appDto.setVersionStatus(content.getStatus());                
                appDto.setSize(content.getSize());                
                
                // set self href
                appDto.setLink(content.getLink());              
                
                // clear relationships data
                appDto.setContents(new ArrayList());                
            }
        }
        
        // attach transactions
        if(transactions.isEmpty() == false)
        {
            // set transactions relationships data
            appDto.setApplicationTransactions(transactions);
        }
                
        // set self href
        //appDto.setLink(links);
        return new org.joko.core.jsonapi.JSONApiDocument(appDto, new org.joko.core.jsonapi.JSONApiLink());
    }
    
    
}
