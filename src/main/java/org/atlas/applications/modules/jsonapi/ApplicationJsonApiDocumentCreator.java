/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.jsonapi;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.Response;
import org.atlas.applications.api.ApiEndpoint;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.JSONApiLink;
import org.atlas.applications.core.utilities.Utilities;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationContent;
import org.atlas.applications.modules.repo.domain.SemVerComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates JSONApiDocument objects using JPA models as data transfer objects.
 * Used in API V2.
 * @author ic
 */
@LocalBean
@Singleton
public class ApplicationJsonApiDocumentCreator implements IJsonApiDocumentCreator<Application,Application>
{ 
    private static final Logger logger = LoggerFactory.getLogger(ApplicationJsonApiDocumentCreator.class);                 
       
    protected Object getContextParamValue(String paramName)
    {
        InitialContext initialContext;
        try {
            initialContext = new javax.naming.InitialContext();
            Object value = (String) initialContext.lookup("java:comp/env/" + paramName);
            return value;
        }
        catch(NamingException ex)
        {
            logger.error(ApplicationJsonApiDocumentCreator.class.getName(), ex);
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }      
    
    @Override
    public String getAppVersionSelfUrl(String serverContextPath, long appId, String version)
    {
        StringBuilder sb = new StringBuilder(serverContextPath);
        sb.append(ApiEndpoint.API_PATH_V2);
        sb.append(ApiEndpoint.APPLICATIONS);
        sb.append("/");
        sb.append(appId);
        sb.append("/download?version=");        
        sb.append(version);
        return sb.toString();             
    }      

    @Override
    public JSONApiDocument asSingle(Application application, ApplicationContent content, String toplevelselfUr)
    {
        // return empty doc
        if (application == null){                       
            return new JSONApiDocument(application);             
        }
        
        // no versions found, return empty relationships
        if (Utilities.isEmpty(content)) {
            JSONApiLink topLevelLinks = new JSONApiLink();
            topLevelLinks.setSelf(toplevelselfUr);
            JSONApiDocument document = new JSONApiDocument(application);
            document.setLinks(topLevelLinks);
            return document;
        }        
        
        // set relationships list
        List<ApplicationContent> contents = new ArrayList();
        contents.add(content);
        application.setApplicationContents(contents);        
        
        // set self href
        JSONApiLink topLevelLinks = new JSONApiLink();
        topLevelLinks.setSelf(toplevelselfUr);        
        JSONApiDocument document = new JSONApiDocument(application);        
        document.appendIncludedData(content);
        document.setLinks(topLevelLinks);        
        return document; 
    }

    @Override
    public JSONApiDocument asSingle(Application application, List<ApplicationContent> contents, String toplevelselfUr)
    {
        // return empty doc
        if (application == null){                       
            return new JSONApiDocument(application);             
        }
        
        // no versions found, return empty relationships
        if (Utilities.isEmpty(contents)) {
            JSONApiLink topLevelLinks = new JSONApiLink();
            topLevelLinks.setSelf(toplevelselfUr);
            JSONApiDocument document = new JSONApiDocument(application);
            document.setLinks(topLevelLinks);
            return document;
        }         
        
        // sort content by version: latest first
        contents.sort(new SemVerComparator());        
        
        // set relationships list
        application.setApplicationContents(contents);
        
        // set self href
        JSONApiLink topLevelLinks = new JSONApiLink();
        topLevelLinks.setSelf(toplevelselfUr);           
        JSONApiDocument document = new JSONApiDocument(application);        
        document.appendIncludedDataCollection(contents);
        document.setLinks(topLevelLinks);        
        return document;   
    }

    @Override
    public JSONApiDocument asArray(List<Application> applications, String toplevelselfUr)
    {
        // return empty doc
        if (Utilities.isEmpty(applications)){                       
            return new JSONApiDocument(applications);             
        }
        
        // include list
        List<ApplicationContent> includes = new ArrayList();
        
        // iterate
        applications.forEach((app)->
        {     
            List<ApplicationContent> contents = app.getApplicationContents();
            if(Utilities.isEmpty(contents) == false)
            {
                // sort content by version: latest first
                contents.sort(new SemVerComparator());
                List<ApplicationContent> list = new ArrayList();
                list.addAll(contents);
                list.forEach((content)->
                {
                    // add to includes list
                    includes.add(content);
                });
            }  
        });       
        
        // set top level self href
        JSONApiLink topLevelLinks = new JSONApiLink();
        topLevelLinks.setSelf(toplevelselfUr);            
        JSONApiDocument document = new JSONApiDocument(applications);         
        document.appendIncludedDataCollection(includes);
        document.setLinks(topLevelLinks);
        return document; 
    }

    @Override
    public JSONApiDocument asArray(List<Application> applications, JSONApiLink topLevelLinks) {
        if (Utilities.isEmpty(applications)){                       
            return new JSONApiDocument(applications);             
        }
        
        // include list
        List<ApplicationContent> includes = new ArrayList();
        
        // iterate
        applications.forEach((app)->
        {     
            List<ApplicationContent> contents = app.getApplicationContents();
            if(Utilities.isEmpty(contents) == false)
            {
                // sort content by version: latest first
                contents.sort(new SemVerComparator());
                List<ApplicationContent> list = new ArrayList();
                list.addAll(contents);
                list.forEach((content)->
                {
                    // add to includes list
                    includes.add(content);
                });
            }  
        });               
      
        JSONApiDocument document = new JSONApiDocument(applications);         
        document.appendIncludedDataCollection(includes);
        document.setLinks(topLevelLinks);
        return document; 
    }

    @Override
    public JSONApiDocument createDocument(Application application, JSONApiLink links) {
        JSONApiDocument document = new JSONApiDocument(application);
        if(links != null){
            document.setLinks(links);
        }
        return document;
    }

    @Override
    public JSONApiDocument createDocument(List<Application> applications, JSONApiLink links) {
        JSONApiDocument document = new JSONApiDocument(applications);
        if(links != null){
            document.setLinks(links);
        }
        return document;
    }

    @Override
    public org.joko.core.jsonapi.JSONApiDocument createDocumentExt(Application object, org.joko.core.jsonapi.JSONApiLink topLevelLinks) {
        org.joko.core.jsonapi.JSONApiDocument document = new org.joko.core.jsonapi.JSONApiDocument(object);
        if(topLevelLinks != null){
            document.setLinks(topLevelLinks);
        }
        return document;
    }
    
    
}
