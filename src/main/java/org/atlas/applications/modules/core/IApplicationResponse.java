package org.atlas.applications.modules.core;

import java.io.UnsupportedEncodingException;
import javax.ejb.Local;
import javax.ws.rs.core.Response;

/**
 * Defines methods to acquire application responses in HTTP requests.
 * @author ic
 */
@Local
public interface IApplicationResponse
{
    /**
     * Returns the binary data file for given application id and version.
     * @param applicationId
     * @param version
     * @return
     * @throws UnsupportedEncodingException 
     */
    public Response getFile(long applicationId, String version)  throws UnsupportedEncodingException;
    
    /**
     * Returns the binary data file for given application id.
     * @param applicationId
     * @return
     * @throws UnsupportedEncodingException 
     */
    public Response getFile(long applicationId)  throws UnsupportedEncodingException;    

}
