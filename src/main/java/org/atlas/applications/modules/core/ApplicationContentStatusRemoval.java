package org.atlas.applications.modules.core;

import java.util.function.Predicate;
import org.atlas.applications.modules.repo.domain.ApplicationContent;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;

/**
 * Predicate to remove application content elements with DELETED status.
 * @author ic
 */
public class ApplicationContentStatusRemoval implements Predicate<ApplicationContent>{

    @Override
    public boolean test(ApplicationContent t) {
        if(t.getStatus().equals(ApplicationStatus.DELETED))
            return true;
        return false;
    }
    
}
