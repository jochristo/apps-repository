/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.core;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import org.atlas.applications.core.exceptions.InvalidArgumentException;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.annotations.JAPIRelationship;
import org.atlas.applications.core.jsonapi.extensions.JSONApiIncludeType;
import org.atlas.applications.core.utilities.ClassUtils;
import org.atlas.applications.modules.jsonapi.IJsonApiDocumentCreator;
import org.atlas.applications.modules.repo.domain.Application;

/**
 * Handling class to resolve included data of {@link Application} instances.
 * @author ic
 */
@LocalBean
@Singleton
public class ApplicationIncludeResolverImpl implements IApplicationIncludeResolver<Application,JSONApiIncludeType>
{     
    /**
     *
     * @param application
     * @param includeType
     * @param jsonApiDocumentCreator
     * @return
     */
    @Override
    public JSONApiDocument getDocument(Application application, JSONApiIncludeType includeType, IJsonApiDocumentCreator jsonApiDocumentCreator)
    {
        Application app = null;
        app = this.resolved(application, includeType);
        return jsonApiDocumentCreator.createDocument(app, application.getLink());        
    }    
    
    /**
     *
     * @param <T>
     * @param object
     * @param includeType
     * @return
     */
    @Override
    public <T extends Object> T resolved (T object, JSONApiIncludeType includeType)
    {
        Set includeTypeSet = includeType.set();   
        Field[] allFields = ClassUtils.mergeClassFields(object.getClass().getDeclaredFields(), object.getClass().getSuperclass().getDeclaredFields());        
        for (Field field : allFields)
        {
            if (field.isAnnotationPresent(JAPIRelationship.class)) {
                field.setAccessible(true);
                Object value;
                try
                {
                    value = field.get(object);   // get relationship object: list, set, other          
                    Class<?> valueClass = value.getClass();
                    Class<?> relationshipClass = field.getAnnotation(JAPIRelationship.class).relationship();
                    Object newValue = relationshipClass.newInstance();
                    if(value instanceof Collection)
                    {
                        ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
                        Class<?> innerTypeClass = (Class<?>) parameterizedType.getActualTypeArguments()[0];
                        valueClass = innerTypeClass;
                        if(!includeTypeSet.contains(relationshipClass) && relationshipClass == valueClass){
                            Collection collection = (Collection) value;
                            collection.clear();
                            collection.add(newValue);
                            field.set(object, null);
                        }                        
                    }
                    else
                    {
                        if(!includeTypeSet.contains(relationshipClass) && relationshipClass == valueClass){
                            field.set(object, null);
                        }                          
                    }                    
                }
                catch (IllegalArgumentException | IllegalAccessException | InstantiationException ex)
                {                    
                    throw new InvalidArgumentException(ex.getMessage());                                    
                }
            }
        }        
        return object;
    }    

    @Override
    public org.joko.core.jsonapi.JSONApiDocument getDocumentExt(Application entity, JSONApiIncludeType includeType, IJsonApiDocumentCreator jsonApiDocumentCreator)
    {
        Application app = null;
        app = this.resolved(entity, includeType);
        return jsonApiDocumentCreator.createDocumentExt(app, null); 
    }
    
}
