package org.atlas.applications.modules.core;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;
import org.atlas.applications.api.Globals;
import org.atlas.applications.api.RequestParameters;
import static org.atlas.applications.api.RequestParameters.APPS_PATH_DEPTH;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.extensions.JSONApiIncludeType;
import org.atlas.applications.core.jsonapi.extensions.JSONApiLinkNoPaging;
import org.atlas.applications.core.jsonapi.JSONApiLink;
import org.atlas.applications.core.jsonapi.helpers.TopLevelLinksProperties;
import org.atlas.applications.core.utilities.Hashing;
import org.atlas.applications.core.utilities.Utilities;
import org.atlas.applications.modules.jsonapi.IJsonApiDocumentCreator;
import org.atlas.applications.modules.multiparts.BaseMultipartBatch;
import org.atlas.applications.modules.multiparts.IMultipartHandler;
import org.atlas.applications.modules.multiparts.PatchMultipartBatch;
import org.atlas.applications.modules.multiparts.UpdateMultipartBatch;
import org.atlas.applications.modules.multiparts.UploadMultipartBatch;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationContent;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;
import org.atlas.applications.modules.repo.domain.ApplicationTransaction;
import org.atlas.applications.modules.repo.domain.SemVerComparator;
import org.atlas.applications.modules.repo.domain.MimeType;
import org.atlas.applications.modules.repo.domain.TransactionType;
import org.atlas.applications.modules.repo.dto.ApplicationDto;
import org.atlas.applications.modules.repo.services.ITransactional;
import org.atlas.applications.modules.repo.services.impl.ApplicationContentServiceImpl;
import org.atlas.applications.modules.repo.services.impl.ApplicationServiceImpl;
import org.atlas.applications.modules.repo.services.impl.ApplicationTransactionServiceImpl;
import org.atlas.applications.modules.repo.services.impl.MimeTypeServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Main Application service handling class containing methods to service endpoints.
 * It handles all transactions with persistence and server ends to create, read, update, and write application files.
 * @author ic
 */
@LocalBean
@Singleton
@TransactionManagement(TransactionManagementType.CONTAINER)
public class BaseApplicationServiceImpl implements IBaseApplicationService
{
    // enterprise services
    @EJB
    private ApplicationServiceImpl applicationService;
    @EJB
    private ApplicationContentServiceImpl applicationContentService;
    @EJB    
    private ApplicationTransactionServiceImpl applicationTransactionService;    
    @EJB
    private MimeTypeServiceImpl mimeTypeService;
    @Context
    HttpServletRequest httpServletRequest;
    @EJB
    private ITransactional iTransactional;
    @EJB
    private IMultipartHandler iMultipartHandler;
    @EJB
    private IJsonApiDocumentCreator jsonApiDocumentCreator;
    @EJB
    private IApplicationIncludeResolver includeResolver;

    // system-context properties
    private static final String CATALINA_BASE = System.getProperty("catalina.base");    

    // dto, etc.
    private ApplicationDto _applicationDto;
    private FileItem _fileItem;

    // Holds current Application/ApplicationContent objects - upload case
    private Application _application = null;
    private ApplicationContent _applicationContent = null;

    private static final Logger logger = LogManager.getLogger(BaseApplicationServiceImpl.class);     

    // multipart batch items
    private UploadMultipartBatch uploadMultipartBatch;
    private UpdateMultipartBatch updateMultipartBatch;
    private PatchMultipartBatch patchMultipartBatch;

    @Override    
    public Application getApplication() {
        return _application;
    }

    @Override
    public void setApplication(Application application) {
        this._application = application;
    }

    @Override    
    public ApplicationContent getApplicationContent() {
        return _applicationContent;
    }

    @Override
    public void setApplicationContent(ApplicationContent _applicationContent) {
        this._applicationContent = _applicationContent;
    }

    /**
     * Gets this instance {@link ApplicationDto} object.
     *
     * @return ApplicationDto
     */
    @Override
    public ApplicationDto getApplicationDocument() {
        return this._applicationDto;
    }

    /**
     * Uploads to server the given multi parts and saves to database
     * accordingly.
     *
     * @param multiparts The attached multi part items.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public void uploadApplication(List<FileItem> multiparts) {
        // get multiparts
        this.uploadMultipartBatch = (UploadMultipartBatch) iMultipartHandler.getUploadMultipartBatch(multiparts);

        // validate
        this.uploadMultipartBatch.validate();

        // set fileitem
        this._fileItem = this.uploadMultipartBatch.getFileItem();

        // create application
        _application = new Application();
        String sanitizedRepositoryName = Utilities.sanitizeRepositoryName(uploadMultipartBatch.getRepository());
        _application.setRepository(sanitizedRepositoryName);
        _application.setName(uploadMultipartBatch.getName()); // application name, e.g.myApplication.jar : optional        
        _application.setLanguage(uploadMultipartBatch.getLang());
        _application.setHash(DigestUtils.sha256Hex(sanitizedRepositoryName)); // hash repository-folder name        
        _application.setIsCompressed(1);
        _application.setCreatedAt(new java.util.Date());
        Application savedApplication = applicationService.createApplication(_application);

        // create contents       
        ApplicationContent appContent = new ApplicationContent();
        appContent.setApplication(savedApplication);
        String checksum = DigestUtils.sha256Hex(_fileItem.get());
        appContent.setChecksum(checksum);
        appContent.setFilename(_fileItem.getName());
        int isExecutable = uploadMultipartBatch.getIsExec().equalsIgnoreCase(Globals.TRUE) ? 1 : 0;
        appContent.setIsExecutable(isExecutable);
        appContent.setSize(_fileItem.getSize());
        appContent.setStatus(ApplicationStatus.ENABLED);
        appContent.setVersion(uploadMultipartBatch.getVer());

        // set main if isExec=true
        if (uploadMultipartBatch.getIsExec().equals(Globals.TRUE)) {
            appContent.setMain(uploadMultipartBatch.getMain());
        }

        String storageName = "";
        try {
            // process stored name of file            
            storageName = Hashing.uuidToBase64(uploadMultipartBatch.getFileItem().get());
            storageName = Utilities.sanitizeRepositoryName(storageName).substring(0, 12);
            storageName += "_" + appContent.getVersion().replaceAll("\\.", "");
        } catch (UnsupportedEncodingException ex) {
            logger.error(BaseApplicationServiceImpl.class.getName(), ex);
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.ILLEGAL_ARGUMENT_TYPE);
        }

        String extension = FilenameUtils.getExtension(_fileItem.getName());          // get extension
        appContent.setStoredname(storageName.concat(".").concat(extension));   // stored filename        
        List<MimeType> mimeType = mimeTypeService.findManyByParam("MimeType.findByMime", "mime", _fileItem.getContentType());
        appContent.setMimeType(mimeType.get(0));
        appContent = applicationContentService.created(appContent);

        // create transaction
        //iTransactional.createTransaction(savedApplication.getId(), TransactionType.CREATE);
        iTransactional.createTransaction(savedApplication.getId(), appContent, TransactionType.CREATE);

        // create file in file system
        writeContents(uploadMultipartBatch, _application.getRepository(), appContent.getStoredname(), false);

        // DTO
        ApplicationDto appDto = new ApplicationDto();
        appDto = new ApplicationDto();
        appDto.setId(savedApplication.getId());
        appDto.setRepository(savedApplication.getRepository());
        appDto.setAppname(savedApplication.getName());
        appDto.setMimeType(appContent.getMimeType().getMime());
        appDto.setLanguage(savedApplication.getLanguage());
        appDto.setMain(appContent.getMain());
        appDto.setVersion(appContent.getVersion());
        appDto.setChecksum(appContent.getChecksum());
        appDto.setStatus(savedApplication.getApplicationStatus());
        appDto.setVersionStatus(appContent.getStatus());
        this._applicationDto = appDto;

        // set internal JPA instances
        setApplication(savedApplication);
        setApplicationContent(appContent);
    }

    /**
     * Updates this application entity and creates new version with given multi
     * part parameters.
     *
     * @param multiparts
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public void updateApplication(List<FileItem> multiparts) {
        // get parts
        this.updateMultipartBatch = (UpdateMultipartBatch) iMultipartHandler.getUpdateMultipartBatch(multiparts);

        // vaidate parts
        this.updateMultipartBatch.validate();

        // set fileitem
        this._fileItem = this.updateMultipartBatch.getFileItem();

        // internal null checking            
        if (this._application == null) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_NOT_FOUND);
        }

        boolean exists = applicationContentService.existsVersion(_application.getId(), updateMultipartBatch.getVer());
        if (exists) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_VERSION_EXISTS);
        }

        // validate semantics version against existing
        List<ApplicationContent> contents = applicationContentService.findByApplicationIdExclDeleted(_application.getId());
        if (contents.isEmpty() == false) {
            List<String> versions = new ArrayList();
            contents.forEach((item) -> {
                versions.add(item.getVersion());
            });
            String latestVersion = versions.size() == 1 ? versions.get(0) : Utilities.semVerGreatest(versions);
            if (!Utilities.isSemverGreater(latestVersion, updateMultipartBatch.getVer())) {
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.INVALID_APPLICATION_VERSION);
            }
        }

        // check application properties   
        if (!Utilities.isEmpty(updateMultipartBatch.getLang())) {
            _application.setLanguage(updateMultipartBatch.getLang());
        }
        if (!Utilities.isEmpty(updateMultipartBatch.getName())) {
            _application.setName(updateMultipartBatch.getName());
        }
        if (_application.getLanguage() != null || _application.getName() != null) {
            _application.setUpdatedAt(new java.util.Date());
            _application = applicationService.updated(_application);
        }

        // create contents       
        ApplicationContent appContent = new ApplicationContent();
        appContent.setApplication(_application);
        appContent.setChecksum(DigestUtils.sha256Hex(updateMultipartBatch.getFileItem().get()));
        appContent.setSize(updateMultipartBatch.getFileItem().getSize());
        appContent.setFilename(updateMultipartBatch.getFileItem().getName());
        appContent.setVersion(updateMultipartBatch.getVer());
        appContent.setStatus(ApplicationStatus.ENABLED);
        appContent.setIsExecutable(0);
        if (updateMultipartBatch.getIsExec() != null) {
            int isExecutable = updateMultipartBatch.getIsExec().equalsIgnoreCase(Globals.TRUE) ? 1 : 0;
            appContent.setIsExecutable(isExecutable);
        }
        List<MimeType> mimeType = mimeTypeService.findManyByParam("MimeType.findByMime", "mime", updateMultipartBatch.getFileItem().getContentType());
        appContent.setMimeType(mimeType.get(0));

        // set main if isExec=true
        if (updateMultipartBatch.getIsExec().equals(Globals.TRUE)) {
            appContent.setMain(updateMultipartBatch.getMain());
        }

        String storageName = "";
        try {
            // process stored name of file            
            storageName = Hashing.uuidToBase64(updateMultipartBatch.getFileItem().get());
            storageName = Utilities.sanitizeRepositoryName(storageName).substring(0, 12);
            storageName += "_" + appContent.getVersion().replaceAll("\\.", "");
        } catch (UnsupportedEncodingException ex) {
            logger.error(BaseApplicationServiceImpl.class.getName(), ex);
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.ILLEGAL_ARGUMENT_TYPE);
        }

        String extension = FilenameUtils.getExtension(_fileItem.getName());         // get extension
        appContent.setStoredname(storageName.concat(".").concat(extension));       // stored filename                    
        appContent = applicationContentService.created(appContent);

        // create transaction
        //iTransactional.createTransaction(_application.getId(), TransactionType.UPDATE);        
        iTransactional.createTransaction(_application.getId(), appContent, TransactionType.UPDATE);

        // create file in file system
        writeContents(updateMultipartBatch, _application.getRepository(), appContent.getStoredname(), true);

        // DTO
        ApplicationDto appDto = new ApplicationDto();
        if (_application != null) {
            appDto = new ApplicationDto();
            appDto.setId(_application.getId());
            appDto.setRepository(_application.getRepository());
            appDto.setAppname(_application.getName());
            appDto.setMimeType(appContent.getMimeType().getMime());
            appDto.setLanguage(_application.getLanguage());
            appDto.setMain(appContent.getMain());
            appDto.setVersion(appContent.getVersion());
            appDto.setChecksum(appContent.getChecksum());
            appDto.setStatus(_application.getApplicationStatus());
            appDto.setVersionStatus(appContent.getStatus());
            this._applicationDto = appDto;
        }

        // set internal JPA instances
        setApplication(_application);
        setApplicationContent(appContent);
    }

    /**
     * Uploads to server the given multi parts and saves to database
     * accordingly. Patch function can update application properties and
     * override its contents, e.g. current version contains bugs.
     *
     * @param multiparts The attached multi part items.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public void patchApplication(List<FileItem> multiparts) {
        // assume patch
        TransactionType transactionType = TransactionType.PATCH;

        // get parts
        this.patchMultipartBatch = (PatchMultipartBatch) iMultipartHandler.getPatchMultipartBatch(multiparts);

        // validate part
        this.patchMultipartBatch.validate();

        this._fileItem = this.patchMultipartBatch.getFileItem();

        // internal null checking            
        if (this._application == null) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_NOT_FOUND);
        }

        if (!Utilities.isEmpty(patchMultipartBatch.getLang())) {
            _application.setLanguage(patchMultipartBatch.getLang());
        }
        if (!Utilities.isEmpty(patchMultipartBatch.getName())) {
            _application.setName(patchMultipartBatch.getName());
        }

        // NON-PATCH: nothing to update -missing application properties parameters
        if (patchMultipartBatch.getIsPatch().equals(RequestParameters.PARAM_FALSE)) {
            if (patchMultipartBatch.getMain() == null && patchMultipartBatch.getLang() == null && patchMultipartBatch.getName() == null) {
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.MISSING_NON_PATCH_MULTIPART_PARAMETERS);
            }
        }

        // update application properties ONLY
        if (_application.getLanguage() != null || _application.getName() != null) {
            _application.setUpdatedAt(new java.util.Date());
            _application = applicationService.updated(_application);
            transactionType = TransactionType.UPDATE;
        }

        // load contents
        ApplicationContent appContent = applicationContentService.findByApplicationIdAndVersionExclDeleted(_application.getId(), patchMultipartBatch.getVer());
        if (appContent == null) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_VERSION_NOT_FOUND);
        }

        appContent.setApplication(_application);
        if (patchMultipartBatch.getIsPatch().equals(RequestParameters.PARAM_TRUE)) {
            appContent.setIsBugFree(1);
            appContent.setSize(patchMultipartBatch.getFileItem().getSize());
            appContent.setChecksum(DigestUtils.sha256Hex(patchMultipartBatch.getFileItem().get()));
            appContent.setStatus(ApplicationStatus.ENABLED);

            // set main if isExec=true
            appContent.setMain(patchMultipartBatch.getMain());
            int isExecutable = Utilities.isEmpty(patchMultipartBatch.getMain()) ? 0 : 1;
            appContent.setIsExecutable(isExecutable);

            // update contents
            appContent = applicationContentService.updated(appContent);

            // patch content fix
            writeContents(patchMultipartBatch, _application.getRepository(), appContent.getStoredname(), true);

            // transaction is PATCH!
            transactionType = TransactionType.PATCH;
            iTransactional.createTransaction(_application.getId(), appContent, transactionType);
        } else {
            // non patch, just update fields in db
            appContent.setStatus(ApplicationStatus.ENABLED);
            if (patchMultipartBatch.getMain() != null) {
                appContent.setMain(patchMultipartBatch.getMain());
                int isExecutable = Utilities.isEmpty(patchMultipartBatch.getMain()) ? 0 : 1;
                appContent.setIsExecutable(isExecutable);
            }

            // update contents
            appContent = applicationContentService.updated(appContent);

            // transaction is UPDATE!
            transactionType = TransactionType.UPDATE;
            iTransactional.createTransaction(_application.getId(), appContent, transactionType);
        }

        // create transaction
        //iTransactional.createTransaction(_application.getId(), transactionType);                 
        // DTO
        ApplicationDto appDto = new ApplicationDto();
        if (_application != null) {
            appDto = new ApplicationDto();
            appDto.setId(_application.getId());
            appDto.setRepository(_application.getRepository());
            appDto.setAppname(_application.getName());
            appDto.setMimeType(appContent.getMimeType().getMime());
            appDto.setLanguage(_application.getLanguage());
            appDto.setMain(appContent.getMain());
            appDto.setVersion(appContent.getVersion());
            appDto.setChecksum(appContent.getChecksum());
            appDto.setStatus(_application.getApplicationStatus());
            appDto.setVersionStatus(appContent.getStatus());
            this._applicationDto = appDto;
        }

        // set internal JPA instances
        setApplication(_application);
        setApplicationContent(appContent);
    }

    /**
     * Writes bytes to given repository using given multi part batch item.
     *
     * @param partBatch Contains multi part parm values.
     * @param repository directory name to store to
     * @param storedName the actual file name to store data to
     * @param isOverride indicates whether this write operation is first time
     * creation or a patch.
     * @return
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean writeContents(BaseMultipartBatch partBatch, String repository, String storedName, boolean isOverride) {
        // create file in file system
        InitialContext initialContext;
        try {
            initialContext = new javax.naming.InitialContext();
            String repoPath = (String) initialContext.lookup("java:comp/env/Applications.Repository.Path");
            String storagePath = CATALINA_BASE.concat(repoPath);

            // hash application repository name           
            String repositoryPath = Hashing.createDirectoryPathString(repository, APPS_PATH_DEPTH);

            // add repository name to path
            File file = new File(storagePath.concat(repositoryPath).concat(repository));
            Path path = Paths.get(file.getAbsolutePath());
            Files.createDirectories(path);

            // write file
            String fullfilePath = new File(file.getAbsolutePath().concat(File.separator).concat(storedName)).getAbsolutePath();
            if (Utilities.existsFile(fullfilePath) && isOverride == false) {
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_FILE_EXISTS);
            }
            Utilities.write(partBatch.getFileItem().get(), fullfilePath, false);
            logger.info("Updating application contents in repository directory path..." + file.getAbsolutePath());
            return true;
        } catch (NamingException | IOException ex) {
            logger.error(BaseApplicationServiceImpl.class.getName(), ex);
            throw new RestApplicationException(Response.Status.BAD_REQUEST, "400", ex.getLocalizedMessage());
        }
    }

    /**
     * Sets and updates application status. Creates ApplicationTransaction
     * instance and persists according to given status.
     *
     * @param applicationId
     * @param status
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void setApplicationStatus(long applicationId, ApplicationStatus status) {

        Application application = applicationService.find(applicationId);
        if (application == null) {
            throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND);
        }
        switch (status) {
            case ENABLED:
                if (!application.getApplicationStatus().equals(ApplicationStatus.ENABLED)) {
                    application.setApplicationStatus(ApplicationStatus.ENABLED);
                    application.setUpdatedAt(new Date());
                    applicationService.update(application);
                    iTransactional.createTransaction(applicationId, TransactionType.ENABLE);
                }
                break;
            case DELETED:
                if (!application.getApplicationStatus().equals(ApplicationStatus.DELETED)) {
                    application.setApplicationStatus(ApplicationStatus.DELETED);
                    application.setUpdatedAt(new Date());
                    applicationService.update(application);
                    iTransactional.createTransaction(applicationId, TransactionType.DELETE);
                }
                break;
            case DISABLED:
                if (!application.getApplicationStatus().equals(ApplicationStatus.DISABLED)) {
                    application.setApplicationStatus(ApplicationStatus.DISABLED);
                    application.setUpdatedAt(new Date());
                    applicationService.update(application);
                    iTransactional.createTransaction(applicationId, TransactionType.DISABLE);
                }
                break;
        }
    }

    /**
     * Sets and updates application content status.
     *
     * @param applicationId
     * @param version
     * @param status
     */
    //@Override
    public void setApplicationVersionStatus(long applicationId, String version, ApplicationStatus status)
    {
        List<ApplicationContent> contents = new ArrayList();
        if (!Utilities.isEmpty(version)) {
            switch (version) {
                case "all":

                    // check version status
                    contents = applicationContentService.findByApplicationIdExclDeleted(applicationId);
                    if (contents.isEmpty()) {
                        throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_VERSIONS_NOT_PRESENT);
                    }

                    // iterare contents
                    contents.stream().filter((item) -> (!item.getStatus().equals(status))).forEachOrdered((i)
                            -> {
                        // update database if:  1. not already on same status and 2. NOT DELETED
                        ApplicationContent content = applicationContentService.findByApplicationIdAndVersionExclDeleted(applicationId, i.getVersion());
                        if (!content.getStatus().equals(ApplicationStatus.DELETED)) {
                            content.setStatus(status);
                            applicationContentService.update(content);
                        }
                    });

                    // create transaction
                    iTransactional.createTransaction(applicationId, "all", status);
                    break;

                default:
                    ApplicationContent content = applicationContentService.findByApplicationIdAndVersionExclDeleted(applicationId, version);
                    if (content == null) {
                        throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_VERSION_NOT_FOUND);
                    }
                    content.setStatus(status);
                    applicationContentService.update(content);

                    // create transaction
                    iTransactional.createTransaction(applicationId, content, status);
                    break;
            }
        }
    }

    /**
     * Finds Application entity in persistence : not DELETED!
     *
     * @param id
     * @return
     */
    @Override
    public Application findApplication(long id) {
        return this.applicationService.find(id);
    }

    @Override
    public Application findApplicationLoadRelationships(long id) {
        return this.applicationService.findLoadRelationships(id);
    }
    
    /**
     * Gets all Application instances in persistence with given status
     *
     * @param status
     * @return
     */
    @Override
    public List<Application> findApplications(ApplicationStatus status) {
        return this.applicationService.findAll(status);
    }

    /**
     * Gets all Application instances in persistence with loaded contents -
     * Excludes application status: DELETED     *
     * @return
     */
    @Override
    public List<Application> findApplicationsLoadContents() {
        return this.applicationService.findAllExclDeletedWithContents();
    }

    @Override
    public List<Application> findAllApplications() {
        return this.applicationService.findAll();
    }

    @Override
    public List<Application> findAllApplications(int page, int size) {
        boolean isPaginationMode = page > 0 && size > 0;
        if (isPaginationMode == false) {
            return this.applicationService.findAll();
        } else {
            return this.applicationService.findAllPaged(page, size);
        }
    }

    @Override    
    public List<Application> findApplications(ApplicationStatus status, int page, int size) {
        boolean isPaginationMode = page > 0 && size > 0;
        if (isPaginationMode == false) {
            return this.applicationService.findAll(status);
        } else {
            return this.applicationService.findAllPaged(status, page, size);
        }
    }
    
    @Override  
    public int getCount() {
        return this.applicationService.getCount().intValue();
    }

    @Override      
    public int getCount(ApplicationStatus status) {
        return this.applicationService.getCount(status).intValue();
    }

    @Override
    public <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationIncludingData(Application application, Class<T> targetDTOClass, JSONApiIncludeType includeType) throws InstantiationException, IllegalAccessException
    {        
        List<ApplicationContent> contents = applicationContentService.findByApplicationIdExclDeleted(application.getId());
        List<ApplicationTransaction> transactions = applicationTransactionService.findByApplicationId(application.getId());
        application.setApplicationContents(contents);
        application.setApplicationTransactions(transactions);

        jsonApiDocumentCreator = targetDTOClass.newInstance();
        includeResolver = (IApplicationIncludeResolver<Application,JSONApiIncludeType>)includeResolver;   
        JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
        resourceLevelLinks.setSelf(getCompleteServerRequestUrl(httpServletRequest));
        application.setLink(resourceLevelLinks);
        return includeResolver.getDocument(application, includeType, jsonApiDocumentCreator);        
    }

    @Override
    public <T extends IJsonApiDocumentCreator> org.joko.core.jsonapi.JSONApiDocument getDataDocument(Application application, Class<T> targetDTOClass, JSONApiIncludeType includeType) throws InstantiationException, IllegalAccessException
    {
        List<ApplicationContent> contents = applicationContentService.findByApplicationIdExclDeleted(application.getId());
        List<ApplicationTransaction> transactions = applicationTransactionService.findByApplicationId(application.getId());
        application.setApplicationContents(contents);
        application.setApplicationTransactions(transactions);

        jsonApiDocumentCreator = targetDTOClass.newInstance();
        includeResolver = (IApplicationIncludeResolver<Application,JSONApiIncludeType>)includeResolver;   
        JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
        resourceLevelLinks.setSelf(getCompleteServerRequestUrl(httpServletRequest));
        application.setLink(resourceLevelLinks);
        return includeResolver.getDocumentExt(application, includeType, jsonApiDocumentCreator);
    }
    
    
    @Override
    public <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationWithVersion(Application application, String version, Class<T> targetDTOClass) throws InstantiationException, IllegalAccessException
    {             
        logger.info("Fetching application with version...", this);                
        jsonApiDocumentCreator = targetDTOClass.newInstance();
        logger.info("Using " + jsonApiDocumentCreator.getClass().getCanonicalName());                

        if (Utilities.isEmpty(version)) {
            List<ApplicationContent> contents = applicationContentService.findByApplicationId(application.getId());
            List<String> versions = new ArrayList();
            contents.forEach((item) -> {
                if (!item.getStatus().equals(ApplicationStatus.DELETED)) {
                    JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
                    resourceLevelLinks.setSelf(jsonApiDocumentCreator.getAppVersionSelfUrl(getServerRequestContextPath(httpServletRequest), application.getId(), item.getVersion()));
                    item.setLink(resourceLevelLinks);
                    versions.add(item.getVersion());
                }
            });

            // no versions found
            if (versions.isEmpty()) {
                contents.clear();
                application.setApplicationContents(contents);
                application.getApplicationTransactions().clear(); // clear transaction data                
                JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
                resourceLevelLinks.setSelf(getCompleteServerRequestUrl(httpServletRequest));

                // prepare json api createDocument
                JSONApiLinkNoPaging topLevelLinks = new JSONApiLinkNoPaging();
                topLevelLinks.setSelf(getCompleteServerRequestUrl(httpServletRequest));
                //return new JSONApiDocument(application, topLevelLinks);
                return jsonApiDocumentCreator.createDocument(application, topLevelLinks);
            }

            // get latest version
            String latestVersion = versions.size() == 1 ? versions.get(0) : Utilities.semVerGreatest(versions);

            // load contents by id and version
            ApplicationContent appContents = applicationContentService.findByApplicationIdAndVersionExclDeleted(application.getId(), latestVersion);
            JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
            resourceLevelLinks.setSelf(jsonApiDocumentCreator.getAppVersionSelfUrl(getServerRequestContextPath(httpServletRequest), application.getId(), appContents.getVersion()));
            appContents.setLink(resourceLevelLinks);

            // add relationships to application
            contents.clear();
            contents.add(appContents);
            application.setApplicationContents(contents);
            application.getApplicationTransactions().clear(); // clear transaction data

            // prepare json api createDocument
            JSONApiLinkNoPaging topLevelLinks = new JSONApiLinkNoPaging();
            topLevelLinks.setSelf(getCompleteServerRequestUrl(httpServletRequest));
            //return new JSONApiDocument(application, topLevelLinks);
            return jsonApiDocumentCreator.createDocument(application, topLevelLinks);
        } 
        else
        {
            // get version from db
            ApplicationContent appContents = applicationContentService.findByApplicationIdAndVersionExclDeleted(application.getId(), version);
            if (appContents == null) {
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_VERSION_NOT_FOUND);
            }

            JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
            resourceLevelLinks.setSelf(jsonApiDocumentCreator.getAppVersionSelfUrl(getServerRequestContextPath(httpServletRequest), application.getId(), appContents.getVersion()));
            appContents.setLink(resourceLevelLinks);

            // add relationships to application
            List<ApplicationContent> contents = new ArrayList();
            contents.add(appContents);
            application.setApplicationContents(contents);
            application.getApplicationTransactions().clear(); // clear transaction data

            // prepare json api createDocument
            JSONApiLinkNoPaging topLevelLinks = new JSONApiLinkNoPaging();
            topLevelLinks.setSelf(getCompleteServerRequestUrl(httpServletRequest));
            //return new JSONApiDocument(application, topLevelLinks);
            return jsonApiDocumentCreator.createDocument(application, topLevelLinks);
        }
    }

    //@Override
    @Override
    public <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationsWithLatestVersion(List<Application> applications, Class<T> targetDTOClass, TopLevelLinksProperties properties) throws InstantiationException, IllegalAccessException
    {
        jsonApiDocumentCreator = targetDTOClass.newInstance();
        
        // holds apps
        List<Application> apps = new ArrayList(); // list containing latest or many version contents
        
        // return null data
        if(Utilities.isEmpty(applications)){
            JSONApiDocument doc = new JSONApiDocument();
            doc.setData(apps);
            return doc;
        }
        applications.forEach((Application application) -> {                                   
            
            // get properties of latest version if exists one            
            List<ApplicationContent> contents = applicationContentService.findByApplicationIdExclDeleted(application.getId()); 
            Application app = application; // copy object
            if (contents.isEmpty() == false) {
                
                // sort content by version: latest first
                contents.sort(new SemVerComparator());
                String latestVersion = contents.get(0).getVersion(); // get first element                                
                ApplicationContent appContents = contents.get(0);            
                
                // set self url for contents
                JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
                resourceLevelLinks.setSelf(jsonApiDocumentCreator.getAppVersionSelfUrl(getServerRequestContextPath(httpServletRequest), application.getId(), appContents.getVersion()));
                appContents.setLink(resourceLevelLinks);                
                
                app.getApplicationContents().clear();
                List<ApplicationContent> contentsList = app.getApplicationContents();
                contentsList.add(appContents);
                app.setApplicationContents(contentsList); // set this app's contents : single or many                
            }
            else{
               app.setApplicationContents(contents); // set empty list of contents - versions
            }
            // clear transaction data
            app.getApplicationTransactions().clear(); 
            
            // add to apps list
            apps.add(app);
        });                        
        
        // create pagination links
        JSONApiLink links = properties.createLinks();        
        //return new JSONApiDocument(apps,links); 
        return jsonApiDocumentCreator.createDocument(apps, links);
        
    }

    @Override
    public <T extends IJsonApiDocumentCreator> JSONApiDocument setStatus(Application application, String version, ApplicationStatus status, Class<T> targetDTOClass) throws InstantiationException, IllegalAccessException
    {
        if(Utilities.isEmpty(version)){
            setApplicationStatus(application.getId(), status);
        }        
        else if(Utilities.isEmpty(version) == false){                       
            setApplicationVersionStatus(application.getId(), version, status);
        }  
        
        // get JSONApidocument
        if(status.equals(ApplicationStatus.DELETED))
        {
            application = applicationService.findInclDeleted(application.getId());
            return getApplicationOnStatusSetIncludingVersions(application, version, true, targetDTOClass);
        }
        else
        {
            application = applicationService.find(application.getId());
            return getApplicationOnStatusSetIncludingVersions(application, version, false, targetDTOClass);
        } 
    }    

    @Override
    public <T extends IJsonApiDocumentCreator> JSONApiDocument getApplicationOnStatusSetIncludingVersions(Application application, String version, boolean includeDeletedVersion, Class<T> targetDTOClass) throws InstantiationException, IllegalAccessException
    {
        jsonApiDocumentCreator = targetDTOClass.newInstance();        
        ApplicationContent appContent = null;
        List<ApplicationContent> contents = new ArrayList();      
        
        // set toplevel links
        JSONApiLinkNoPaging toplevelLinks = new JSONApiLinkNoPaging();
        toplevelLinks.setSelf(getCompleteServerRequestUrl(httpServletRequest));
        application.setLink(toplevelLinks);                                  
                
        if(Utilities.isEmpty(version) == false)
        {            
            if (includeDeletedVersion) // fetch DELETED stuff for confirmation
            {
                // version=all
                if(version.equals("all"))
                {
                    contents = applicationContentService.findByApplicationIdExclDeleted(application.getId());
                    contents.forEach((item)->
                    {
                        JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();                        
                        resourceLevelLinks.setSelf(jsonApiDocumentCreator.getAppVersionSelfUrl(getServerRequestContextPath(httpServletRequest), application.getId(), item.getVersion()));
                        item.setLink(resourceLevelLinks);                         
                    });
       
                    // sort content by version: latest first
                    contents.sort(new SemVerComparator());                      

                    // add relationships to application
                    application.setApplicationContents(contents);  
                    application.getApplicationTransactions().clear(); // clear transaction data                   
                    //return new JSONApiDocument(application);
                    return jsonApiDocumentCreator.createDocument(application, toplevelLinks);
                } 
                
                // load specified version
                appContent = applicationContentService.findByApplicationIdAndVersionInclDeleted(application.getId(), version);     
                JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
                resourceLevelLinks.setSelf(jsonApiDocumentCreator.getAppVersionSelfUrl(getServerRequestContextPath(httpServletRequest), application.getId(), appContent.getVersion()));
                appContent.setLink(resourceLevelLinks);                  
                
                // add relationships to application
                contents.add(appContent);
                application.setApplicationContents(contents);
                application.getApplicationTransactions().clear(); // clear transaction data               
                //return new JSONApiDocument(application);
                return jsonApiDocumentCreator.createDocument(application, toplevelLinks);
            } 
            else // non DELETE request
            {
                // version check
                if(version.equals("all"))
                {
                    contents = applicationContentService.findByApplicationIdExclDeleted(application.getId());  
                    contents.forEach((item) -> {
                        JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
                        resourceLevelLinks.setSelf(jsonApiDocumentCreator.getAppVersionSelfUrl(getServerRequestContextPath(httpServletRequest), application.getId(), item.getVersion()));
                        item.setLink(resourceLevelLinks);
                    });

                    // sort content by version: latest first
                    contents.sort(new SemVerComparator());

                    // add relationships to application
                    application.setApplicationContents(contents);
                    application.getApplicationTransactions().clear(); // clear transaction data
                }           
                else
                {
                    appContent = applicationContentService.findByApplicationIdAndVersionExclDeleted(application.getId(), version);  
                    JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
                    resourceLevelLinks.setSelf(jsonApiDocumentCreator.getAppVersionSelfUrl(getServerRequestContextPath(httpServletRequest), application.getId(), appContent.getVersion()));
                    appContent.setLink(resourceLevelLinks);                    
                }
                
                // if contents is not null add to relationships list
                if(appContent != null) {
                    
                    contents.add(appContent);
                    // add relationships to application
                    application.setApplicationContents(contents);
                    application.getApplicationTransactions().clear(); // clear transaction data                    
                }                
                  
                //return new JSONApiDocument(application); 
                return jsonApiDocumentCreator.createDocument(application, toplevelLinks);
            }            
        }        
        { //else load latest content version if any
            
            // fetch DELETED stuff for confirmation
            if(includeDeletedVersion)
            {                
                application.getApplicationContents().clear();
                application.getApplicationTransactions().clear(); // clear transaction data
                //return new JSONApiDocument(application);
                return jsonApiDocumentCreator.createDocument(application, toplevelLinks);
            }
        
            // fetch contents by application id and exclude DELETED versions
            List<ApplicationContent> applicationContents = applicationContentService.findByApplicationId(application.getId());
            List<String> versions = new ArrayList();
            applicationContents.forEach((item) -> {
                if(!item.getStatus().equals(ApplicationStatus.DELETED)){
                    JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
                    resourceLevelLinks.setSelf(jsonApiDocumentCreator.getAppVersionSelfUrl(getServerRequestContextPath(httpServletRequest), application.getId(), item.getVersion()));
                    item.setLink(resourceLevelLinks);                
                    versions.add(item.getVersion());
                }
            });

            // nothing found , return with empty relationships list
            if(versions.isEmpty()){                
                application.getApplicationContents().clear();
                application.getApplicationTransactions().clear(); // clear transaction data
                //return new JSONApiDocument(application);
                return jsonApiDocumentCreator.createDocument(application, toplevelLinks);
            }                        
            
            // get latest version
            String latestVersion = versions.size() == 1 ? versions.get(0) : Utilities.semVerGreatest(versions);        
            appContent = applicationContentService.findByApplicationIdAndVersionExclDeleted(application.getId(), latestVersion);
            JSONApiLinkNoPaging resourceLevelLinks = new JSONApiLinkNoPaging();
            resourceLevelLinks.setSelf(jsonApiDocumentCreator.getAppVersionSelfUrl(getServerRequestContextPath(httpServletRequest), application.getId(), appContent.getVersion()));
            appContent.setLink(resourceLevelLinks);
            
            // add relationships to application
            contents.add(appContent);
            application.setApplicationContents(contents);             
            application.getApplicationTransactions().clear(); // clear transaction data
            //return new JSONApiDocument(application); 
            return jsonApiDocumentCreator.createDocument(application, toplevelLinks);
        } 
    }
    
    /**
     * Formulates a string from http servlet request. 
     * Used in top-level resource links objects.
     * Example: "http://localhost:8080/atlas-apps-repository/rest/v1/applications/3/disable?version=1.0.1-alpha"
     * i.e. scheme + server name + server port + context path + servlet path + query string
     * @param httpServletRequest
     * @return 
     */
    public static String getCompleteServerRequestUrl(HttpServletRequest httpServletRequest)
    {
        String scheme = httpServletRequest.getScheme();
        String serverName = httpServletRequest.getServerName();
        int serverPort = httpServletRequest.getServerPort();
        String contextPath = httpServletRequest.getContextPath();
        String servletPath = httpServletRequest.getServletPath();
        String pathInfo = httpServletRequest.getPathInfo();
        String queryString = httpServletRequest.getQueryString();
        StringBuilder sb = new StringBuilder(scheme);
        sb.append("://")
                .append(serverName)
                .append(":")
                .append(serverPort)
                .append(contextPath)
                .append(servletPath);
        if(queryString != null){
            sb.append("?" + queryString);
        }
        return sb.toString();
    }     
    
    /**
     * Formulates a string from http servlet request.
     * Used in self url links objects.
     * Example: "http://localhost:8080/atlas-apps-repository" i.e. scheme + server name + server port + context path
     * @param httpServletRequest
     * @return 
     */
    public static String getServerRequestContextPath(HttpServletRequest httpServletRequest)
    {
        String scheme = httpServletRequest.getScheme();
        String serverName = httpServletRequest.getServerName();
        int serverPort = httpServletRequest.getServerPort();
        String contextPath = httpServletRequest.getContextPath();
        StringBuilder sb = new StringBuilder(scheme);
        sb.append("://")
                .append(serverName)
                .append(":")
                .append(serverPort)
                .append(contextPath);
        return sb.toString();

    }    
    
}
