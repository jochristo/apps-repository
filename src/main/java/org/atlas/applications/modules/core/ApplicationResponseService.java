/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.core;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import org.apache.commons.codec.digest.DigestUtils;
import static org.atlas.applications.api.RequestParameters.APPS_PATH_DEPTH;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.atlas.applications.core.utilities.Hashing;
import org.atlas.applications.core.utilities.Utilities;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationContent;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;
import org.atlas.applications.modules.repo.domain.TransactionType;
import org.atlas.applications.modules.repo.services.ITransactional;
import org.atlas.applications.modules.repo.services.impl.ApplicationContentServiceImpl;
import org.atlas.applications.modules.repo.services.impl.ApplicationServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains methods to return application data responses to HTTP clients.
 * @author johnny
 */
@LocalBean
@Singleton
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ApplicationResponseService implements IApplicationResponse
{ 
    @EJB private ApplicationServiceImpl applicationService;
    @EJB private ApplicationContentServiceImpl applicationContentService;    
    @EJB private ITransactional iTransactional;
    private static final Logger logger = LoggerFactory.getLogger(ApplicationResponseService.class);
    private static final String CATALINA_BASE = System.getProperty("catalina.base");            
    private static final String REPOSITORY_PATH_ENV_NAME = "Applications.Repository.Path";

    /**
     * Downloads the file requested by given application id and version.
     * @param applicationId
     * @param version
     * @return
     * @throws java.io.UnsupportedEncodingException
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)    
    @Override
    public Response getFile(long applicationId, String version) throws UnsupportedEncodingException
    {        
        Application application = applicationService.find(applicationId);
        if (application == null) {
            throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND);
        }
        if(application.getApplicationStatus() == ApplicationStatus.DELETED){
           throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND); 
        }        
        if(application.getApplicationStatus() == ApplicationStatus.DISABLED){
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_NOT_ENABLED);
        }       

        // get content by version and app id
        ApplicationContent content = applicationContentService.findByApplicationIdAndVersion(applicationId, version);
        
        // check version status
        if(content.getStatus() == ApplicationStatus.DELETED){
           throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_VERSION_NOT_FOUND); 
        }        
        if(content.getStatus() == ApplicationStatus.DISABLED){
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_VERSION_NOT_ENABLED);
        }  
        
        String filepath = content.getStoredname();
        String servedFilename = content.getFilename();
        String repoPath = (String) getContextParamValue(REPOSITORY_PATH_ENV_NAME);
        String storagePath = CATALINA_BASE.concat(repoPath);

        // hash application repository name            
        String repositoryPath = Hashing.createDirectoryPathString(application.getRepository(), APPS_PATH_DEPTH);
        File file = new File(storagePath.concat(repositoryPath).concat(application.getRepository()).concat(File.separator).concat(filepath));
        if (Utilities.existsFile(file.getAbsolutePath()) == false) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_FILE_NOT_FOUND);
        }
        byte[] data = Utilities.toByteArray(file.getAbsolutePath());

        //SHA-256 checksum
        String shaChecksum = DigestUtils.sha256Hex(data);
        if (shaChecksum == null ? content.getChecksum() != null : !shaChecksum.equals(content.getChecksum())) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.BAD_FILE_CHECKSUM);
        }

        // write bytes to output stream
        String filename = servedFilename;
        String temp = filename;
        StreamingOutput fileStream = (java.io.OutputStream output) -> {
            output.write(data);
            output.flush();
            logger.info("Writing file to output stream..." + temp);
        };
        // try encode filename
        servedFilename = URLEncoder.encode(filename, "UTF-8");
        logger.info("Downloading file... " + temp);
        
        // create transaction
        iTransactional.createTransaction(applicationId, content, TransactionType.DOWNLOAD);
        
        return Response.ok(fileStream, MediaType.APPLICATION_OCTET_STREAM).header("Content-Disposition", "attachment; filename=" + servedFilename).build();
    }    

    /**
     * Downloads the file requested by given application id (in its latest version: default).
     * @param applicationId
     * @return 
     * @throws java.io.UnsupportedEncodingException 
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)    
    @Override
    public Response getFile(long applicationId) throws UnsupportedEncodingException
    {
        Application application = applicationService.find(applicationId);
        if (application == null) {
            throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND);
        }
        if(application.getApplicationStatus() == ApplicationStatus.DELETED){
           throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND); 
        }        
        if(application.getApplicationStatus() == ApplicationStatus.DISABLED){
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_NOT_ENABLED);
        }
        
        String filepath = null;
        byte[] data;

        // get latest version
        //List<ApplicationContent> contents = application.getApplicationContents();
        List<ApplicationContent> contents = applicationContentService.findByApplicationId(applicationId);
        List<String> versions = new ArrayList();
        contents.forEach((item) -> {
            versions.add(item.getVersion());
        });
        String latestVersion = Utilities.semVerGreatest(versions);

        // get file contents
        ApplicationContent content = applicationContentService.findByApplicationIdAndVersion(applicationId, latestVersion);
        
        // check version status
        if(content.getStatus() == ApplicationStatus.DELETED){
           throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND); 
        }        
        if(content.getStatus() == ApplicationStatus.DISABLED){
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_VERSION_NOT_ENABLED);
        }         
        
        filepath = content.getStoredname();
        String servedFilename = content.getFilename();
        String repoPath = (String) getContextParamValue(REPOSITORY_PATH_ENV_NAME);
        String storagePath = CATALINA_BASE.concat(repoPath);

        // hash application repository name            
        String repositoryPath = Hashing.createDirectoryPathString(application.getRepository(), APPS_PATH_DEPTH);
        File file = new File(storagePath.concat(repositoryPath).concat(application.getRepository()).concat(File.separator).concat(filepath));
        if (Utilities.existsFile(file.getAbsolutePath()) == false) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_FILE_NOT_FOUND);
        }        
        data = Utilities.toByteArray(file.getAbsolutePath());

        //SHA-256 checksum
        String shaChecksum = DigestUtils.sha256Hex(data);
        if (shaChecksum == null ? content.getChecksum() != null : !shaChecksum.equals(content.getChecksum())) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.BAD_FILE_CHECKSUM);
        }

        // write bytes to output stream
        String filename = servedFilename;
        String temp = filename;
        StreamingOutput fileStream = (java.io.OutputStream output) -> {
            output.write(data);
            output.flush();
            logger.info("Writing file to output stream... " + temp);
        };
        // try encode filename
        servedFilename = URLEncoder.encode(filename, "UTF-8");
        logger.info("Downloading file... " + temp);
        
        // create transaction
        iTransactional.createTransaction(applicationId, content, TransactionType.DOWNLOAD);
        
        return Response.ok(fileStream, MediaType.APPLICATION_OCTET_STREAM).header("Content-Disposition", "attachment; filename=" + servedFilename).build();
    }  
 
    protected Object getContextParamValue(String paramName)
    {
        InitialContext initialContext;
        try {
            initialContext = new javax.naming.InitialContext();
            Object value = (String) initialContext.lookup("java:comp/env/" + paramName);
            return value;
        }
        catch(NamingException ex)
        {
            logger.error(ApplicationResponseService.class.getName(), ex);
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }    
    
}
