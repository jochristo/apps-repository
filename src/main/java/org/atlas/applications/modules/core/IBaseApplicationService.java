/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.core;

import java.util.List;
import javax.ejb.Local;
import org.apache.commons.fileupload.FileItem;
import org.atlas.applications.modules.multiparts.BaseMultipartBatch;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationContent;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;
import org.atlas.applications.modules.repo.dto.ApplicationDto;
import org.atlas.applications.modules.jsonapi.IApplicationJSONApiCreator;

/**
 * Defines methods to handle {@link Application} and {@link ApplicationContent} instances.
 * @author ic
 */
@Local
public interface IBaseApplicationService extends IApplicationJSONApiCreator
{
    public Application getApplication();
    
    public ApplicationContent getApplicationContent();
    
    public void setApplication(Application application);
    
    public void setApplicationContent(ApplicationContent applicationContent);    

    public ApplicationDto getApplicationDocument();      
    
    public boolean writeContents(BaseMultipartBatch partBatch, String repository, String storedName, boolean isOverride);
    
    public void uploadApplication(List<FileItem> items);
    
    public void updateApplication(List<FileItem> items);
    
    public void patchApplication(List<FileItem> multiparts);    
    
    public void setApplicationStatus(long applicationId, ApplicationStatus status);
    
    public Application findApplication(long id);    
    
    public Application findApplicationLoadRelationships(long id);
        
    public List<Application> findApplicationsLoadContents();
    
    public List<Application> findApplications(ApplicationStatus status);
    
    public List<Application> findAllApplications();    
    
    public List<Application> findAllApplications(int page, int size);
    
    public List<Application> findApplications(ApplicationStatus status, int page, int size);
    
    public int getCount();

    public int getCount(ApplicationStatus status);
    
}
