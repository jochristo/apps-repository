/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Response;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.atlas.applications.core.exceptions.InvalidArgumentException;
import org.atlas.applications.core.jsonapi.extensions.JSONApiIncludeType;
import org.atlas.applications.core.utilities.Utilities;

/**
 * Parses include query parameter and defines the JSONApiIncludeType.
 * @author ic
 */
public final class IncludedDataParser
{
    private final String queryString;
    private JSONApiIncludeType includeType;
    private final Map<JSONApiIncludeType, Object> parameters = new HashMap<>();

    public IncludedDataParser(String queryString) {
        this.queryString = queryString;
        this.parse();
    }    
    
    protected void parse()
    {
        String[] splitted = this.queryString.split("include=");
        if(Utilities.isEmpty(splitted)){
            throw new InvalidArgumentException("Query string is empty");
        }
        List<String> list = new ArrayList<>(Arrays.asList(splitted));
        String includeString = list.get(list.size()-1);
        List<String> elements = Arrays.asList(includeString.split(","));
        if(Utilities.isEmpty(elements))
        {            
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.INVALID_RELATIONSHIP_PATH,  ": " + "Include parameter string is empty");
        }
        if(elements.size() == 1){
            if(elements.get(0).equalsIgnoreCase("versions"))
            {                
                setIncludeType(JSONApiIncludeType.VERSIONS);
                parameters.put(JSONApiIncludeType.VERSIONS, "versions");
            }
            else if("transactions".equalsIgnoreCase(elements.get(0)))
            {
                setIncludeType(JSONApiIncludeType.TRANSACTIONS);
                parameters.put(JSONApiIncludeType.TRANSACTIONS, "transactions");
            }
            else
            {                
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.INVALID_RELATIONSHIP_PATH,  ": " + elements.get(0));
            }
        }
        else // include=versions,transactions OR include=transactions,versions
        {
            boolean isValid = elements.size() == 2 && (elements.contains("versions") && elements.contains("transactions"));
            if(isValid == true)
            {                
                setIncludeType(JSONApiIncludeType.VERSIONS_TRANSACTIONS);
                parameters.put(JSONApiIncludeType.VERSIONS_TRANSACTIONS, "versions,transactions");
            }
            else
            {                
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.INVALID_RELATIONSHIP_PATH,  ": " + includeString);
            }
        }
    }

    public JSONApiIncludeType getIncludeType() {
        return includeType;
    }

    private void setIncludeType(JSONApiIncludeType includeType) {
        this.includeType = includeType;
    }

    public Map<JSONApiIncludeType, Object> getParameters() {
        return parameters;
    }

    public String getQueryString() {
        return queryString;
    }
    
    
    
    
}
