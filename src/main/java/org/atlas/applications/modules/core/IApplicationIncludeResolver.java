package org.atlas.applications.modules.core;

import javax.ejb.Local;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.extensions.JSONApiIncludeType;
import org.atlas.applications.modules.jsonapi.IJsonApiDocumentCreator;

/**
 * Defines methods to transfer data with included relationships to REST clients into JSONApiDocument format.
 * Includes extended version of JSON API document type.
 * @author ic
 * @param <T>
 * @param <E>
 */
@Local
public interface IApplicationIncludeResolver<T extends Object, E extends Enum<E>>
{       
    /**
     * Creates and return {@link JSONAPiDocument} from given T source and include type using provided 
     * {@link IJsonApiDocumentCreator} implementation.
     * @param entity the source object
     * @param includeType the type of included data
     * @param jsonApiDocumentCreator
     * @return JSONApiDocument
     */
    public JSONApiDocument getDocument(T entity, E includeType, IJsonApiDocumentCreator jsonApiDocumentCreator);
       
    /**
     * Resolves included data of given source object and returns modified data.
     * {@link IJsonApiDocumentCreator} implementation.
     * @param <T> the  type of source
     * @param object the source object
     * @param includeType the type of included data
     * @return T
     */
    public <T extends Object> T resolved (T object, JSONApiIncludeType includeType);
    
    public org.joko.core.jsonapi.JSONApiDocument getDocumentExt(T entity, E includeType, IJsonApiDocumentCreator jsonApiDocumentCreator);
}
