/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.multiparts;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ws.rs.core.Response;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;
import org.atlas.applications.api.RequestParameters;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.api.exceptions.InvalidApplicationFileException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles multi part items
 * @author ic
 */
@LocalBean
@Singleton
public class MultipartHandlerImpl implements IMultipartHandler{

    private static final Logger logger = LoggerFactory.getLogger(MultipartHandlerImpl.class);
    
    @Override
    public UploadMultipartBatch getUploadMultipartBatch(List<FileItem> multiparts) {
        
        UploadMultipartBatch uploadMultipartBatch = new UploadMultipartBatch();
        
        // Process multipart form data                    
        for (FileItem item : multiparts) {
            if (item.isFormField()) {
                String fieldName = item.getFieldName();
                if (!"".equals(fieldName) && fieldName.equals(RequestParameters.NAME_PARAM)) {
                    uploadMultipartBatch.setName(item.getString());
                    logger.info("Setting param name: "+ item.getString());
                    logger.debug("Setting param name: "+ item.get());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.REPOSITORY_PARAM)) {
                    uploadMultipartBatch.setRepository(item.getString());
                    logger.info("Setting param repository: "+ item.getString());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.MAIN_PARAM)) {
                    uploadMultipartBatch.setMain(item.getString());
                    logger.info("Setting param main: "+ item.getString());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.LANG_PARAM)) {
                    uploadMultipartBatch.setLang(item.getString());
                    logger.info("Setting param lang: "+ item.getString());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.VER_PARAM)) {
                    uploadMultipartBatch.setVer(item.getString());
                    logger.info("Setting param ver: "+ item.getString());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.ISEXEC_PARAM)) {
                    uploadMultipartBatch.setIsExec(item.getString());
                    logger.info("Setting param isExec: "+ item.getString());
                } 
            }
            if (!item.isFormField()) {
                String fieldName = item.getFieldName();
                if (!"".equals(fieldName) && fieldName.equals(RequestParameters.UFILE_PARAM)) {
                    // validate type of file: zip required
                    FileItem fItem = handleFileItem(item);                    
                    uploadMultipartBatch.setFileItem(fItem);                    
                    logger.info("Setting param ufile: " + item.getName());
                }
            }
        }              
        return uploadMultipartBatch;
    }

    @Override
    public UpdateMultipartBatch getUpdateMultipartBatch(List<FileItem> multiparts) {
        UpdateMultipartBatch updateMultipartBatch = new UpdateMultipartBatch();
        
        // Process multipart form data                    
        for (FileItem item : multiparts) {
            if (item.isFormField()) {
                String fieldName = item.getFieldName();
                if (!"".equals(fieldName) && fieldName.equals(RequestParameters.NAME_PARAM)) {
                    updateMultipartBatch.setName(item.getString());
                    logger.info("Setting param name: "+ item.getString());                    
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.MAIN_PARAM)) {
                    updateMultipartBatch.setMain(item.getString());
                    logger.info("Setting param main: "+ item.getString());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.LANG_PARAM)) {
                    updateMultipartBatch.setLang(item.getString());
                    logger.info("Setting param lang: "+ item.getString());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.VER_PARAM)) {
                    updateMultipartBatch.setVer(item.getString());
                    logger.info("Setting param ver: "+ item.getString());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.ISEXEC_PARAM)) {
                    updateMultipartBatch.setIsExec(item.getString());
                    logger.info("Setting param isExec: "+ item.getString());
                } 
                
            }
            if (!item.isFormField()) {
                String fieldName = item.getFieldName();
                if (!"".equals(fieldName) && fieldName.equals(RequestParameters.UFILE_PARAM)) {
                    // validate type of file: zip required
                    FileItem fItem = handleFileItem(item);
                    updateMultipartBatch.setFileItem(fItem);

                    logger.info("Setting param ufile: " + item.getName());
                }
            }
        }              

        return updateMultipartBatch;
    }

    @Override
    public PatchMultipartBatch getPatchMultipartBatch(List<FileItem> multiparts) {
        
        PatchMultipartBatch patchMultipartBatch = new PatchMultipartBatch();
        
        // Process multipart form data                    
        for (FileItem item : multiparts) {
            if (item.isFormField()) {
                String fieldName = item.getFieldName();
                if (!"".equals(fieldName) && fieldName.equals(RequestParameters.NAME_PARAM)) {
                    patchMultipartBatch.setName(item.getString());
                    logger.info("Setting param name: "+ item.getString());
                    logger.debug("Setting param name: "+ item.get());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.MAIN_PARAM)) {
                    patchMultipartBatch.setMain(item.getString());
                    logger.info("Setting param main: "+ item.getString());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.LANG_PARAM)) {
                    patchMultipartBatch.setLang(item.getString());
                    logger.info("Setting param lang: "+ item.getString());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.VER_PARAM)) {
                    patchMultipartBatch.setVer(item.getString());
                    logger.info("Setting param ver: "+ item.getString());
                } else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.ISEXEC_PARAM)) {
                    patchMultipartBatch.setIsExec(item.getString());
                    logger.info("Setting param isExec: "+ item.getString());
                } 
                else if (!"".equals(fieldName) && fieldName.equals(RequestParameters.IS_PATCH_PARAM)) {
                    patchMultipartBatch.setIsPatch(item.getString());
                    logger.info("Setting param isPatch: " + item.getString());
                }               
            }
            if (!item.isFormField()) {
                String fieldName = item.getFieldName();
                if (!"".equals(fieldName) && fieldName.equals(RequestParameters.UFILE_PARAM)) {
                    // validate type of file: zip required
                    FileItem fItem = handleFileItem(item);
                    patchMultipartBatch.setFileItem(fItem);
   
                    logger.info("Setting param ufile: " + item.getName());
                }
            }
        }              

        return patchMultipartBatch;
    }
    
    /**
     * Handles given File item. Throws exception if invalid type is found (i.e. non zip).
     * @param fileItem
     * @return 
     */
    protected static FileItem handleFileItem(FileItem fileItem)
    {
        // validate file type and contents
        if(!FilenameUtils.getExtension(fileItem.getName()).equalsIgnoreCase("zip")){
            throw new InvalidApplicationFileException(Response.Status.BAD_REQUEST, ApiError.INVALID_APPLICATION_FILE_TYPE);
        }
        if(!fileItem.getContentType().equals("application/zip") 
                && !"application/x-compressed".equals(fileItem.getContentType())
                && !fileItem.getContentType().equals("application/x-zip-compressed")){
            throw new InvalidApplicationFileException(Response.Status.BAD_REQUEST, ApiError.INVALID_APPLICATION_FILE_TYPE);
        }        
        
        return fileItem;
    }    
    
}
