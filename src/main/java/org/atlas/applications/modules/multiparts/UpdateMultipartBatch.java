package org.atlas.applications.modules.multiparts;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.ws.rs.core.Response;
import org.apache.commons.fileupload.FileItem;
import org.atlas.applications.api.RequestParameters;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.atlas.applications.core.utilities.Utilities;

/**
 * Represents an update multi-part batch item holding fields to update an existing application object.
 * @author ic
 */
public class UpdateMultipartBatch  extends BaseMultipartBatch
{

    public UpdateMultipartBatch() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }            
    
    
    public String getMain() {      
        return main;
    }

    public void setMain(String main) {

        this.main = main;
    }            
     
    public String getIsExec() {
        return isExec;
    }
  
    public void setIsExec(@Pattern(regexp = "^[tT][rR][uU][eE]|[fF][aA][lL][sS][eE]$", message = ApiError.INVALID_ISEXEC_PARAMETER_VIOLATION) String isExec) {
        this.isExec = isExec;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @NotNull(message = ApiError.MISSING_VER_PARAMETER_VIOLATION)        
    @Pattern(regexp = semverPattern, message = ApiError.INVALID_VER_PARAMETER_VIOLATION)    
    public String getVer() {
        return ver;
    }
    
    public void setVer(String ver) {
        this.ver = ver;
    }

    @NotNull(message = ApiError.MISSING_UFILE_PARAMETER_VIOLATION)     
    @Override
    public FileItem getFileItem() {
        return fileItem;
    }   
   
    @Override
    public void setFileItem(FileItem fileItem) {
        this.fileItem = fileItem;
    }

    @Override
    public UpdateMultipartBatch validate()
    {
        // validate dto
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<UpdateMultipartBatch>> violations = validator.validate(this);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        
        if (this.getIsExec() != null) {
            if (this.getIsExec().equals(RequestParameters.PARAM_TRUE) && Utilities.isEmpty(this.getMain())) {
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.MISSING_MAIN_PARAMETER);
            }
        } 
        
        return this;
    }

    
}
