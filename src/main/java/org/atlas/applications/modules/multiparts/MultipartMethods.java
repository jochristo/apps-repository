package org.atlas.applications.modules.multiparts;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.Response;
import org.apache.commons.fileupload.FileItem;
import org.atlas.applications.api.RequestParameters;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.api.exceptions.MultipartMissingException;
import org.atlas.applications.core.utilities.Utilities;

/**
 * Contains methods to validate multipart items
 * @author ic
 */
public interface MultipartMethods {
    
    /**
     * Validates multiparts on update
     * @param multiparts 
     */
    public static void validateMultipartsOnUpdate(List<FileItem> multiparts) {        
        if(Utilities.isEmpty(multiparts)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_MULTIPART_PARAMETERS);
        }
        Set<String> set = new HashSet();
        Map<String,Object> map = new HashMap();
        multiparts.forEach((item) -> {                   
            set.add(item.getFieldName());
            if(item.isFormField()){
                map.put(item.getFieldName(), item.getString());
            }else{
                map.put(item.getFieldName(), item);
            }
        });    
        
        if(!set.contains(RequestParameters.VER_PARAM)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_VER_PARAMETER);
        }
        if(Utilities.isEmpty((String)map.get(RequestParameters.VER_PARAM))){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_VER_PARAMETER);
        }  
        
        if(!set.contains(RequestParameters.UFILE_PARAM)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_UFILE_PARAMETER);
        }
        if((FileItem)map.get(RequestParameters.UFILE_PARAM) == null){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_UFILE_PARAMETER);
        }        
    }    
    
    /**
     * Validates multiparts on upload
     * @param multiparts 
     */
    public static void validateMultiparts(List<FileItem> multiparts) {        
        if(Utilities.isEmpty(multiparts)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_MULTIPART_PARAMETERS);
        }
        Set<String> set = new HashSet();
        Map<String,Object> map = new HashMap();
        multiparts.forEach((item) -> {                   
            set.add(item.getFieldName());
            if(item.isFormField()){
                map.put(item.getFieldName(), item.getString());
            }else{
                map.put(item.getFieldName(), item);
            }
        }); 
        
        if(!set.contains(RequestParameters.REPOSITORY_PARAM)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_REPOSITORY_PARAMETER);
        }       
        if(Utilities.isEmpty((String)map.get(RequestParameters.REPOSITORY_PARAM))){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_REPOSITORY_PARAMETER);
        }         
        
        if(!set.contains(RequestParameters.LANG_PARAM)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_LANG_PARAMETER);
        }
        if(Utilities.isEmpty((String)map.get(RequestParameters.LANG_PARAM))){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_LANG_PARAMETER);
        }         
        
        if(!set.contains(RequestParameters.MAIN_PARAM)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_MAIN_PARAMETER);
        }
        if(Utilities.isEmpty((String)map.get(RequestParameters.MAIN_PARAM))){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_MAIN_PARAMETER);
        }         
        
        if(!set.contains(RequestParameters.ISEXEC_PARAM)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_ISEXEC_PARAMETER);
        }
        if(Utilities.isEmpty((String)map.get(RequestParameters.ISEXEC_PARAM))){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_ISEXEC_PARAMETER);
        }         
        
        if(!set.contains(RequestParameters.VER_PARAM)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_VER_PARAMETER);
        }
        if(Utilities.isEmpty((String)map.get(RequestParameters.VER_PARAM))){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_VER_PARAMETER);
        }         
        
        if(!set.contains(RequestParameters.UFILE_PARAM)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_UFILE_PARAMETER);
        }
        if((FileItem)map.get(RequestParameters.UFILE_PARAM) == null){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_UFILE_PARAMETER);
        }         
    }     
    
    /**
     * Validates multiparts on update
     * @param multiparts 
     */
    public static void validateMultipartsOnPatch(List<FileItem> multiparts) {        
        if(Utilities.isEmpty(multiparts)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_MULTIPART_PARAMETERS);
        }
        Set<String> set = new HashSet();
        Map<String,Object> map = new HashMap();
        multiparts.forEach((item) -> {                   
            set.add(item.getFieldName());
            if(item.isFormField()){
                map.put(item.getFieldName(), item.getString());
            }else{
                map.put(item.getFieldName(), item);
            }
        });       
           
        
        if(!set.contains(RequestParameters.VER_PARAM)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_VER_PARAMETER);
        }    
        if(Utilities.isEmpty((String)map.get(RequestParameters.VER_PARAM))){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_VER_PARAMETER);
        }         

        if(!set.contains(RequestParameters.IS_PATCH_PARAM)){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_ISPATCH_PARAMETER);
        }    
        if(Utilities.isEmpty((String)map.get(RequestParameters.IS_PATCH_PARAM))){
            throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_ISPATCH_PARAMETER);
        }         

        if (((String)map.get(RequestParameters.IS_PATCH_PARAM)).equals(RequestParameters.PARAM_TRUE)) {
            if(!set.contains(RequestParameters.UFILE_PARAM) || (FileItem)map.get(RequestParameters.UFILE_PARAM) == null){
                throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_UFILE_PARAMETER);
            }                         
        }
        else if(((String)map.get(RequestParameters.IS_PATCH_PARAM)).equals(RequestParameters.PARAM_FALSE))
        {
            if(!set.contains(RequestParameters.MAIN_PARAM) && !set.contains(RequestParameters.LANG_PARAM) && !set.contains(RequestParameters.NAME_PARAM)){
                throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_NON_PATCH_MULTIPART_PARAMETERS);
            }
            if(Utilities.isEmpty((String)map.get(RequestParameters.MAIN_PARAM)) && Utilities.isEmpty((String)map.get(RequestParameters.LANG_PARAM)) 
                    && Utilities.isEmpty((String)map.get(RequestParameters.NAME_PARAM))){
                throw new MultipartMissingException(Response.Status.BAD_REQUEST, ApiError.MISSING_NON_PATCH_MULTIPART_PARAMETERS);
            }

        }
        
    }        
}
