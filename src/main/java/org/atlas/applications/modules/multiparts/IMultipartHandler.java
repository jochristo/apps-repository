package org.atlas.applications.modules.multiparts;

import java.util.List;
import javax.ejb.Local;
import org.apache.commons.fileupload.FileItem;
import org.atlas.applications.modules.multiparts.BaseMultipartBatch;

/**
 * Defines operations to handle upload multi-part items.
 * @author ic
 */
@Local
public interface IMultipartHandler {
    
    /**
     * Gets the application request input items from uploaded items
     * @param multiparts
     * @return {@link ApplicationRequestInput} 
     */    
    public BaseMultipartBatch getUploadMultipartBatch(List<FileItem> multiparts);
    
    /**
     * Gets the application request input items from updated items
     * @param multiparts
     * @return {@link ApplicationRequestInput} 
     */    
    public BaseMultipartBatch getUpdateMultipartBatch(List<FileItem> multiparts);
    
     /**
     * Gets the application request input items from patched items
     * @param multiparts
     * @return {@link ApplicationRequestInput} 
     */    
    public BaseMultipartBatch getPatchMultipartBatch(List<FileItem> multiparts);
}
