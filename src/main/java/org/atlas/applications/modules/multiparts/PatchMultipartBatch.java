package org.atlas.applications.modules.multiparts;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.ws.rs.core.Response;
import org.apache.commons.fileupload.FileItem;
import org.atlas.applications.api.Globals;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.core.utilities.Utilities;
import org.atlas.applications.api.RequestParameters;
import org.atlas.applications.api.exceptions.RestApplicationException;

/**
 * Represents a patch multi-part batch item holding fields to patch an existing application object.
 * @author ic
 */
public class PatchMultipartBatch extends BaseMultipartBatch
{
    private String isPatch = Globals.FALSE;
    //private String isExec = Globals.FALSE;

    public PatchMultipartBatch() {
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }       

    @NotNull(message = ApiError.MISSING_IS_PATCH_PARAMETER_VIOLATION)   
    @Pattern(regexp = "^[tT][rR][uU][eE]|[fF][aA][lL][sS][eE]$", message = ApiError.INVALID_IS_PATCH_PARAMETER_VIOLATION)     
    public String getIsPatch() {
        return isPatch;
    }
    
    public void setIsPatch(String isPatch) {
        this.isPatch = isPatch;
    }
    
    public String getMain() {     
        return main;
    }

    public void setMain(String main) {

        this.main = main;
    }        
    
    public String getIsExec() {
        return isExec;
    }

    public void setIsExec(@Pattern(regexp = "^[tT][rR][uU][eE]|[fF][aA][lL][sS][eE]$", message = ApiError.INVALID_ISEXEC_PARAMETER_VIOLATION)   String isExec) {
        this.isExec = isExec;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @NotNull(message = ApiError.MISSING_VER_PARAMETER_VIOLATION)        
    @Pattern(regexp = semverPattern, message = ApiError.INVALID_VER_PARAMETER_VIOLATION)        
    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    //@NotNull(message = "ufile parameter is required")       
    @Override
    public FileItem getFileItem() {
        return fileItem;
    }   
 
    @Override
    public void setFileItem(FileItem fileItem) {
        this.fileItem = fileItem;
    }    

    @Override
    public PatchMultipartBatch validate() 
    {
        // validate dto
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<PatchMultipartBatch>> violations = validator.validate(this);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        if(this.getIsPatch().equals(RequestParameters.PARAM_TRUE)){
            if(this.getFileItem() == null){
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.MISSING_UFILE_PARAMETER);
            }
        }
        
        if (this.getIsExec() != null) {
            if (this.getIsExec().equals(RequestParameters.PARAM_TRUE) && Utilities.isEmpty(this.getMain())) {
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.MISSING_MAIN_PARAMETER);
            }
        }
        /*
        else{
            if (!Utilities.isEmpty(this.getMain())) {
                throw new ApplicationException(Response.Status.BAD_REQUEST, ApiError.MISSING_ISEXEC_PARAMETER);
            }
        } 
        */
        
        return this;
    }
    
    
    
}
