package org.atlas.applications.modules.multiparts;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.ws.rs.core.Response;
import org.apache.commons.fileupload.FileItem;
import org.atlas.applications.api.Globals;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.core.utilities.Utilities;
import org.atlas.applications.api.RequestParameters;
import org.atlas.applications.api.exceptions.RestApplicationException;

/**
 * Represents an upload multi-part batch item holding fields to upload a new and complete application object.
 * @author ic
 */
public class UploadMultipartBatch extends BaseMultipartBatch
{    
    private String repository;   
    private String isExec = Globals.FALSE;
    
    public UploadMultipartBatch() {
    }

    @NotNull(message = ApiError.MISSING_NAME_PARAMETER_VIOLATION)        
    public String getName() {
        
        return name;        
    }
 
    public void setName(String name) {
        this.name = name;
    }

    @NotNull(message = ApiError.MISSING_REPOSITORY_PARAMETER_VIOLATION)     
    public String getRepository() {
        return repository;
    }
      
    public void setRepository(String repository) {
        this.repository = repository;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }    
    
    @NotNull(message = ApiError.MISSING_ISEXEC_PARAMETER_VIOLATION)    
    @Pattern(regexp = "^[tT][rR][uU][eE]|[fF][aA][lL][sS][eE]$", message = ApiError.INVALID_ISEXEC_PARAMETER_VIOLATION)      
    public String getIsExec() {
        return isExec;
    }
 
    public void setIsExec(String isExec) {
        this.isExec = isExec;
    }

    @NotNull(message = ApiError.MISSING_LANG_PARAMETER_VIOLATION)    
    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @NotNull(message = ApiError.MISSING_VER_PARAMETER_VIOLATION)            
    @Pattern(regexp = semverPattern, message = ApiError.INVALID_VER_PARAMETER_VIOLATION)            
    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    @NotNull(message = ApiError.MISSING_UFILE_PARAMETER_VIOLATION)        
    @Override
    public FileItem getFileItem() {
        return fileItem;
    }   

    @Override
    public void setFileItem(FileItem fileItem) {
        this.fileItem = fileItem;
    }    

    @Override
    public UploadMultipartBatch validate()
    {    
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<UploadMultipartBatch>> violations = validator.validate(this);
        if (!violations.isEmpty()) {            
            throw new ConstraintViolationException(violations);
        }

        if (this.getIsExec() != null) {
            if (this.getIsExec().equals(RequestParameters.PARAM_TRUE) && Utilities.isEmpty(this.getMain())) {
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.MISSING_MAIN_PARAMETER);
            }
        }
        return this;
    }

    
    
}
