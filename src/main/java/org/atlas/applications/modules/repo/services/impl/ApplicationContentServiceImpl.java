package org.atlas.applications.modules.repo.services.impl;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.ws.rs.core.Response;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.atlas.applications.modules.repo.domain.ApplicationContent;
import org.atlas.applications.modules.repo.services.IApplicationContentService;

/**
 * Service class to support ApplicationContent entity operations.
 * @author ic
 */
@LocalBean
@Stateless
public class ApplicationContentServiceImpl extends BaseControllerServiceImpl<ApplicationContent> implements IApplicationContentService
{

    @Override
    public ApplicationContent findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ApplicationContent> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override    
    public EntityManager em() {
        return em;
    }
    
    @Override  
    public List<ApplicationContent> findByApplicationId(long applicationId)
    {
        List<ApplicationContent> appContents = em().createNamedQuery("ApplicationContent.findById", ApplicationContent.class)
                .setParameter("id", applicationId)                
                .getResultList();
        /*
        if (appContents.isEmpty()) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_VERSIONS_NOT_PRESENT);
        } 
        */
        return appContents;
    }  
    
    @Override  
    public List<ApplicationContent> findByApplicationIdExclDeleted(long applicationId)
    {
        List<ApplicationContent> appContents = em().createNamedQuery("ApplicationContent.findByIdExclDeleted", ApplicationContent.class)
                .setParameter("id", applicationId)                
                .getResultList();
        return appContents;
    }        
    
    @Override
    public ApplicationContent findByApplicationIdAndVersion(long applicationId, String version)
    {
        List<ApplicationContent> appContents = em().createNamedQuery("ApplicationContent.findByIdAndVersion", ApplicationContent.class)
                .setParameter("id", applicationId)
                .setParameter("version", version)
                .getResultList();
        if (appContents.isEmpty()) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_VERSION_NOT_FOUND);
        }
        return appContents.get(0);
    }
    
    @Override
    public boolean existsVersion(long applicationId, String version)
    {
        List<ApplicationContent> appContents = em().createNamedQuery("ApplicationContent.findByIdAndVersionExclDeleted", ApplicationContent.class)
                .setParameter("id", applicationId)
                .setParameter("version", version)
                .getResultList();
        if (!appContents.isEmpty()) {
            //throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_VERSION_EXISTS);
            return true;
        }
        return false;
    }  
    
    @Override  
    public boolean containsVersions(long applicationId)
    {
        List<ApplicationContent> appContents = em().createNamedQuery("ApplicationContent.findByIdExclDeleted", ApplicationContent.class)
                .setParameter("id", applicationId)
                .getResultList();
        if (appContents.isEmpty()) {
            return false;
        }
        return true;
    }      
    
    @Override
    public ApplicationContent findByApplicationIdAndVersionInclDeleted(long applicationId, String version)
    {
        List<ApplicationContent> appContents = em().createNamedQuery("ApplicationContent.findByIdAndVersionInclDeleted", ApplicationContent.class)
                .setParameter("id", applicationId)
                .setParameter("version", version)
                .getResultList();
        if (appContents.isEmpty()) {
            throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.APPLICATION_VERSION_NOT_FOUND);
        }
        return appContents.get(0);
    }    
    
    @Override
    public ApplicationContent findByApplicationIdAndVersionExclDeleted(long applicationId, String version)
    {
        List<ApplicationContent> appContents = em().createNamedQuery("ApplicationContent.findByIdAndVersionExclDeleted", ApplicationContent.class)
                .setParameter("id", applicationId)
                .setParameter("version", version)
                .getResultList();
        if (appContents.isEmpty()) {
            return null;
        }
        return appContents.get(0);
    }    

    
}
