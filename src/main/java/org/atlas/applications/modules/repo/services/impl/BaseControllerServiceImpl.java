package org.atlas.applications.modules.repo.services.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.atlas.applications.core.exceptions.InvalidArgumentException;
import org.atlas.applications.modules.repo.services.IBaseControllerService;

/**
 * Base controller service class providing persistence operations.
 * @author ic
 * @param <T>
 */
public abstract class BaseControllerServiceImpl<T extends Serializable> implements IBaseControllerService<T>
{
    
    @PersistenceContext(unitName="org.atlas.applications.repo_atlas-apps-repository_war_1.0.1PU") 
    protected EntityManager em;
    private final Class<T> entityClass;

    public BaseControllerServiceImpl() {
        Type type = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType)type;
        entityClass = (Class)pt.getActualTypeArguments()[0];
    }
        
    @Override
    public final EntityManager getEntityManager() {
        return em;        
    }    
    
    public void setEntityManager(EntityManager entityManager) {
        this.em = entityManager;
    }
    
    @Override
    public T find(Object id) {        
        T found = em.find(entityClass, id);
        if(found != null){
            em.refresh(found);
        }
        //return em.find(entityClass, id);
        return found;
    }

    @Override
    public boolean exists(Object id)
    {
        T t = em.find(entityClass, id);
        return t != null;        
    }        

    @Override
    public abstract T findByParameterSingle(Object param);

    @Override
    public abstract List<T> findByParameterMany(Object param);
    
    @Override
    public <N extends String> T findSingleByParam(N namedQuery, N paramName, Object paramValue)
    {
        List<T> query = getEntityManager()
                .createNamedQuery(namedQuery, entityClass)
                .setParameter(paramName, paramValue)
                .getResultList();
        if(query != null && query.size() == 1)
        {
            return query.get(0);
        }
        return null;
    }
    
    /**
     * Finds and return a list of all Profile entities matching given criteria.
     * @param <N> The named query defined on the entity class.
     * @param namedQuery The named query string.
     * @param paramName The parameter name defined in the named query.
     * @param paramValue the value of the parameter to search.
     * @return 
     */
    @Override
    public <N extends String> List<T> findManyByParam(N namedQuery, N paramName, Object paramValue)
    {
        List<T> query = getEntityManager()
                .createNamedQuery(namedQuery, entityClass)
                .setParameter(paramName, paramValue)
                .getResultList();
        if(query != null && query.size() > 0)
        {
            return query;
        }
        return null;
    }    
    
    /**
     * Finds and return a list of all Profile entities matching given criteria.
     * @param <N> The named query defined on the entity class.
     * @param namedQuery The named query string.
     * @param paramNames The parameter name defined in the named query.
     * @param paramValues the value of the parameter to search.
     * @return 
     */
    @Override
    public <N extends String> T findSingleByTwoParams(N namedQuery, List<N> paramNames, List<Object> paramValues)             
    {
        if(paramNames.size() != 2){
            try {
                throw new InvalidArgumentException("Wrong size of parameter names. Size expected: 2, found: "+paramNames.size());
            } catch (InvalidArgumentException ex) {
                Logger.getLogger(BaseControllerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(paramValues.size() != 2)
        {
            try {
                throw new InvalidArgumentException("Wrong size of parameter values list. Size expected: 2, found: "+paramValues.size());
            } catch (InvalidArgumentException ex) {
                Logger.getLogger(BaseControllerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        List<T> query = getEntityManager()
                .createNamedQuery(namedQuery, entityClass)
                .setParameter(paramNames.get(0), paramValues.get(0))
                .setParameter(paramNames.get(1), paramValues.get(1))
                .getResultList();
        if(query.size() > 0 && query.size() == 1)
        {
            return query.get(0);
        }
        return null;
    }    
        
    /**
     * Finds and returns a list of all entities in the database.
     * @return List<T>
     */
    @Override
    public List<T> findAll() {        
        Query query = em.createQuery("SELECT e FROM ".concat(entityClass.getCanonicalName()).concat(" e"));
	List<T> list = null;
	list = query.getResultList();        
	return list;
    }
    
    /**
     * Persists and returns the created entity instance.
     * @param entity
     * @return T The entity to create and persist.
     */
    @Override
    public T created(T entity) {
      em.persist(entity);      
      return entity;
    }

    /**
     * Updates and returns the updated entity instance.
     * @param entity The entity to update.
     * @return Entity T.
     */
    @Override
    public T updated(T entity) {
      em.merge(entity);
      em.flush();
      em.refresh(em.merge(entity));
      return entity;
    }    

    /**
     * Removes the entity instance.
     * @param entity The entity to remove.
     */    
    @Override
    public void remove(T entity) {	
        em.merge(entity);
        em.refresh(em.merge(entity));
        em.remove(entity);
    }
    
    /**
     * Persists and returns the created entity instance.
     * @param entity
     */
    @Override
    public void create(T entity) {
      em.persist(entity);      
    }

    /**
     * Updates and returns the updated entity instance.
     * @param entity The entity to update.
     */
    @Override
    public void update(T entity) {
      em.merge(entity);
      em.flush();
      em.refresh(em.merge(entity));            
    }    
    
    /**
     * Returns all entity records found using criteria builder API.
     * @return List<T>
     */
    @Override
    public final List<T> findAllCb() {
        javax.persistence.criteria.CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return em.createQuery(cq).getResultList();
    }

    /**
     * Returns all entities using given search range.
     * @param range
     * @return List<T>
     */
    @Override
    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = em.createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    /**
     * Returns a count of of entities found in the underlying database.
     * @return int
     */
    @Override
     public int count() {
        javax.persistence.criteria.CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(em.getCriteriaBuilder().count(rt));
        javax.persistence.Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }    
}
