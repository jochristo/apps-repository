/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.repo.model;

import java.util.List;

/**
 *
 * @author ic
 */
public class MimeTypes {
    public List<Mime> mimes;

    public List<Mime> getMimes() {
        return mimes;
    }

    public void setMimes(List<Mime> mimes) {
        this.mimes = mimes;
    }
    
}
