package org.atlas.applications.modules.repo.domain;

/**
 *
 * @author ic
 */
public enum TransactionType
{
    /**
     * value: 1
     */
    CREATE,
    
    /**
     * value: 2
     */
    UPDATE,
    
    /**
     * value: 3
     */
    DELETE,    
    
     /**
     * value: 4
     */
    ENABLE,    
    
     /**
     * value: 5
     */
    DISABLE,
    
    /**
     * value: 6
     */     
    PATCH,
   
    /**
     * value: 7
     */     
    DOWNLOAD
    
    ;
    
}
