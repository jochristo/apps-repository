package org.atlas.applications.modules.repo.services;

import javax.ejb.Local;

/**
 * Defines methods to perform persistence operations on MimeType objects.
 * @author ic
 */
@Local
public interface IMimeTypeService
{
    /**
     * Pre-inserts all available mime types prior to starting the REST application
     */
    public void preInsertMimeTypes();
}
