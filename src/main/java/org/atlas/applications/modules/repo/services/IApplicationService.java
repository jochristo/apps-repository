package org.atlas.applications.modules.repo.services;

import java.util.List;
import javax.ejb.Local;
import javax.persistence.EntityManager;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;


/**
 * Defines methods to perform persistence operations on Application objects.
 * @author ic
 */
@Local
public interface IApplicationService
{    
    public EntityManager em();
    
    public void upload();
    
    public Application createApplication(Application application);
    
    public List<Application> findAll();
    
    public List<Application> findAllExclDeletedWithContents();
    
    public List<Application> findAll(ApplicationStatus status);
    
    public Application find(long applicationId);
    
    public Application findInclDeleted(long applicationId);
    
    public void update(Application application);
    
    public Application updated(Application application);
    
    public List<Application> findAllPaged(int page, int size);
    
    public List<Application> findAllPaged(ApplicationStatus status, int page, int size);    
    
    
}
