package org.atlas.applications.modules.repo.services.impl;

import com.github.jasminb.jsonapi.JSONAPIDocument;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationContent;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;
import org.atlas.applications.modules.repo.domain.ApplicationTransaction;
import org.atlas.applications.modules.repo.domain.TransactionType;
import org.atlas.applications.modules.repo.dto.ApplicationDto;
import org.atlas.applications.modules.repo.services.IApplicationService;
import org.atlas.applications.modules.repo.services.IApplicationTransactionService;
import org.atlas.applications.modules.repo.services.ITransactional;

/**
 * Service class to support Application Transaction entity operations.
 * @author ic
 */
@LocalBean
@Stateless
public class ApplicationTransactionServiceImpl extends BaseControllerServiceImpl<ApplicationTransaction> implements ITransactional, IApplicationTransactionService
{
    @EJB private IApplicationService applicationService;

    @Override
    public ApplicationTransaction findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ApplicationTransaction> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Creates ApplicationTransaction in persistence layer with given JsonApi document.
     * @param document 
     */
    @Override
    public void createTransaction(JSONAPIDocument<ApplicationDto> document) {
        ApplicationDto dto = document.get();
        ApplicationTransaction applicationTransaction = new ApplicationTransaction();
        applicationTransaction.setApplication(applicationService.em().find(Application.class, dto.getId()));
        applicationTransaction.setCreatedAt(new Date());
        applicationTransaction.setInfo("");
        applicationTransaction.setType(TransactionType.CREATE);
        this.create(applicationTransaction);
    }

    /**
     * Creates ApplicationTransaction of given type in persistence layer with given JsonApi document.
     * @param document 
     * @param type 
     */    
    @Override
    public void createTransaction(JSONAPIDocument<ApplicationDto> document, TransactionType type) {
        ApplicationDto dto = document.get();
        ApplicationTransaction applicationTransaction = new ApplicationTransaction();
        applicationTransaction.setApplication(applicationService.em().find(Application.class, dto.getId()));
        applicationTransaction.setCreatedAt(new Date());
        applicationTransaction.setInfo("");
        applicationTransaction.setType(type);
        this.create(applicationTransaction);
    }
    
    /**
     * Creates ApplicationTransaction of given type in persistence layer with given application id.
     * @param applicationId
     * @param type 
     */
    @Override
    public void createTransaction(long applicationId, TransactionType type) {        
        ApplicationTransaction applicationTransaction = new ApplicationTransaction();
        applicationTransaction.setApplication(applicationService.em().find(Application.class, applicationId));
        applicationTransaction.setCreatedAt(new Date());
        applicationTransaction.setInfo("");
        applicationTransaction.setType(type);
        this.create(applicationTransaction);
    }

    @Override
    public List<ApplicationTransaction> findByApplicationId(long applicationId) {
        return this.getEntityManager().createNamedQuery("ApplicationTransaction.findByApplicationId", ApplicationTransaction.class)
                .setParameter("applicationId", applicationId)
                .getResultList();
    }

    @Override
    public List<ApplicationTransaction> findByApplicationIdAndVersion(long applicationId, long versionId) {
        return this.getEntityManager().createNamedQuery("ApplicationTransaction.findByApplicationIdAndVersion", ApplicationTransaction.class)
                .setParameter("applicationId", applicationId)
                .setParameter("versionId", versionId)                
                .getResultList();
    }

    @Override
    public List<ApplicationTransaction> findByApplicationId(long applicationId, TransactionType type) {
        return this.getEntityManager().createNamedQuery("ApplicationTransaction.findByApplicationIdAndType", ApplicationTransaction.class)
                .setParameter("applicationId", applicationId)
                .setParameter("type", type)
                .getResultList();
    }

    @Override
    public List<ApplicationTransaction> findByApplicationIdAndVersion(long applicationId, long versionId, TransactionType type) {        
        return this.getEntityManager().createNamedQuery("ApplicationTransaction.findByApplicationIdAndVersion", ApplicationTransaction.class)
                .setParameter("applicationId", applicationId)
                .setParameter("versionId", versionId)
                .setParameter("type", type)
                .getResultList();
    }

    @Override
    public List<ApplicationTransaction> findAll(TransactionType type) {
        return this.findAll();
    }

    @Override
    public void createTransaction(long applicationId, ApplicationContent applicationContent, TransactionType type) 
    {
        ApplicationTransaction applicationTransaction = new ApplicationTransaction();
        applicationTransaction.setApplication(applicationService.em().find(Application.class, applicationId));
        applicationTransaction.setCreatedAt(new Date());
        applicationTransaction.setType(type);
        applicationTransaction.setVersionId(applicationContent.getId());
        applicationTransaction.setInfo(applicationContent.getVersion());        
        this.create(applicationTransaction);
    }
    
    @Override
    public void createTransaction(long applicationId, String info, TransactionType type) 
    {
        ApplicationTransaction applicationTransaction = new ApplicationTransaction();
        applicationTransaction.setApplication(applicationService.em().find(Application.class, applicationId));
        applicationTransaction.setCreatedAt(new Date());
        applicationTransaction.setType(type);
        applicationTransaction.setVersionId(0);
        applicationTransaction.setInfo(info);        
        this.create(applicationTransaction);
    }    
    
    @Override
    public void createTransaction(long applicationId, String info, ApplicationStatus status)
    {
        TransactionType transactionType = TransactionType.ENABLE;
        switch (status) {
            case ENABLED :
                transactionType = TransactionType.ENABLE;
                break;
            case DELETED :  
                transactionType = TransactionType.DELETE;
                break;
            case DISABLED :
                transactionType = TransactionType.DISABLE;
                break;                
        }         
        ApplicationTransaction applicationTransaction = new ApplicationTransaction();
        applicationTransaction.setApplication(applicationService.em().find(Application.class, applicationId));
        applicationTransaction.setCreatedAt(new Date());
        applicationTransaction.setType(transactionType);
        applicationTransaction.setVersionId(0);
        applicationTransaction.setInfo(info);        
        this.create(applicationTransaction);
    }    
    
    @Override
    public void createTransaction(long applicationId, ApplicationContent applicationContent, ApplicationStatus status)
    {
        TransactionType transactionType = TransactionType.ENABLE;
        switch (status) {
            case ENABLED :
                transactionType = TransactionType.ENABLE;
                break;
            case DELETED :  
                transactionType = TransactionType.DELETE;
                break;
            case DISABLED :
                transactionType = TransactionType.DISABLE;
                break;                
        }         
        ApplicationTransaction applicationTransaction = new ApplicationTransaction();
        applicationTransaction.setApplication(applicationService.em().find(Application.class, applicationId));
        applicationTransaction.setCreatedAt(new Date());
        applicationTransaction.setType(transactionType);
        applicationTransaction.setVersionId(applicationContent.getId());
        applicationTransaction.setInfo(applicationContent.getVersion());       
        this.create(applicationTransaction);
    }     
    
}
