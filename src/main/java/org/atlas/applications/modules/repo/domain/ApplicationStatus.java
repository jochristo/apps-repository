package org.atlas.applications.modules.repo.domain;

/**
 *
 * @author ic
 */
public enum ApplicationStatus
{
    /**
     * value: 1
     */
    ENABLED,
    /**
     * value: 2
     */
    DISABLED,
    /**
     * value: 3
     */
    DELETED;    
    
}
