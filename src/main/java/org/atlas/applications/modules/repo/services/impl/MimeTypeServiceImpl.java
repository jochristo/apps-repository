package org.atlas.applications.modules.repo.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.atlas.applications.modules.repo.domain.MimeType;
import org.atlas.applications.modules.repo.model.Mime;
import org.atlas.applications.modules.repo.services.IMimeTypeService;

/**
 * Service class to support MimeType entity operations.
 * @author ic
 */
@LocalBean
@Stateless
public class MimeTypeServiceImpl extends BaseControllerServiceImpl<MimeType> implements IMimeTypeService{

    @Override
    public MimeType findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<MimeType> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void preInsertMimeTypes() {
        
        // load mime types resource file
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream is = classLoader.getResourceAsStream("MIME.json");                       
        
        // convert to pojo
        try {
            ObjectMapper mapper = new ObjectMapper(); 
            Mime[] mimes = mapper.readValue(is, Mime[].class);            
            List<Mime> list = Arrays.asList(mimes);
            list.forEach((new Consumer<Mime>()
            {
                // persist to db
                @Override
                public void accept(Mime item) {
                    MimeType mt = new MimeType();
                    mt.setMime(item.getMimeType());
                    mt.setExtension(item.getExtension());
                    em.persist(mt);
                }
            })
            );
        } catch (IOException ex) {
            Logger.getLogger(MimeTypeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
  
    
}
