package org.atlas.applications.modules.repo.domain;

import com.github.zafarkhaja.semver.Version;
import java.util.Comparator;

/**
 *
 * @author ic 
 */
public class SemVerComparator implements Comparator<ApplicationContent> {

    @Override
    public int compare(ApplicationContent o1, ApplicationContent o2) {
        Version v1 = Version.valueOf(o1.getVersion());
        Version v2 = Version.valueOf(o2.getVersion());        
        return v2.compareTo(v1);
    }
    
}
