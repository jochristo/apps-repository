/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.repo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.github.zafarkhaja.semver.Version;
import java.io.Serializable;
import java.util.Comparator;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import org.atlas.applications.core.jsonapi.JSONApiLink;
import org.atlas.applications.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.applications.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.applications.core.jsonapi.annotations.JAPILink;
import org.atlas.applications.core.jsonapi.annotations.JAPIType;
import org.atlas.applications.modules.repo.domain.converters.ApplicationStatusConverter;
import org.atlas.applications.modules.repo.domain.listeners.ApplicationContentListener;

/**
 *
 * @author ic
 */
@Entity
@Table(name="application_contents")
@NamedQueries({
    @NamedQuery(name = "ApplicationContent.findByIdAndVersion", 
        query = "SELECT ac FROM ApplicationContent ac WHERE ac.application.id =:id AND ac.version=:version"),
    @NamedQuery(name = "ApplicationContent.findById", 
        query = "SELECT ac FROM ApplicationContent ac WHERE ac.application.id =:id ORDER BY ac.id ASC"),
    @NamedQuery(name = "ApplicationContent.findByIdAndVersionInclDeleted", 
        query = "SELECT ac FROM ApplicationContent ac WHERE ac.application.id =:id AND ac.version=:version"),    
    @NamedQuery(name = "ApplicationContent.findByIdAndVersionExclDeleted", 
        query = "SELECT ac FROM ApplicationContent ac WHERE ac.application.id =:id AND ac.version=:version AND ac.status != org.atlas.applications.modules.repo.domain.ApplicationStatus.DELETED"),     
    @NamedQuery(name = "ApplicationContent.findByIdExclDeleted", 
        query = "SELECT ac FROM ApplicationContent ac WHERE ac.application.id =:id AND ac.status != org.atlas.applications.modules.repo.domain.ApplicationStatus.DELETED ORDER BY ac.id ASC"),    
})
@EntityListeners(ApplicationContentListener.class)
@JAPIType(type="versions")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"version",
"status",
"mime_type",
"filename",
"main",
"language",
"size",
"checksum",
"isExecutable"
})
public class ApplicationContent implements Serializable, Comparator<Object>
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition="integer(11) AUTO_INCREMENT")
    @JAPIIdentifier
    @JsonIgnore
    private long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="applicationId",columnDefinition="integer(11) not null", nullable = false, referencedColumnName = "id")         
    private Application application;  
        
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="mimeTypeId",columnDefinition="integer(11) not null", nullable = false, referencedColumnName = "id")     
    @JAPIAttribute   
    @JsonIgnore
    private MimeType mimeType;   
    
    @Transient
    @JAPIAttribute
    private String mime_type;
    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }    
    
    @Column(name = "checksum",columnDefinition="char(64) not null", nullable = false)    
    @JAPIAttribute    
    private String checksum;  
    
    @Column(name = "filename", columnDefinition="varchar(255) not null", nullable = false)    
    @JAPIAttribute      
    private String filename;    
    
    @Column(name = "storedname", columnDefinition="varchar(255) not null", nullable = false)        
    private String storedname;     
    
    @Column(name = "version", columnDefinition="varchar(20) not null", nullable = false)    
    @Size(max=20)
    @JAPIAttribute  
    private String version;      

    @Column(name = "isExecutable", columnDefinition="tinyint(1) not null", nullable = false)
    @JAPIAttribute  
    private int isExecutable;   
    
    @Column(name = "main", nullable = true)   
    @JAPIAttribute  
    private String main;     
    
    @Column(name = "size", columnDefinition="bigint(20) not null", nullable = false)          
    @JAPIAttribute  
    private long size;
    
    @Column(name = "status", columnDefinition="tinyint(2) DEFAULT 1 not null", nullable = false)    
    @Convert(converter = ApplicationStatusConverter.class)
    @JAPIAttribute
    private ApplicationStatus status = ApplicationStatus.ENABLED; 
    
    @Column(name = "isBugFree", columnDefinition="tinyint(1) DEFAULT 1 not null", nullable = false)  
    @JsonIgnore
    private int isBugFree = 1;    
    
    @JAPILink(self = "")
    @Transient
    private JSONApiLink link;   
    public JSONApiLink getLink() { return link; }
    public void setLink(JSONApiLink link) { this.link = link; }     

    public ApplicationContent() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public MimeType getMimeType() {
        return mimeType;
    }

    public void setMimeType(MimeType mimeType) {
        this.mimeType = mimeType;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getStoredname() {
        return storedname;
    }

    public void setStoredname(String storedname) {
        this.storedname = storedname;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getIsExecutable() {
        return isExecutable;
    }

    public void setIsExecutable(int isExecutable) {
        this.isExecutable = isExecutable;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public ApplicationStatus getStatus() {
        return status;
    }

    public void setStatus(ApplicationStatus status) {
        this.status = status;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public int getIsBugFree() {
        return isBugFree;
    }

    public void setIsBugFree(int isBugFree) {
        this.isBugFree = isBugFree;
    } 

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }       

    @Override
    public int compare(Object o1, Object o2) {
        ApplicationContent content1 = (ApplicationContent) o1;
        ApplicationContent content2 = (ApplicationContent) o2;
        Version v1 = Version.valueOf(content1.getVersion());
        Version v2 = Version.valueOf(content2.getVersion());        
        return v2.compareTo(v1);
    }
    
}
