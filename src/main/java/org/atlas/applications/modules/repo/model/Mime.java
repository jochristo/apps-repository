/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.repo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ic
 */
public class Mime {
    
    @JsonProperty("extension")
    public String extension;
    @JsonProperty("mimeType")
    public String mimeType;

    public Mime() {
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    
    
    
}
