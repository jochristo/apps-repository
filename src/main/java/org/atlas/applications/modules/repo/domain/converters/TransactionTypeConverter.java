
package org.atlas.applications.modules.repo.domain.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.atlas.applications.modules.repo.domain.TransactionType;

/**
 *
 * @author ic
 */
@Converter
public class TransactionTypeConverter implements AttributeConverter<TransactionType, Integer>
{
    @Override
    public Integer convertToDatabaseColumn(TransactionType x) {
        switch (x) {
            case CREATE:
                return 1;
            case UPDATE:
                return 2;
            case DELETE:
                return 3;
            case ENABLE:
                return 4;                 
            case DISABLE:
                return 5;    
            case PATCH:
                return 6;                 
            case DOWNLOAD:
                return 7;                 
            default:
                throw new IllegalArgumentException("Unknown" + x);
        }
    }

    @Override
    public TransactionType convertToEntityAttribute(Integer y) {
        switch (y) {
            case 1:
                return TransactionType.CREATE;
            case 2:
                return TransactionType.UPDATE;
            case 3:
                return TransactionType.DELETE;
            case 4:
                return TransactionType.ENABLE;      
            case 5:
                return TransactionType.DISABLE;  
            case 6:
                return TransactionType.PATCH;   
            case 7:
                return TransactionType.DOWNLOAD;
            default:
                throw new IllegalArgumentException("Unknown" + y);
        }
    }   
}
