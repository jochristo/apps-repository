package org.atlas.applications.modules.repo.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;
import javax.persistence.Convert;
import org.atlas.applications.api.ApiEndpoint;
import org.atlas.applications.api.annotations.APIVersion;
import org.atlas.applications.core.jsonapi.JSONApiLink;
import org.atlas.applications.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.applications.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.applications.core.jsonapi.annotations.JAPILink;
import org.atlas.applications.core.jsonapi.annotations.JAPIRelationship;
import org.atlas.applications.core.jsonapi.annotations.JAPIType;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;
import org.atlas.applications.modules.repo.domain.ApplicationTransaction;
import org.atlas.applications.modules.repo.domain.converters.ApplicationStatusConverter;

/**
 * Represents an application repository DTO instance.
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"repository",
"appname",
"status",
"mime_type",
"language",
"main",
"version",
"version_status",
"checksum",
"size"
})
@JAPIType(type="applications")
@APIVersion(ApiEndpoint.API_PATH_V1)
public class ApplicationDto
{         
    @JAPIIdentifier
    @JsonIgnore
    private Long id;       

    @JAPIAttribute
    private String repository;

    @JAPIAttribute
    private String appname;    

    @Convert(converter = ApplicationStatusConverter.class)
    @JAPIAttribute
    private ApplicationStatus status;    
   
    @JsonProperty("mime_type")
    @JAPIAttribute(name = "mime_type")
    private String mimeType;

    @JAPIAttribute
    private String language;

    @JAPIAttribute
    private String main;

    @JAPIAttribute
    private String version;    

    @JsonProperty("version_status")
    @Convert(converter = ApplicationStatusConverter.class)
    @JAPIAttribute(name = "version_status")
    private ApplicationStatus versionStatus;  

    @JAPIAttribute
    private String checksum;
    
    @JsonIgnore
    private String hash;

    @JAPIAttribute
    private long createdBy;

    @JAPIAttribute
    @JsonIgnore
    private long isCompressed;
    
    @JsonProperty("size")
    @JAPIAttribute
    private long size;     
    
    @JsonIgnore
    @JAPIRelationship(relationship = ApplicationContentDto.class, selfLink = "")    
    private List<ApplicationContentDto> contents;
   
    @JsonIgnore
    @JAPIRelationship(relationship = ApplicationTransactionDto.class, selfLink = "")     
    private List<ApplicationTransactionDto> transactions;
    
    @JsonIgnore
    @JAPIRelationship(relationship = ApplicationTransaction.class, selfLink = "")
    private List<ApplicationTransaction> applicationTransactions;   
    public List<ApplicationTransaction> getApplicationTransactions() {
        return applicationTransactions;
    }

    public void setApplicationTransactions(List<ApplicationTransaction> applicationTransactions) {
        this.applicationTransactions = applicationTransactions;
    }       

    @JAPILink(self = "")
    private JSONApiLink link;
   
    public JSONApiLink getLink() {
        return link;
    }

    public void setLink(JSONApiLink link) {
        this.link = link;
    }   
    
    public ApplicationStatus getStatus() {
        return status;
    }

    public void setStatus(ApplicationStatus status) {
        this.status = status;
    }          
        
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    @JsonProperty("mime_type")
    public String getMimeType() {
        return mimeType;
    }    

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }
        
    public String getHash() {
        return hash;
    }
    
    public void setHash(String hash) {
        this.hash = hash;
    }

    public long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(long createdBy) {
        this.createdBy = createdBy;
    }

    public long getIsCompressed() {
        return isCompressed;
    }

    public void setIsCompressed(long isCompressed) {
        this.isCompressed = isCompressed;
    }    

    public List<ApplicationContentDto> getContents() {
        return contents;
    }

    public void setContents(List<ApplicationContentDto> contents) {
        this.contents = contents;
    }

    public List<ApplicationTransactionDto> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<ApplicationTransactionDto> transactions) {
        this.transactions = transactions;
    }
   
    public ApplicationStatus getVersionStatus() {
        return versionStatus;
    }

    public void setVersionStatus(ApplicationStatus versionStatus) {
        this.versionStatus = versionStatus;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
    
    

}
