package org.atlas.applications.modules.repo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import org.atlas.applications.api.ApiEndpoint;
import org.atlas.applications.api.annotations.APIVersion;
import org.atlas.applications.core.jsonapi.JSONApiLink;
import org.atlas.applications.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.applications.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.applications.core.jsonapi.annotations.JAPILink;
import org.atlas.applications.core.jsonapi.annotations.JAPIRelationship;
import org.atlas.applications.core.jsonapi.annotations.JAPIType;
import org.atlas.applications.modules.repo.domain.converters.ApplicationStatusConverter;
import org.atlas.applications.modules.repo.domain.listeners.ApplicationListener;

/**
 * Represents a gateway application entity.
 * @author ic
 */
@Entity
@Table(name="application")
@NamedQueries({
    @NamedQuery(name="Application.findAll", query = "SELECT a FROM Application a WHERE a.applicationStatus != org.atlas.applications.modules.repo.domain.ApplicationStatus.DELETED"),    
    @NamedQuery(name="Application.findAllWithContents", query = "SELECT DISTINCT a FROM Application a INNER JOIN a.applicationContents c WHERE a.applicationStatus != org.atlas.applications.modules.repo.domain.ApplicationStatus.DELETED"),        
    @NamedQuery(name="Application.findById", query = "SELECT a FROM Application a WHERE a.id =:id AND a.applicationStatus != org.atlas.applications.modules.repo.domain.ApplicationStatus.DELETED"),
    @NamedQuery(name="Application.findByIdInclDeleted", query = "SELECT a FROM Application a WHERE a.id =:id"),
    @NamedQuery(name="Application.findByStatus", query = "SELECT a FROM Application a WHERE a.applicationStatus =:status"),
    @NamedQuery(name="Application.findByIdAndStatus", query = "SELECT a FROM Application a WHERE a.id =:id AND a.applicationStatus =:status"),
    @NamedQuery(name="Application.findByName", query = "SELECT a FROM Application a WHERE a.name =:name"),
    @NamedQuery(name="Application.findByRepository", query = "SELECT a FROM Application a WHERE a.repository =:repository"),
    @NamedQuery(name="Application.findByIdInclContents", query = "SELECT a FROM Application a INNER JOIN a.applicationContents c WHERE c.status != org.atlas.applications.modules.repo.domain.ApplicationStatus.DELETED "
        + "AND a.id =:id AND a.applicationStatus != org.atlas.applications.modules.repo.domain.ApplicationStatus.DELETED"),    
    @NamedQuery(name="Application.count", query="SELECT COUNT(a.id) FROM Application a WHERE a.applicationStatus != org.atlas.applications.modules.repo.domain.ApplicationStatus.DELETED"),
    @NamedQuery(name="Application.countByStatus", query="SELECT COUNT(a.id) FROM Application a WHERE a.applicationStatus =:status"),    
    @NamedQuery(name="Application.findByIdLoadAllRelationships", query = "SELECT a FROM Application a INNER JOIN a.applicationContents c INNER JOIN a.applicationTransactions tr WHERE c.status != org.atlas.applications.modules.repo.domain.ApplicationStatus.DELETED "
        + "AND a.id =:id AND a.applicationStatus != org.atlas.applications.modules.repo.domain.ApplicationStatus.DELETED"),    
})
@EntityListeners(ApplicationListener.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "repository",
    "appname",
    "status",
    "language",
    "isCompressed",
})
@JAPIType(type="applications")
@APIVersion(ApiEndpoint.API_PATH_V2)
public class Application implements Serializable
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition="integer(11) AUTO_INCREMENT")
    @JsonIgnore
    @JAPIIdentifier
    private long id;

    @Column(name = "createdBy", columnDefinition="integer(11) DEFAULT 0 NOT NULL", nullable = false)     
    private long createdBy = 0;       
       
    @Column(name = "name",nullable = false)
    @Basic(optional = false)
    @Size(max = 255)    
    @JAPIAttribute(name = "appname")
    @JsonProperty("appname")
    private String name;    

    @Column(name = "repository",nullable = false)
    @Basic(optional = false)
    @Size(max = 255)       
    @JAPIAttribute
    private String repository;        
    
    @Column(name = "createdAt",nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    //@JsonSerialize(using = JsonDateFormatSerializer.class)
    private Date createdAt;
    
    @Column(name = "updatedAt", insertable=false, updatable=true)
    @Temporal(value = TemporalType.TIMESTAMP)
    //@JsonSerialize(using = JsonDateFormatSerializer.class)
    private Date updatedAt; 

    @Column(name = "isCompressed", columnDefinition="tinyint(1) NOT NULL", nullable = false)      
    @JsonIgnore
    private int isCompressed;    
    
    @Column(name = "lang", nullable = false)    
    @JAPIAttribute
    private String language;   
      
    @Column(name = "hash_val", columnDefinition="char(64) NOT NULL", nullable = false)   
    @Basic(optional = false)
    @Size(max=64)
    @JsonIgnore
    private String hash;  
    
    @Column(name = "status",columnDefinition="tinyint(2) DEFAULT 1 NOT NULL", nullable = false)    
    @Convert(converter = ApplicationStatusConverter.class)
    @JAPIAttribute(name = "status")
    @JsonProperty("status")
    private ApplicationStatus applicationStatus = ApplicationStatus.ENABLED;  
    
    @OneToMany(mappedBy="application",fetch=FetchType.LAZY, cascade = CascadeType.DETACH)
    @JsonIgnore
    @JAPIRelationship(relationship = ApplicationContent.class, selfLink = "")
    private List<ApplicationContent> applicationContents;     
    
    @OneToMany(mappedBy="application",fetch=FetchType.LAZY, cascade = CascadeType.DETACH)
    @JsonIgnore
    @JAPIRelationship(relationship = ApplicationTransaction.class, selfLink = "")
    private List<ApplicationTransaction> applicationTransactions;   
    
    @JAPILink(self = "")
    @Transient
    private JSONApiLink link;   
    public JSONApiLink getLink() { return link; }
    public void setLink(JSONApiLink link) { this.link = link; }    

    public Application() {
    }       

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(long createdBy) {
        this.createdBy = createdBy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }   
    
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getIsCompressed() {
        return isCompressed;
    }

    public void setIsCompressed(int isCompressed) {
        this.isCompressed = isCompressed;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public ApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(ApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public List<ApplicationContent> getApplicationContents() {
        return applicationContents;
    }

    public void setApplicationContents(List<ApplicationContent> applicationContents) {
        this.applicationContents = applicationContents;
    }

    public List<ApplicationTransaction> getApplicationTransactions() {
        return applicationTransactions;
    }

    public void setApplicationTransactions(List<ApplicationTransaction> applicationTransactions) {
        this.applicationTransactions = applicationTransactions;
    }
    
    
    
}
