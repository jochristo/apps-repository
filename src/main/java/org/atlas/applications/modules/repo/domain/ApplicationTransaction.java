/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.repo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import org.atlas.applications.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.applications.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.applications.core.jsonapi.annotations.JAPIType;
import org.atlas.applications.core.jsonapi.serializers.JsonDateFormatSerializer;
import org.atlas.applications.modules.repo.domain.converters.TransactionTypeConverter;

/**
 *
 * @author ic
 */
@Entity
@Table(name="application_transactions")
@NamedQueries({
    @NamedQuery(name="ApplicationTransaction.findAll", query = "SELECT at FROM ApplicationTransaction at"),        
    @NamedQuery(name="ApplicationTransaction.findByType", query = "SELECT at FROM ApplicationTransaction at WHERE at.type =:type"),
    @NamedQuery(name="ApplicationTransaction.findByApplicationId", query = "SELECT at FROM ApplicationTransaction at WHERE at.application.id =:applicationId"),
    @NamedQuery(name="ApplicationTransaction.findByApplicationIdAndType", query = "SELECT at FROM ApplicationTransaction at WHERE at.application.id =:applicationId AND at.type =:type"),
    @NamedQuery(name="ApplicationTransaction.findByApplicationIdAndVersion", query = "SELECT at FROM ApplicationTransaction at WHERE at.application.id =:applicationId AND at.versionId =:versionId"),
    @NamedQuery(name="ApplicationTransaction.findByApplicationIdAndVersionAndType", query = "SELECT at FROM ApplicationTransaction at WHERE at.application.id =:applicationId AND at.versionId =:versionId AND at.type =:type"),
})
@JAPIType(type="transactions")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"type",    
"transactionBy",
"createdAt",
"versionId",
})
public class ApplicationTransaction implements Serializable
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition="integer(11) AUTO_INCREMENT")    
    @JsonIgnore
    @JAPIIdentifier    
    private long id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="applicationId",columnDefinition="integer(11) not null", nullable = false, referencedColumnName = "id")     
    private Application application; 
    
    @Column(name = "transactionBy", columnDefinition="integer(11) DEFAULT 0 not null", nullable = false)
    @JAPIAttribute
    private long transactionBy = 0; // default temporarily     
    
    @Column(name = "createdAt", insertable=true, updatable=false, nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateFormatSerializer.class)
    @JAPIAttribute
    private Date createdAt;   
    
    @Column(name = "type", columnDefinition="tinyint(2) not null", nullable = false)            
    @Convert(converter = TransactionTypeConverter.class)
    @JAPIAttribute
    private TransactionType type;      
        
    @Column(name = "info",columnDefinition="varchar(2500) not null", nullable = false)
    @Size(max=2500)    
    @JAPIAttribute
    private String info;     
    
    @Column(name = "versionId", columnDefinition="integer(11) DEFAULT 0 NOT NULL", nullable = false) 
    @JAPIAttribute
    private long versionId;

    public ApplicationTransaction() {
    }    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public long getTransactionBy() {
        return transactionBy;
    }

    public void setTransactionBy(long transactionBy) {
        this.transactionBy = transactionBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }    
    
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public long getVersionId() {
        return versionId;
    }

    public void setVersionId(long versionId) {
        this.versionId = versionId;
    }
    
    
}
