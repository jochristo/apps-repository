/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.repo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import org.atlas.applications.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.applications.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.applications.core.jsonapi.annotations.JAPIType;

/**
 *
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"applicationId",    
"type",
"transactionBy",
"info"
})
@Type(value="transactions")
@JAPIType(type = "transactions")
public class ApplicationTransactionDto {
    
    @Id
    @JsonProperty("id")
    @JAPIIdentifier
    private String id; 
    
    @JsonProperty("applicationId")
    @JAPIAttribute    
    private long applicationId;

    @JsonProperty("type")
    @JAPIAttribute    
    private int type;

    @JsonProperty("transactionBy")
    @JAPIAttribute    
    private long transactionBy;

    @JsonProperty("info")
    @JAPIAttribute    
    private String info;    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(long applicationId) {
        this.applicationId = applicationId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTransactionBy() {
        return transactionBy;
    }

    public void setTransactionBy(long transactionBy) {
        this.transactionBy = transactionBy;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    
    
}
