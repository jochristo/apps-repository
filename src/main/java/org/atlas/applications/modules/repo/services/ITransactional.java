/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.repo.services;

import com.github.jasminb.jsonapi.JSONAPIDocument;
import javax.ejb.Local;
import org.atlas.applications.modules.repo.domain.ApplicationContent;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;
import org.atlas.applications.modules.repo.domain.TransactionType;
import org.atlas.applications.modules.repo.dto.ApplicationDto;

/**
 * Defines methods to create application/version transaction entries in persistence store.
 * @author ic
 */
@Local
public interface ITransactional {
    
    public void createTransaction(JSONAPIDocument<ApplicationDto> document);
    
    public void createTransaction(JSONAPIDocument<ApplicationDto> document, TransactionType type);
    
    public void createTransaction(long applicationId, TransactionType type);    
    
    public void createTransaction(long applicationId, ApplicationContent applicationContent, TransactionType type);     
        
    public void createTransaction(long applicationId, String info, TransactionType type); 
    
    public void createTransaction(long applicationId, String info, ApplicationStatus status); 
    
    public void createTransaction(long applicationId, ApplicationContent applicationContent, ApplicationStatus status);     
}
