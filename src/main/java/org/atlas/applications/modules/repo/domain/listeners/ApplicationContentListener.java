package org.atlas.applications.modules.repo.domain.listeners;

import javax.persistence.PostLoad;
import org.atlas.applications.modules.repo.domain.ApplicationContent;

/**
 * ApplicationContent entity listener class.
 * @author ic
 */
public class ApplicationContentListener {
    
    @PostLoad
    public void postLoad(ApplicationContent ac)
    {
        if(ac != null){
            if(ac.getMimeType() != null){
            ac.setMime_type(ac.getMimeType().getMime());
            }
        }
    }
}
