/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.repo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ic
 */
@Entity
@Table(name="mime_types")
@NamedQueries({
    @NamedQuery(name="MimeType.findByMime", query ="Select m FROM MimeType m WHERE m.mime = :mime"),
    @NamedQuery(name="MimeType.findByMimeAndExtension", query ="Select m FROM MimeType m WHERE m.mime =:mime AND m.extension =:extension"),
})
public class MimeType implements Serializable
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition="integer(11) AUTO_INCREMENT")        
    @JsonIgnore
    private long id;

    @Column(name = "mime", columnDefinition="varchar(255) not null")
    @NotNull
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mime; 
    
    @Column(name = "extension", columnDefinition="varchar(255) not null")
    @NotNull   
    @JsonIgnore
    private String extension; 

    @Column(name = "encoding", columnDefinition="varchar(255)")    
    @JsonIgnore
    private String encoding; 
    
    @OneToMany(mappedBy="mimeType")
    @JsonIgnore
    private List<ApplicationContent> applicationContents;    

    public MimeType() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public List<ApplicationContent> getApplicationContents() {
        return applicationContents;
    }

    public void setApplicationContents(List<ApplicationContent> applicationContents) {
        this.applicationContents = applicationContents;
    }
    
    
}
