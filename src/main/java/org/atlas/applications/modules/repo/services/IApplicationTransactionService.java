package org.atlas.applications.modules.repo.services;

import java.util.List;
import javax.ejb.Local;
import org.atlas.applications.modules.repo.domain.ApplicationTransaction;
import org.atlas.applications.modules.repo.domain.TransactionType;

/**
 * Defines methods to perform persistence operations on ApplicationTransaction objects.
 * @author ic
 */
@Local
public interface IApplicationTransactionService
{
    public List<ApplicationTransaction> findAll();
    
    public List<ApplicationTransaction> findAll(TransactionType type);
    
    public List<ApplicationTransaction> findByApplicationId(long applicationId);    
    
    public List<ApplicationTransaction> findByApplicationIdAndVersion(long applicationId, long versionId);
    
    public List<ApplicationTransaction> findByApplicationId(long applicationId, TransactionType type);    
    
    public List<ApplicationTransaction> findByApplicationIdAndVersion(long applicationId, long versionId, TransactionType type);    
}
