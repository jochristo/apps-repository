package org.atlas.applications.modules.repo.domain.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;
/**
 *
 * @author ic
 */
@Converter
public class ApplicationStatusConverter implements AttributeConverter<ApplicationStatus, Integer>
{

    @Override
    public Integer convertToDatabaseColumn(ApplicationStatus x) {
        switch (x) {
            case ENABLED:
                return 1;
            case DISABLED:
                return 2;
            case DELETED:
                return 3;
            default:
                throw new IllegalArgumentException("Unknown" + x);
        }
    }

    @Override
    public ApplicationStatus convertToEntityAttribute(Integer y) {
        switch (y) {
            case 1:
                return ApplicationStatus.ENABLED;
            case 2:
                return ApplicationStatus.DISABLED;
            case 3:
                return ApplicationStatus.DELETED;
            default:
                throw new IllegalArgumentException("Unknown" + y);
        }
    }
    
}
