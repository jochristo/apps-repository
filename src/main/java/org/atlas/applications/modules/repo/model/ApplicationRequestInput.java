/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.repo.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.apache.commons.fileupload.FileItem;
import org.atlas.applications.api.annotations.RequiredParam;

/**
 * Holds the uploaded multi part items from form parameters.
 * @author ic
 */
public class ApplicationRequestInput {
 
    //@NotNull(message = "name parameter is required")    
    //@RequiredParam
    public String name;
    
    @NotNull(message = "repository parameter is required")    
    @RequiredParam
    public String repository;    
    
    @NotNull(message = "isExec parameter is required")    
    @Pattern(regexp = "^[tT][rR][uU][eE]|[fF][aA][lL][sS][eE]$", message = "isExec parameter value should be one of: true|false")
    @RequiredParam
    public String isExec;
        
    @NotNull(message = "main parameter is required")
    public String main;
    
    @NotNull(message = "lang parameter is required")    
    @RequiredParam
    public String lang;
        
    @NotNull(message = "ver parameter is required")    
    //@Pattern(regexp = "^[1-9][.][0-9][.][0-9]$", message = "Versioning must follow Semantic Versioning Specification (SemVer)")
    @Pattern(regexp = "^(([1-9]+)\\.([0-9]+)(?:\\.)?([0-9]*)(\\.|-|\\+)?([0-9A-Za-z-.]*)?)$", message = "Versioning must follow Semantic Versioning Specification (SemVer)")
    @RequiredParam
    public String ver;     
    
    @RequiredParam
    @NotNull(message = "ufile parameter is required")
    public FileItem fileItem;
    
    @RequiredParam
    @NotNull(message = "isPatch parameter is required")    
    public String isPatch = "false";

    //@NotNull(message = "name parameter is required")    
    //@RequiredParam    
    public String getName() {
        return name;
    }

    //@NotNull(message = "name parameter is required")    
    //@RequiredParam
    public void setName(String name) {
        this.name = name;
    }

    @NotNull(message = "repository parameter is required")    
    @RequiredParam
    public String getRepository() {
        return repository;
    }

    @NotNull(message = "repository parameter is required")    
    @RequiredParam
    public void setRepository(String repository) {
        this.repository = repository;
    }    
    
    public String getIsExec() {
        return isExec;
    }

    public void setIsExec(String isExec) {
        this.isExec = isExec;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public FileItem getFileItem() {
        return fileItem;
    }

    public void setFileItem(FileItem fileItem) {
        this.fileItem = fileItem;
    }

    public String getIsPatch() {
        return isPatch;
    }

    public void setIsPatch(String isPatch) {
        this.isPatch = isPatch;
    }        
    
    public ApplicationRequestInput() {
        
    }
    
    
    
}
