/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.repo.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import javax.persistence.Convert;
import org.atlas.applications.core.jsonapi.JSONApiLink;
import org.atlas.applications.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.applications.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.applications.core.jsonapi.annotations.JAPILink;
import org.atlas.applications.core.jsonapi.annotations.JAPIType;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;
import org.atlas.applications.modules.repo.domain.converters.ApplicationStatusConverter;

/**
 *
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"appname",
"version",
"status",
"mime_type",
"filename",
"main",
"language",
"size",
"checksum",
"isExecutable"
})
@JAPIType(type = "versions")
public class ApplicationContentDto 
{    
    @JsonProperty("id")
    @JAPIIdentifier
    private String id;

    @JsonProperty("appname")    
    @JAPIAttribute
    private String appname;
    
    @JsonProperty("main")
    @JAPIAttribute
    private String main;  

    @JsonProperty("mime_type")
    @JAPIAttribute(name = "mime_type")
    private String mimeType;

    @JsonProperty("filename")
    @JAPIAttribute
    private String filename;
    
    @JsonProperty("checksum")
    @JAPIAttribute
    private String checksum;    

    @JsonProperty("storedName")
    @JsonIgnore
    private String storedName;

    @JsonProperty("version")
    @JAPIAttribute
    private String version;   
    
    @JsonProperty("size")
    @JAPIAttribute
    private long size;    
    
    @JsonProperty("status")
    @Convert(converter = ApplicationStatusConverter.class)
    @JAPIAttribute
    private ApplicationStatus status;
           
    @JsonProperty("isExecutable")
    @JAPIAttribute
    private int isExecutable;    
    
    @JAPILink(self = "")
    private JSONApiLink link;    

    public JSONApiLink getLink() {
        return link;
    }

    public void setLink(JSONApiLink link) {
        this.link = link;
    }     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String application) {
        this.appname = application;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }       

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }    

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getStoredName() {
        return storedName;
    }

    public void setStoredName(String storedName) {
        this.storedName = storedName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public ApplicationStatus getStatus() {
        return status;
    }

    public void setStatus(ApplicationStatus status) {
        this.status = status;
    } 

    public int getIsExecutable() {
        return isExecutable;
    }

    public void setIsExecutable(int isExecutable) {
        this.isExecutable = isExecutable;
    }
    
    
}
