package org.atlas.applications.modules.repo.services.impl;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;
import org.atlas.applications.modules.repo.services.IApplicationService;

/**
 * Service class to support Application entity operations.
 * @author ic
 */
@LocalBean
@Stateless
public class ApplicationServiceImpl extends BaseControllerServiceImpl<Application> implements IApplicationService
{ 

    @Override
    public Application findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Application> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void upload() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Application createApplication(Application application) {        
        return created(application);
    }
    
    @Override
    public EntityManager em() {
        return em;
    }    

    @Override
    public List<Application> findAll(ApplicationStatus status) {
        return getEntityManager().createNamedQuery("Application.findByStatus", Application.class)
                .setParameter("status",status)
                .getResultList();
    }   

    @Override
    public Application find(long applicationId) {
        //return getEntityManager().find(Application.class, applicationId);
        List<Application> list =  getEntityManager().createNamedQuery("Application.findById", Application.class)
                .setParameter("id",applicationId)
                .getResultList();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }
    
    public Application findInclContents(long applicationId) {
        //return getEntityManager().find(Application.class, applicationId);
        List<Application> list =  getEntityManager().createNamedQuery("Application.findByIdInclContents", Application.class)
                .setParameter("id",applicationId)
                .getResultList();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }    
    
    public Application findLoadRelationships(long applicationId) {
        //return getEntityManager().find(Application.class, applicationId);
        List<Application> list =  getEntityManager().createNamedQuery("Application.findByIdLoadAllRelationships", Application.class)
                .setParameter("id",applicationId)
                .getResultList();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }     
    
    @Override
    public Application findInclDeleted(long applicationId) {
        //return getEntityManager().find(Application.class, applicationId);
        List<Application> list =  getEntityManager().createNamedQuery("Application.findByIdInclDeleted", Application.class)
                .setParameter("id",applicationId)
                .getResultList();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }    

    @Override
    public List<Application> findAll() {
        return getEntityManager().createNamedQuery("Application.findAll", Application.class)
                .getResultList();
    }
    
    @Override
    public List<Application> findAllExclDeletedWithContents() {        
        return getEntityManager().createNamedQuery("Application.findAllWithContents", Application.class)
               .getResultList();

    }    

    @Override
    public void update(Application entity) {
        super.update(entity); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Application updated(Application entity) {
        return super.updated(entity); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Returns all Application instances with pagination.
     * @param page
     * @param size
     * @return 
     */
    @Override
    public List<Application> findAllPaged(int page, int size) {
        return getEntityManager().createNamedQuery("Application.findAll", Application.class)
            .setFirstResult((page-1)*size)
            .setMaxResults(size) 
            .getResultList();
    }

    /**
     * Returns all Application instances of given status with pagination.
     * @param status
     * @param page
     * @param size
     * @return 
     */
    @Override
    public List<Application> findAllPaged(ApplicationStatus status, int page, int size) {
        return getEntityManager().createNamedQuery("Application.findByStatus", Application.class)
            .setFirstResult((page-1)*size)
            .setMaxResults(size)                 
            .setParameter("status",status)
            .getResultList();
    }
    
    public Long getCount()
    {
        Query query = em.createNamedQuery("Application.count", Application.class);                
        return (Long)query.getSingleResult();
    }    
   
    public Long getCount(ApplicationStatus status)
    {
        Query query = em.createNamedQuery("Application.countByStatus", Application.class)
            .setParameter("status", status);                        
        return (Long)query.getSingleResult();
    }     
    
}
