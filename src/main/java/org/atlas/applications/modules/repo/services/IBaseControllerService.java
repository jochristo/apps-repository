package org.atlas.applications.modules.repo.services;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * Defines the persistence methods on entity service classes.
 * @author ic
 * @param <T>
 */
public abstract interface IBaseControllerService<T extends Serializable> {
    
    public EntityManager getEntityManager();    
    
    public T find(Object id);   
    
    public T findByParameterSingle(Object param);
    
    public List<T> findByParameterMany(Object param);
    
    public List<T> findAll();
    
    public List<T> findRange(int[] range);
    
    public List<T> findAllCb();
    
    public int count();
    
    public <N extends String> T findSingleByParam(N namedQuery, N paramName, Object paramValue);
    
    public <N extends String> List<T> findManyByParam(N namedQuery, N paramName, Object paramValue);
    
    public <N extends String> T findSingleByTwoParams(N namedQuery, List<N> paramNames, List<Object> paramValues);  
    
    public T created(T entity);
    
    public T updated(T entity); 
    
    public void remove(T entity);
    
    public void create(T entity);
    
    public void update(T entity);
    
    public boolean exists(Object id);

}
