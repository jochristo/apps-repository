package org.atlas.applications.modules.repo.services;

import java.util.List;
import javax.ejb.Local;
import javax.persistence.EntityManager;
import org.atlas.applications.modules.repo.domain.ApplicationContent;

/**
 * Defines methods to perform persistence operations on ApplicationContent objects.
 * @author ic
 */
@Local
public interface IApplicationContentService
{    
    public EntityManager em();  
    
    public ApplicationContent findByApplicationIdAndVersion(long applicationId, String version);
    
    public boolean existsVersion(long applicationId, String version);
    
    public List<ApplicationContent> findByApplicationId(long applicationId);
    
    public List<ApplicationContent> findByApplicationIdExclDeleted(long applicationId);
    
    public boolean containsVersions(long applicationId);
    
    public ApplicationContent findByApplicationIdAndVersionInclDeleted(long applicationId, String version);
    
    public ApplicationContent findByApplicationIdAndVersionExclDeleted(long applicationId, String version);
}
