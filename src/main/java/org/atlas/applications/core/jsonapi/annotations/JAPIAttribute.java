package org.atlas.applications.core.jsonapi.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface JAPIAttribute {
    public String name() default "";	
}
