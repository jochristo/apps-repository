package org.atlas.applications.core.jsonapi.extensions;

import java.util.HashSet;
import java.util.Set;
import org.atlas.applications.core.jsonapi.JSONApiRelationship;

/**
 * Custom object for current project to support more that one relationships object.
 * @author ic
 */
public class JSONApiRelationshipRoot {
    

    private Set<JSONApiRelationship> items;

    public JSONApiRelationshipRoot() {
        items = new HashSet();        
    }    
    
    public void addRelationshipItem(JSONApiRelationship item)
    {
        this.items.add(item);
    }

    public Set<JSONApiRelationship> getItems() {
        return items;
    }

    public void setItems(Set<JSONApiRelationship> items) {
        this.items = items;
    }    
        

    
    
}
