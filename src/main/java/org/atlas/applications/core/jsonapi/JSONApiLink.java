package org.atlas.applications.core.jsonapi;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import org.atlas.applications.core.jsonapi.annotations.JAPILink;
import org.atlas.applications.core.jsonapi.helpers.RelatedLink;

/**
 * JSON API Implementation - Links
 * @author Konstantinos Antonopoulos <a href="mailto:konsantonop@gmail.com">Konstantinos Antonopoulos</a>
 * @version 0.1 
 * @category System core ( JSON API Schema Implementation )
 * @since 2016-08-05
 * @see http://jsonapi.org/format/#document-links
 **/

@JAPILink(self = "")
public class JSONApiLink implements Serializable{
	
    private String self;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private RelatedLink related;

    private String first;
    private String prev;
    private String next;
    private String last;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public RelatedLink getRelated() {
        return related;
    }

    public void setRelated(RelatedLink related) {
        this.related = related;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getPrev() {
        return prev;
    }

    public void setPrev(String prev) {
        this.prev = prev;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }         
}
