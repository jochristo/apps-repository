package org.atlas.applications.core.jsonapi;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.atlas.applications.core.exceptions.InvalidArgumentException;
import org.atlas.applications.core.jsonapi.annotations.JAPILink;
import org.atlas.applications.core.jsonapi.annotations.JAPIRelationship;

public class JSONApiDocument {

    private Object data;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ArrayList<JSONApiErrors> errors;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object meta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private JSONApiLink links;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object included;

    private JSONApi jsonapi = new JSONApi();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object relationships;

    public JSONApiDocument() {
    }

    public JSONApiDocument(Object obj) {

        if (obj == null) {
            this.data = new JSONApiData();
            return;
        }

        if ((obj instanceof ArrayList)) {
            ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
            int arraySize = ((ArrayList) obj).size();
            for (int i = 0; i < arraySize; i++) {
                Object curObj = ((ArrayList) obj).get(i);
                list.add(new JSONApiData().convertToJSONApiData(curObj));
                
                // read and attach included relationships data
                this.readAndResolveIncludedData(curObj);                 
            }
            this.data = list;
        } else {
            this.data = new JSONApiData();
            ((JSONApiData) this.data).convertToJSONApiData(obj);
            
            // read and attach included relationships data
            this.readAndResolveIncludedData(obj);            
        }
        
        this.setDocumentLinks(obj); 
    }
    
    /**
     * Creates a new JSONApiDocument of given object and attaches given JSONApiLink to document.
     * @param obj
     * @param links 
     */
    public JSONApiDocument(Object obj, JSONApiLink links) {

        if (obj == null) {
            this.data = new JSONApiData();
            return;
        }

        if ((obj instanceof ArrayList)) {
            ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
            int arraySize = ((ArrayList) obj).size();
            for (int i = 0; i < arraySize; i++) {
                Object curObj = ((ArrayList) obj).get(i);
                list.add(new JSONApiData().convertToJSONApiData(curObj));
                
                // read and attach included relationships data
                this.readAndResolveIncludedData(curObj);                
            }
            this.data = list;
        } else {
            this.data = new JSONApiData();
            ((JSONApiData) this.data).convertToJSONApiData(obj);
            
            // read and attach included relationships data
            this.readAndResolveIncludedData(obj);            
        }
        
        //set links
        this.setLinks(links);
    }    

    //This will be used only for override single object type
    public JSONApiDocument(Object obj, String type) {
        if (obj == null) {
            this.data = new JSONApiData(type);
            return;
        }

        if ((obj instanceof ArrayList)) {
            ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
            int arraySize = ((ArrayList) obj).size();
            for (int i = 0; i < arraySize; i++) {
                Object curObj = ((ArrayList) obj).get(i);
                list.add(new JSONApiData(type).convertToJSONApiData(curObj));
            }
            this.data = list;
        } else {
            this.data = new JSONApiData(type);
            ((JSONApiData) this.data).convertToJSONApiData(obj);
        }
    }

    public Object getData() {

        if ((this.data instanceof java.util.LinkedHashMap)) {
            ObjectMapper mapper = new ObjectMapper();
            this.data = mapper.convertValue(this.data, JSONApiData.class);
        }

        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ArrayList<JSONApiErrors> getErrors() {
        return errors;
    }

    public void setErrors(ArrayList<JSONApiErrors> errors) {
        this.errors = errors;
    }

    public Object getMeta() {
        return meta;
    }

    public void setMeta(Object meta) {
        this.meta = meta;
    }

    public JSONApi getJsonapi() {
        return jsonapi;
    }

    public void setJsonapi(JSONApi jsonapi) {
        this.jsonapi = jsonapi;
    }

    public JSONApiLink getLinks() {
        return links;
    }

    public void setLinks(JSONApiLink links) {
        this.links = links;
    }

    public Object getIncluded() {
        ObjectMapper mapper = new ObjectMapper();
        if ((this.included instanceof java.util.LinkedHashMap)) {
            this.included = mapper.convertValue(this.included, JSONApiData.class);
        }

        if ((this.included instanceof java.util.ArrayList)) {
            ArrayList<JSONApiData> data = new ArrayList<JSONApiData>();
            for (Object obj : (ArrayList<Object>) this.included) {
                data.add(mapper.convertValue(obj, JSONApiData.class));
            }
            this.included = data;
        }

        return this.included;
    }

    public void setIncluded(Object included) {
        this.included = included;
    }

    public void includeData(Object... included) {
        ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
        for (Object include : included) {
            list.add(new JSONApiData().convertToJSONApiData(include));
        }
        this.included = list;
    }

    public void includeDataAsArrayList(ArrayList<?> included) {
        ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
        for (Object include : included) {
            list.add(new JSONApiData().convertToJSONApiData(include));
        }
        this.included = list;
    }

    public void includeDataAsSet(Set<?> included) {
        ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
        for (Object include : included) {
            list.add(new JSONApiData().convertToJSONApiData(include));
        }
        this.included = list;
    }

    /**
     * Appends items in the given data collection to this instance included
     * property.
     *
     * @param includedData - The collection to include in the included property
     */
    public void appendIncludedDataCollection(Collection<?> includedData) {
        Object object = this.getIncluded();
        ArrayList<JSONApiData> list = new ArrayList<>();
        if ((null != object && object instanceof java.util.LinkedHashMap)) {
            Set set = (HashSet<?>) object;
            set.forEach((include) -> {
                list.add(new JSONApiData().convertToJSONApiData(include));
            });
            includedData.forEach((include) -> {
                set.add(new JSONApiData().convertToJSONApiData(include));
            });
            this.included = list;
        } else if (null != object && object instanceof java.util.ArrayList) {
            List tempList = (List<?>) object;
            tempList.forEach((include) -> {
                list.add(new JSONApiData().convertToJSONApiData(include));
            });
            List includedCopy = (List<?>) this.included;
            List includeddataStream = new ArrayList((List<?>) includedData);
            includeddataStream.stream().forEach((include) -> {
                //tempList.add(new JSONApiData().convertToJSONApiData(include));                    
                includedCopy.add(new JSONApiData().convertToJSONApiData(include)); // new feature                    
            });
            //this.included = tempList;     
            this.included = includedCopy;
        } else if (object == null) {
            includedData.forEach((include) -> {
                list.add(new JSONApiData().convertToJSONApiData(include));
            });
            this.included = list;
        }
    }

    public void appendIncludedData(Object... included) {
        Object object = this.getIncluded();
        ArrayList<JSONApiData> list = new ArrayList<>();
        if ((null != object && object instanceof java.util.LinkedHashMap)) {
            Set set = (HashSet<?>) object;
            for (Object include : included) {
                set.add(new JSONApiData().convertToJSONApiData(include));
            }
            this.included = set;
        } else if (null != object && object instanceof java.util.ArrayList) {
            List tempList = (List<?>) object;
            for (Object include : included) {
                tempList.add(new JSONApiData().convertToJSONApiData(include));
            }
            this.included = tempList;
        } else if (object == null) {
            for (Object include : included) {
                list.add(new JSONApiData().convertToJSONApiData(include));
                this.included = list;
            }
        }
    }

    // relationships added
    public Object getRelationships() {
        ObjectMapper mapper = new ObjectMapper();
        if ((this.relationships instanceof java.util.LinkedHashMap)) {
            this.relationships = mapper.convertValue(this.relationships, JSONApiData.class);
        }

        if ((this.relationships instanceof java.util.ArrayList)) {
            ArrayList<JSONApiData> data = new ArrayList<JSONApiData>();
            for (Object obj : (ArrayList<Object>) this.relationships) {
                data.add(mapper.convertValue(obj, JSONApiData.class));
            }
            this.relationships = data;
        }

        return this.relationships;
    }

    public void setRelationships(Object relationships) {
        this.relationships = relationships;
    }

    public void attachRelationshipsData(Object... included) {
        ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
        for (Object include : included) {
            list.add(new JSONApiData().convertToJSONApiData(include));
        }
        this.relationships = list;
    }

    public void attachRelationshipsDataAsArrayList(ArrayList<?> included) {
        ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
        for (Object include : included) {
            list.add(new JSONApiData().convertToJSONApiData(include));
        }
        this.relationships = list;
    }

    public void attachRelationshipsDataAsSet(Set<?> included) {
        ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
        for (Object include : included) {
            list.add(new JSONApiData().convertToJSONApiData(include));
        }
        this.relationships = list;
    }

    /**
     * Appends items in the given data collection to this instance relationships
     * property.
     *
     * @param included - The collection to append in the relationships property
     */
    public void appendRelationshipsDataCollection(Collection<?> included) {
        Object object = this.getIncluded();
        ArrayList<JSONApiData> list = new ArrayList<>();
        if ((null != object && object instanceof java.util.LinkedHashMap)) {
            Set set = (HashSet<?>) object;
            set.forEach((include) -> {
                list.add(new JSONApiData().convertToJSONApiData(include));
            });
            included.forEach((include) -> {
                set.add(new JSONApiData().convertToJSONApiData(include));
            });
            this.relationships = list;
        } else if (null != object && object instanceof java.util.ArrayList) {
            List tempList = (List<?>) object;
            tempList.forEach((include) -> {
                list.add(new JSONApiData().convertToJSONApiData(include));
            });
            included.forEach((include) -> {
                tempList.add(new JSONApiData().convertToJSONApiData(include));
            });
            this.relationships = tempList;
        } else if (object == null) {
            included.forEach((include) -> {
                list.add(new JSONApiData().convertToJSONApiData(include));
            });
            this.relationships = list;
        }
    }

    /**
     * Appends given object to this instance relationships property.
     *
     * @param included
     */
    public void appendRelationshipsData(Object... included) {
        Object object = this.getIncluded();
        ArrayList<JSONApiData> list = new ArrayList<>();
        if ((null != object && object instanceof java.util.LinkedHashMap)) {
            Set set = (HashSet<?>) object;
            for (Object include : included) {
                set.add(new JSONApiData().convertToJSONApiData(include));
            }
            this.relationships = set;
        } else if (null != object && object instanceof java.util.ArrayList) {
            List tempList = (List<?>) object;
            for (Object include : included) {
                tempList.add(new JSONApiData().convertToJSONApiData(include));
            }
            this.relationships = tempList;
        } else if (object == null) {
            for (Object include : included) {
                list.add(new JSONApiData().convertToJSONApiData(include));
                this.relationships = list;
            }
        }
    }

    private void readAndResolveIncludedData(Object object)
    {
        Field[] fields = mergeClassFields(object.getClass().getDeclaredFields(), object.getClass().getSuperclass().getDeclaredFields());
        Object relationship = null;
        for (Field field : fields) {
            if (field.isAnnotationPresent(JAPIRelationship.class)) {
                field.setAccessible(true);
                Object value;
                try {
                    value = field.get(object);

                    // overwrite check
                    if (relationship == null) // first write
                    {
                        relationship = value;
                    } else // already assigned, append new data if property is a collection 
                    {
                        if (relationship instanceof Collection) {
                            // indirect list, iterate
                            Collection relationshipCollection = (Collection) relationship;
                            List copy = new ArrayList(relationshipCollection);

                            if (value instanceof Collection) {
                                Collection valueCollection = (Collection) value;
                                List list = new ArrayList(valueCollection); // copy indirect list to new list
                                list.forEach((valueItem) -> {
                                    copy.add(valueItem);
                                });
                                relationship = copy;
                            }
                        }
                    }
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    throw new InvalidArgumentException(e.getMessage());
                }
            }
        }
     
        // set relationships value
        if (relationship != null) {
            if (relationship instanceof Collection) {
                // indirect list, iterate
                Collection collection = (Collection) relationship;
                List list = new ArrayList(collection);
                list.forEach((item) -> {
                    this.appendIncludedData(item);
                });
            } else {
                this.appendIncludedData(relationship);
            }
        }
    }

    /**
     * Finds JSONApiLink value in given object and sets this document's JSONApiLink field.
     * @param object 
     */
    private void setDocumentLinks(Object object)    
    {        
        JSONApiLink jsonApiLink = new JSONApiLink();
        boolean isLinksAnnotationFound = false;
        Field[] fields = mergeClassFields(object.getClass().getDeclaredFields(), object.getClass().getSuperclass().getDeclaredFields());
        for (Field field : fields)
        {
            if (field.isAnnotationPresent(JAPILink.class))
            {
                field.setAccessible(true);
                Object value;
                try
                {
                    value = field.get(object);
                    jsonApiLink = (JSONApiLink) value;
                    isLinksAnnotationFound = true;                           
                }
                catch (IllegalArgumentException | IllegalAccessException e)
                {
                    throw new InvalidArgumentException(e.getMessage());
                }                
            }
        }
        
        // sets document's link and clear links field of source data object (to not repeat in json serialization)
        if (isLinksAnnotationFound) {
            this.setLinks(jsonApiLink);
            //clearDataLinks();
        }     
    }
    
    /**
     * Clears the value of JSONApiLink field in field "data" of this document.
     * since it will be written on this document's JSONApiLink field.
     */
    private void clearDataLinks()    
    {        
        Object data = this.getData();        
        Field[] fields = mergeClassFields(this.getData().getClass().getDeclaredFields(), this.getData().getClass().getSuperclass().getDeclaredFields());
        for (Field field : fields)
        {            
            field.setAccessible(true);
            Object value;
            try {
                value = field.get(data);
                if (value instanceof JSONApiLink) {
                    field.set(this.getData(), null);
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                throw new InvalidArgumentException(e.getMessage());
            }                         
        }             
    }    

    
    /**
     *
     * @param a - Is the current Class
     * @param b - Is the Superclass;
     * @return
     */
    private Field[] mergeClassFields(Field[] a, Field[] b) {

        int aLen = a.length;
        int bLen = b.length;

        if (bLen == 0) {
            return a;
        }

        Field[] c = new Field[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;

    }

}
