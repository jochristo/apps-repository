package org.atlas.applications.core.jsonapi.extensions;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.atlas.applications.core.jsonapi.JSONApiLink;

/**
 * Resource-level JSON API links object: Does not include null pagination links.
 * @author ic
 */
public class JSONApiLinkNoPaging extends JSONApiLink
{
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Override
    public String getLast() {
        return super.getLast(); //To change body of generated methods, choose Tools | Templates.
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Override
    public String getNext() {
        return super.getNext(); //To change body of generated methods, choose Tools | Templates.
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Override
    public String getPrev() {
        return super.getPrev(); //To change body of generated methods, choose Tools | Templates.
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Override
    public String getFirst() {
        return super.getFirst(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
