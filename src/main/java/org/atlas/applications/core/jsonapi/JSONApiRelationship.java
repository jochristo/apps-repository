package org.atlas.applications.core.jsonapi;

/**
 * JSON API Implementation - Relationships
 * @author Konstantinos Antonopoulos <a href="mailto:konsantonop@gmail.com">Konstantinos Antonopoulos</a>
 * @version 0.1 
 * @category System core ( JSON API Schema Implementation )
 * @since 2016-08-05
 * @see http://jsonapi.org/format/#document-resource-object-relationships
 **/

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class JSONApiRelationship {
	
	@JsonIgnore
	private String relationtype;
	
        @JsonInclude(Include.NON_NULL)
	private JSONApiLink links;

	private Object data;
	
        @JsonInclude(Include.NON_NULL)
	private Object meta;
	
	public JSONApiRelationship(){}
	
	public JSONApiLink getLinks() {
		return links;
	}
	public void setLinks(JSONApiLink links) {
		this.links = links;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Object getMeta() {
		return meta;
	}
	public void setMeta(Object meta) {
		this.meta = meta;
	}

	public String getRelationtype() {
		return relationtype;
	}
	public void setRelationtype(String relationtype) {
		this.relationtype = relationtype;
	}
}
