package org.atlas.applications.core.jsonapi.extensions;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Designates a root object for relationships fields.
 * @author ic
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface JAPIRelationshipRoot
{
    public String selfLink();
    
    public Class<?> relationship();	
}
