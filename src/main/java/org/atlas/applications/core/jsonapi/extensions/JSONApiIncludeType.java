package org.atlas.applications.core.jsonapi.extensions;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.atlas.applications.modules.repo.domain.ApplicationContent;
import org.atlas.applications.modules.repo.domain.ApplicationTransaction;
import org.atlas.applications.modules.repo.dto.ApplicationContentDto;
import org.atlas.applications.modules.repo.dto.ApplicationTransactionDto;

/**
 * Defines type of included relationship data name
 * @author ic
 */
public enum JSONApiIncludeType
{
    VERSIONS("versions", ApplicationContent.class, ApplicationContentDto.class), 
    
    TRANSACTIONS("transactions", ApplicationTransaction.class,ApplicationTransactionDto.class),
    
    VERSIONS_TRANSACTIONS("versions_transactions", ApplicationContent.class, ApplicationContentDto.class, ApplicationTransaction.class,ApplicationTransactionDto.class);
    
    private final String type;
    private final Class<?>[] clazz;
    private JSONApiIncludeType(String type, Class<?>... clazz)
    {
        this.type = type;
        this.clazz = clazz;
    }  

    public String getType() {
        return type;
    }

    public Class<?>[] getClazz() {
        return clazz;
    }
    
    public Set<Class<?>> set()
    {           
        return new HashSet(Arrays.asList(this.getClazz()));        
    }
    
}
