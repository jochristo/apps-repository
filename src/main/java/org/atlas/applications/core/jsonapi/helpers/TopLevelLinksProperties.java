/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.core.jsonapi.helpers;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import org.atlas.applications.core.jsonapi.JSONApiLink;
import org.atlas.applications.core.jsonapi.extensions.JSONApiLinkNoPaging;
import org.atlas.applications.core.utilities.Utilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Holds the top-level links properties for a JSON API document.
 * @author ic
 */
public class TopLevelLinksProperties
{
    private final int count;
    private final int page;
    private final int size;
    private final String self;   
    private long lastPage;
    private String first;
    private String prev;
    private String next;
    private String last;
    private JSONApiLink links;
    
    private final String pageParamsString = "&page%5Bnumber%5D=1&page%5Bsize%5D=";
    private final String pageNumberParamString = "&page%5Bnumber%5D=";
    private final String pageSizeParamString = "&page%5Bsize%5D=";
    private final String UTF8_ENCODING = "UTF-8";
    private final String LEFT_BRACKET_ENCODED = "%5B";
    private final String RIGHT_BRACKET_ENCODED = "%5D";
    
    private static final Logger logger = LoggerFactory.getLogger(TopLevelLinksProperties.class);

    public TopLevelLinksProperties(int count, int page, int size, String self) {
        this.count = count;
        this.page = page;
        this.size = size;
        this.self = self;
        this.links = new JSONApiLink();
    }    
    
    /**
     * Gets the {@link JSONApiLink} object from pagination properties.
     * @return 
     */
    public JSONApiLink createLinks()
    {        
        try {
            prepareLinks();
        } catch (UnsupportedEncodingException ex) {
            logger.error(this.getClass().getName(), ex);
        }
        return this.links;
    }
    
    /**
     * Creates the links fields.
     * @throws java.io.UnsupportedEncodingException
     */
    protected void prepareLinks() throws UnsupportedEncodingException
    {

        String queryStringFirstParamOnly = isolateUrl(self);        
        links.setSelf(self); 
        if(count > 0 && page > 0 & size >0)
        {
            lastPage = Utilities.roundUp(count, size);        
            first = queryStringFirstParamOnly + pageParamsString + this.size;
            links.setFirst(String.valueOf(first));
            if (page > 1) {
                long previous = page - 1;
                prev = queryStringFirstParamOnly + pageNumberParamString + previous +pageSizeParamString + this.size;
                links.setPrev(String.valueOf(prev));
            }

            if (this.page < lastPage) {
                long nextPage = this.page + 1;
                next = queryStringFirstParamOnly + pageNumberParamString + nextPage + pageSizeParamString + this.size;
                links.setNext(String.valueOf(next));
            }

            if (lastPage > 0 && lastPage != page) {
                last = queryStringFirstParamOnly + pageNumberParamString + lastPage + pageSizeParamString + this.size;
                links.setLast(String.valueOf(last));
            }    
        }
        else{
            this.links = new JSONApiLinkNoPaging();
            this.links.setSelf(this.self);
        }
    }
    
    /**
     * Extracts up to the first query part.
     * @param selfUrl
     * @return 
     * @throws java.io.UnsupportedEncodingException 
     */
    protected String isolateUrl(String selfUrl) throws UnsupportedEncodingException
    {
        String decoded = URLDecoder.decode(self, UTF8_ENCODING);
        String[] string = decoded.split("&page");
        return string[0];        
    }
    
    
}
