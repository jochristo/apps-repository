package org.atlas.applications.core.jsonapi;

import org.atlas.applications.core.jsonapi.helpers.ErrorSource;

public class JSONApiErrors {

	private String id;
	private Object links;
	private String status;
	private String code;
	private String title;
	private String detail;
	private ErrorSource source;
	private Object meta;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Object getLinks() {
		return links;
	}
	public void setLinks(Object links) {
		this.links = links;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public ErrorSource getSource() {
		return source;
	}
	public void setSource(ErrorSource source) {
		this.source = source;
	}
	public Object getMeta() {
		return meta;
	}
	public void setMeta(Object meta) {
		this.meta = meta;
	}
	
}
