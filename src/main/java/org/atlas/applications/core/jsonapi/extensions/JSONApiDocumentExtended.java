/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.core.jsonapi.extensions;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.atlas.applications.core.exceptions.InvalidArgumentException;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.JSONApiLink;
import org.atlas.applications.core.jsonapi.annotations.JAPIRelationship;

/**
 * Extends JSONApiDocument class to include top-level links, relationships and included data.
 * @author Admin
 * @param <T> The type of source class
 */
public class JSONApiDocumentExtended<T extends Object> extends JSONApiDocument {

    public JSONApiDocumentExtended() {
    }

    public JSONApiDocumentExtended(T obj) {
        super(obj);
    }
    
    /**
     * Returns a single resource document.
     * @param object single resource
     * @param toplevelLinkUrl
     * @return 
     */
    public JSONApiDocumentExtended toSingle (T object, String toplevelLinkUrl) throws IllegalArgumentException, IllegalAccessException
    {
        // return empty doc
        if (object == null){                       
            return new JSONApiDocumentExtended(object);
        }
                
        Field[] allFields = mergeClassFields(object.getClass().getDeclaredFields(), object.getClass().getSuperclass().getDeclaredFields());
        Object relationship = null;
        for (Field field : allFields)
        {
            if (field.isAnnotationPresent(JAPIRelationship.class)) {
                field.setAccessible(true);
                Object value;
                try
                {
                    value = field.get(object);   
                    
                    // overwrite check
                    if(relationship == null) // first write
                    {
                        relationship = value;
                    }
                    else // already assigned, append new data if property is a collection 
                    {
                        if(relationship instanceof Collection)
                        {
                            // indirect list, iterate
                            Collection relationshipCollection = (Collection) relationship;
                            List copy = new ArrayList(relationshipCollection);
                            
                            if(value instanceof Collection)
                            {
                               Collection valueCollection = (Collection) value;
                               List list = new ArrayList(valueCollection); // copy indirect list to new list
                               list.forEach((valueItem)->{copy.add(valueItem);});
                               relationship = copy;
                            }
                        }                          
                    }                                    
                    
                    //relationship = value;
                } catch (IllegalArgumentException | IllegalAccessException e) {                    
                    throw new InvalidArgumentException(e.getMessage());
                }
            }
        }
        
        JSONApiDocumentExtended document = new JSONApiDocumentExtended(object);         
        JSONApiLinkNoPaging topLevelLinks = new JSONApiLinkNoPaging();
        topLevelLinks.setSelf(toplevelLinkUrl);
        document.setLinks(topLevelLinks);        
        
        // set relationships value
        if(relationship != null)
        {
            if(relationship instanceof Collection)
            {
                // indirect list, iterate
                Collection collection = (Collection) relationship;
                List list = new ArrayList(collection);
                list.forEach((item)->{document.appendIncludedData(item);});                
            }  
            else
            {
                document.appendIncludedData(relationship);
            }    
        }                                
        return document;             
    }
    
    /**
     * Returns a single resource document.
     * @param object single resource
     * @param toplevelLinkUrl
     * @return 
     */
    public JSONApiDocumentExtended toSingle (T object) throws IllegalArgumentException, IllegalAccessException
    {
        // return empty doc
        if (object == null){                       
            return new JSONApiDocumentExtended(object);
        }
                
        Field[] allFields = mergeClassFields(object.getClass().getDeclaredFields(), object.getClass().getSuperclass().getDeclaredFields());
        Object relationship = null;
        for (Field field : allFields)
        {
            if (field.isAnnotationPresent(JAPIRelationship.class)) {
                field.setAccessible(true);
                Object value;
                try
                {
                    value = field.get(object);   
                    
                    // overwrite check
                    if(relationship == null) // first write
                    {
                        relationship = value;
                    }
                    else // already assigned, append new data if property is a collection 
                    {
                        if(relationship instanceof Collection)
                        {
                            // indirect list, iterate
                            Collection relationshipCollection = (Collection) relationship;
                            List copy = new ArrayList(relationshipCollection);
                            
                            if(value instanceof Collection)
                            {
                               Collection valueCollection = (Collection) value;
                               List list = new ArrayList(valueCollection); // copy indirect list to new list
                               list.forEach((valueItem)->{copy.add(valueItem);});
                               relationship = copy;
                            }
                        }                          
                    }                                    
                    
                    //relationship = value;
                } catch (IllegalArgumentException | IllegalAccessException e) {                    
                    throw new InvalidArgumentException(e.getMessage());
                }
            }
        }
        
        JSONApiDocumentExtended document = new JSONApiDocumentExtended(object);               
        
        // set relationships value
        if(relationship != null)
        {
            if(relationship instanceof Collection)
            {
                // indirect list, iterate
                Collection collection = (Collection) relationship;
                List list = new ArrayList(collection);
                list.forEach((item)->{document.appendIncludedData(item);});                
            }  
            else
            {
                document.appendIncludedData(relationship);
            }    
        }                                
        return document;             
    }    
    
    /**
     * Returns an collection of resources document.
     * @param object collection of resources
     * @param toplevelLinkUrl
     * @return 
     */
    public JSONApiDocumentExtended toArray(List<T> object, String toplevelLinkUrl)
    {
        // return empty doc
        if (object == null){                       
            return new JSONApiDocumentExtended(object);             
        }
                
        JSONApiDocumentExtended document = new JSONApiDocumentExtended(object);
        JSONApiLink topLevelLinks = new JSONApiLink();
        topLevelLinks.setSelf(toplevelLinkUrl);
        document.setLinks(topLevelLinks); 
        
        Collection collection = new ArrayList();
        object.forEach((item)->
        {
            
            Field[] allFields = mergeClassFields(item.getClass().getDeclaredFields(), item.getClass().getSuperclass().getDeclaredFields());
            Object relationship = null;
            for (Field annotation : allFields)
            {
                if (annotation.isAnnotationPresent(JAPIRelationship.class)) {
                    annotation.setAccessible(true);
                    Object value;
                    try {
                        value = annotation.get(item);                    
                        relationship = value;
                    } catch (IllegalArgumentException | IllegalAccessException e) {                    
                        throw new InvalidArgumentException(e.getMessage());
                    }
                }
            }

            // set relationships value
            if(relationship != null)
            {
                if(relationship instanceof Collection)
                {
                    // indirect list, iterate
                    Collection relationshipCollection = (Collection) relationship;
                    List list = new ArrayList(relationshipCollection);
                    list.forEach((relationshipItem)->{ collection.add(relationshipItem); });                
                }  
                else
                {
                    collection.add(relationship);
                }             
            }              
            
        });                        

        if(collection.isEmpty() == false){
            document.appendIncludedDataCollection(collection);
        }
        return document;      
        
    }          
        
    
    /**
     * Returns an collection of resources document.
     * @param object collection of resources
     * @param topLevelLinks
     * @return 
     */
    public JSONApiDocumentExtended toArray(List<T> object, JSONApiLink topLevelLinks)
    {
        // return empty doc
        if (object == null){                       
            return new JSONApiDocumentExtended(object);             
        }
                
        JSONApiDocumentExtended document = new JSONApiDocumentExtended(object);        
        document.setLinks(topLevelLinks); 
        
        Collection collection = new ArrayList();
        object.forEach((item)->
        {
            
            Field[] allFields = mergeClassFields(item.getClass().getDeclaredFields(), item.getClass().getSuperclass().getDeclaredFields());
            Object relationship = null;
            for (Field annotation : allFields)
            {
                if (annotation.isAnnotationPresent(JAPIRelationship.class)) {
                    annotation.setAccessible(true);
                    Object value;
                    try {
                        value = annotation.get(item);                    
                        relationship = value;
                    } catch (IllegalArgumentException | IllegalAccessException e) {                    
                        throw new InvalidArgumentException(e.getMessage());
                    }
                }
            }

            // set relationships value
            if(relationship != null)
            {
                if(relationship instanceof Collection)
                {
                    // indirect list, iterate
                    Collection relationshipCollection = (Collection) relationship;
                    List list = new ArrayList(relationshipCollection);
                    list.forEach((relationshipItem)->{ collection.add(relationshipItem); });                
                }  
                else
                {
                    collection.add(relationship);
                }             
            }              
            
        });                        

        if(collection.isEmpty() == false){
            document.appendIncludedDataCollection(collection);
        }
        return document;      
        
    }      
    
    /**
     *
     * @param a - Is the current Class
     * @param b - Is the Superclass;
     * @return
     */
    private static Field[] mergeClassFields(Field[] a, Field[] b) {

        int aLen = a.length;
        int bLen = b.length;

        if (bLen == 0) {
            return a;
        }

        Field[] fields = new Field[aLen + bLen];
        System.arraycopy(a, 0, fields, 0, aLen);
        System.arraycopy(b, 0, fields, aLen, bLen);
        return fields;

    }      
}
