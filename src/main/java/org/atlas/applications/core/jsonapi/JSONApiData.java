package org.atlas.applications.core.jsonapi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.atlas.applications.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.applications.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.applications.core.jsonapi.annotations.JAPILink;
import org.atlas.applications.core.jsonapi.annotations.JAPIRelationship;
import org.atlas.applications.core.jsonapi.annotations.JAPIType;
import org.atlas.applications.core.jsonapi.extensions.JSONApiRelationshipRoot;
import org.atlas.applications.core.jsonapi.helpers.RelatedLink;
import org.atlas.applications.core.jsonapi.serializers.RelationshipSerializer;
import org.atlas.applications.core.utilities.Utilities;

public class JSONApiData {

    private final ObjectMapper mapper = new ObjectMapper();

    @JsonInclude(Include.ALWAYS)
    private String id; // = java.util.UUID.randomUUID().toString();

    @JsonInclude(Include.ALWAYS)
    private String type;

    @JsonInclude(Include.NON_EMPTY)
    private Object attributes;

    //@JsonSerialize(using = RelationshipSerializer.class)
    //@JsonInclude(Include.NON_NULL)    
    @JsonIgnore
    private JSONApiRelationship relationshipz;

    @JsonSerialize(using = RelationshipSerializer.class)
    @JsonInclude(Include.NON_NULL)
    private JSONApiRelationshipRoot relationships;

    @JsonInclude(Include.NON_EMPTY)
    private JSONApiLink links;

    @JsonIgnore
    private boolean isResponse = false;

    @JsonIgnore
    private String classpath = "";

    @JsonIgnore
    private String overridedDataType;

    public JSONApiData() {
        this.overridedDataType = null;
    }

    public JSONApiData(String overridedDataType) {
        this.overridedDataType = overridedDataType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getAttributes() {

        if (this.isResponse) {
            if (this.attributes != null) {
                try {
                    return this.mapper.convertValue(this.attributes, Class.forName(this.classpath));
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return this.attributes;
    }

    public void setAttributes(Object attributes) {
        this.attributes = attributes;
    }

    public JSONApiRelationship getRelationshipz() {
        return relationshipz;
    }

    public void setRelationshipz(JSONApiRelationship relationshipz) {
        this.relationshipz = relationshipz;
    }

    public JSONApiRelationshipRoot getRelationships() {
        return relationships;
    }

    public void setRelationships(JSONApiRelationshipRoot relationships) {
        this.relationships = relationships;
    }

    

    /**
     *
     * @param a - Is the current Class
     * @param b - Is the Superclass;
     * @return
     */
    private Field[] mergeClassFields(Field[] a, Field[] b) {

        int aLen = a.length;
        int bLen = b.length;

        if (bLen == 0) {
            return a;
        }

        Field[] c = new Field[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;

    }

    public JSONApiData convertToJSONApiData(Object obj) {

        LinkedHashMap lhm = new LinkedHashMap();//Attributes
        JSONApiRelationship relationship = null;

        // init relationshipsRoot object
        JSONApiRelationshipRoot apiRelationshipRoot = null;

        getTypeByClass(obj);

        Field[] allFields = this.mergeClassFields(obj.getClass().getDeclaredFields(), obj.getClass().getSuperclass().getDeclaredFields());

        for (Field annotation : allFields) {//obj.getClass().getDeclaredFields()

            if (annotation.isAnnotationPresent(JAPIIdentifier.class)) {
                annotation.setAccessible(true);
                Object value;
                try {
                    value = annotation.get(obj);
                    this.id = value.toString();
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (annotation.isAnnotationPresent(JAPIAttribute.class)) {
                annotation.setAccessible(true);
                Object value;
                try {
                    // try read field new name using annotation if present
                    JAPIAttribute attribute = annotation.getAnnotation(JAPIAttribute.class);
                    String name = annotation.getName();
                    if (attribute != null) {
                        if (!attribute.name().isEmpty()) {
                            name = attribute.name();
                        }
                    }
                    value = annotation.get(obj);
                    //lhm.put(annotation.getName(), value);
                    lhm.put(name, value);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            if (annotation.isAnnotationPresent(JAPIRelationship.class)) {

                JAPIRelationship japran = (JAPIRelationship) annotation.getAnnotation(JAPIRelationship.class);
                relationship = new JSONApiRelationship();
                JSONApiLink link = new JSONApiLink();
                annotation.setAccessible(true);
                Object value;
                try {
                    value = annotation.get(obj);
                    if (value != null) {

                        link.setSelf(japran.selfLink().replaceFirst("\\{.+\\}", String.valueOf(value)));
                        relationship.setRelationtype(japran.relationship().getSimpleName());
                        relationship.setLinks(link);

                        // new stuff
                        if (japran.selfLink().isEmpty()) { // no self link set on JAPIRelationship field 
                            relationship.setLinks(null);
                        }

                        Object item = null;
                        Collection<Object> collectionData;

                        if (value instanceof Set) // set object
                        {
                            Set set = (Set) value;
                            List list = new ArrayList();
                            list.addAll(set);
                            if (list.isEmpty() == false) {
                                item = list.get(0);

                                //iterate to set id and type
                                collectionData = new ArrayList();
                                for (int i = 0; i < list.size(); i++) {
                                    JSONApiData apiData = new JSONApiData();
                                    apiData.convertToJSONApiData(list.get(i));
                                    JSONApiData apiDataNew = new JSONApiData();
                                    apiDataNew.setId(apiData.getId());
                                    apiDataNew.setType(apiData.getType());
                                    collectionData.add(apiDataNew);
                                }
                                // set relationships data
                                if (collectionData.size() > 1) {
                                    relationship.setData(collectionData);
                                } else if (collectionData.size() == 1) {
                                    relationship.setData(collectionData.toArray()[0]);
                                }
                            }
                        }
                        if (value instanceof List) // list object
                        {
                            List list = (List) value;
                            if (list.isEmpty() == false) {
                                item = list.get(0);

                                //iterate to set id and type
                                collectionData = new ArrayList();
                                for (int i = 0; i < list.size(); i++) {
                                    JSONApiData apiData = new JSONApiData();
                                    apiData.convertToJSONApiData(list.get(i));
                                    JSONApiData apiDataNew = new JSONApiData();
                                    apiDataNew.setId(apiData.getId());
                                    apiDataNew.setType(apiData.getType());
                                    collectionData.add(apiDataNew);
                                }
                                // set relationships data                                                
                                if (collectionData.size() > 1) {
                                    relationship.setData(collectionData);
                                } else if (collectionData.size() == 1) {
                                    relationship.setData(collectionData.toArray()[0]);
                                }
                            }
                        } else {                       // single object
                            item = value;
                            JSONApiData apiData = new JSONApiData();
                            apiData.convertToJSONApiData(item);
                            JSONApiData apiDataNew = new JSONApiData();
                            apiDataNew.setId(apiData.getId());
                            apiDataNew.setType(apiData.getType());
                            relationship.setData(apiDataNew);
                        }
                        // try read JAPIAttribute name of present on relationship class
                        if (Utilities.isEmpty(item) == false) {
                            if (item.getClass().isAnnotationPresent(JAPIType.class)) {
                                JAPIType apiTypeAnnotation = japran.relationship().getAnnotation(JAPIType.class);
                                relationship.setRelationtype(apiTypeAnnotation.type());
                            }
                        }

                        // null relationship if empty list/set                           
                        if (value instanceof List) {
                            List list = (List) value;
                            if (Utilities.isEmpty(list) == true) {
                                relationship = null;
                            }
                        } else if (value instanceof Set) {
                            Set set = (Set) value;
                            if (Utilities.isEmpty(set) == true) {
                                relationship = null;
                            }
                        }

                        // add relationship item to root relationship object: NEW feature
                        if (Utilities.isEmpty(relationship) == false) {
                            if (apiRelationshipRoot == null) {
                                this.relationships = new JSONApiRelationshipRoot();
                                apiRelationshipRoot = new JSONApiRelationshipRoot();
                                apiRelationshipRoot.addRelationshipItem(relationship);
                            } else {
                                if (!apiRelationshipRoot.getItems().contains(relationship)) {
                                    apiRelationshipRoot.addRelationshipItem(relationship);
                                }
                            }
                        }
                    }
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            // set resource-level links: different from top-level links object
            if (annotation.isAnnotationPresent(JAPILink.class)) {
                annotation.setAccessible(true);
                Object value;
                try {
                    value = annotation.get(obj);
                    if (value instanceof JSONApiLink) {
                        JSONApiLink jSONApiLink = (JSONApiLink) value;
                        this.setLinks(jSONApiLink);
                    }

                } catch (IllegalArgumentException | IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        this.isResponse = true;
        this.attributes = lhm;

        // set relationships object
        if (relationship != null) {
            this.relationshipz = relationship;
        }

        // set ROOT relatioships object: NEW feature        
        if (apiRelationshipRoot != null) {
            this.relationships = apiRelationshipRoot;
        }

        configureDocumentLinks(obj);

        return this;
    }

    private void configureDocumentLinks(Object obj) {
        JSONApiLink links = new JSONApiLink();
        boolean linksAnnotationFund = false;
        for (Annotation annotation : obj.getClass().getDeclaredAnnotations()) {
            if ((annotation instanceof JAPILink)) {
                linksAnnotationFund = true;
                JAPILink japlan = (JAPILink) annotation;
                links.setSelf(japlan.self().replaceFirst("\\{.+\\}", this.id));
                links.setRelated(new RelatedLink(japlan.related().replaceFirst("\\{.+\\}", this.id)));
            }
        }
        if (linksAnnotationFund) {
            this.setLinks(links);
        }
    }

    private void getTypeByClass(Object object) {

        boolean typeAnnotationFound = false;
        for (Annotation annotation : object.getClass().getDeclaredAnnotations()) {
            if ((annotation instanceof JAPIType)) {
                typeAnnotationFound = true;
                JAPIType japtype = (JAPIType) annotation;
                this.type = japtype.type();
            }
        }

        String classname = object.getClass().toString();

        this.classpath = "";
        String[] temp = classname.split(" ");
        temp = temp[temp.length - 1].split("\\.");

        int tempLength = temp.length;
        for (int i = 0; i < tempLength; i++) {
            if (i == tempLength - 1) {
                if (!typeAnnotationFound) {
                    this.type = temp[i].toLowerCase();
                }
                this.classpath += temp[i].substring(0, 1).toUpperCase() + temp[i].substring(1);
            } else {
                this.classpath += temp[i].toLowerCase() + ".";
            }
        }

        if (this.overridedDataType != null) {
            this.type = this.overridedDataType;
        }
    }

    public Object convertTo(Class<?> classModel) {
        return this.mapper.convertValue(this.attributes, classModel);
    }

    public JSONApiLink getLinks() {
        return links;
    }

    public void setLinks(JSONApiLink links) {
        this.links = links;
    }

}
