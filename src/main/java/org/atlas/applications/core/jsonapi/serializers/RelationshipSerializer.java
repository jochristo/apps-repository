package org.atlas.applications.core.jsonapi.serializers;

import java.io.IOException;

import org.atlas.applications.core.jsonapi.JSONApiRelationship;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.atlas.applications.core.jsonapi.extensions.JSONApiRelationshipRoot;

public class RelationshipSerializer extends JsonSerializer<Object> {

    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {

        if(value instanceof  JSONApiRelationship){
            JSONApiRelationship relationship = (JSONApiRelationship) value;
            gen.writeStartObject();
            gen.writeObjectField(relationship.getRelationtype().toLowerCase(), value);
            gen.writeEndObject();
        }
        if(value instanceof  JSONApiRelationshipRoot)
        {
            JSONApiRelationshipRoot relationship = (JSONApiRelationshipRoot) value;
            
            if(relationship.getItems().isEmpty() == false){
                gen.writeStartObject();
                relationship.getItems().forEach((item)->
                {
                    item = (JSONApiRelationship)item;
                    try {
                        gen.writeObjectField(item.getRelationtype().toLowerCase(), item);
                    } catch (IOException ex) {
                        Logger.getLogger(RelationshipSerializer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                
                gen.writeEndObject();            
            }
        }

    }

}
