/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.core.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ic
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInformation
{
    private final String id;
    private final String email;
    private final String firstName;
    private final String gender;
    private final String lastName;
    private final String link;
    private String role;

    public UserInformation(@JsonProperty("id") String id, @JsonProperty("email") String email,
            @JsonProperty("first_name") String firstName, @JsonProperty("gender") String gender,
            @JsonProperty("last_name") String lastName, @JsonProperty("link") String link) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.gender = gender;
        this.lastName = lastName;
        this.link = link;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getGender() {
        return gender;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLink() {
        return link;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    
    
    
}
