package org.atlas.applications.core.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * OAuth interceptor
 * @author ic
 */
@Provider
public class OAuthContainerRequestFilter implements ContainerRequestFilter
{
    @Context
    private ResourceInfo resourceInfo;

    private static final Logger logger = LoggerFactory.getLogger(OAuthContainerRequestFilter.class);
    private static final String USER_COOKIE = "USER_COOKIE"; // just store in a cookie

    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "Bearer";
    private static final Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED)
                                                        .entity("You cannot access this resource").build();
    private static final Response ACCESS_FORBIDDEN = Response.status(Response.Status.FORBIDDEN)
                                                        .entity("Access blocked for all users !!").build();    
    
    public OAuthContainerRequestFilter() {
    }             
    

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException
    {        
        
        //Get request headers
        final MultivaluedMap<String, String> headers = requestContext.getHeaders();

        //Fetch authorization header
        List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);

        authorization = new ArrayList();
        
        // TO-DO: implement oauth authorization methods        
        
        // assume not empty for now... 
        authorization.add(AUTHORIZATION_PROPERTY);
        
        //If no authorization information present; block access
        if (authorization == null || authorization.isEmpty()) {
            throw new RestApplicationException(Response.Status.UNAUTHORIZED, ApiError.INVALID_ACCESS_TOKEN);            
        }
    }
}
