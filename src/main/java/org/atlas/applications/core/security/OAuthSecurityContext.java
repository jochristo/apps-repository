package org.atlas.applications.core.security;

import java.security.Principal;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author ic
 */
public class OAuthSecurityContext implements SecurityContext
{
    private final UserInformation userInformation;

    public OAuthSecurityContext(UserInformation userInformation) {
        this.userInformation = userInformation;
    }    
    
    @Override
    public Principal getUserPrincipal() {
        return new Principal() {

            public String getName() {
                return userInformation.getFirstName() + ", " + userInformation.getLastName();
            }
        };
    }

    @Override
    public boolean isUserInRole(String role) {
        return userInformation.getRole().equalsIgnoreCase(role);
    }

    @Override
    public boolean isSecure() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String getAuthenticationScheme() {
        // TODO Auto-generated method stub
        return null;
    }
    
}
