package org.atlas.applications.core.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.UUID;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import org.slf4j.LoggerFactory;

/**
 * Provides PBKDF2 with HMAC SHA1/SHA256 encryption.
 * @author ic
 */
public class Hashing
{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Hashing.class);
    
    public static String getHash(String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        int iterations = 10000;
        char[] chars = publicKey.toCharArray();
        byte[] salt = getSalt();
         
        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }

    
    private static byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }
     
    public static String toHex(byte[] array) throws NoSuchAlgorithmException
    {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format("%0"  +paddingLength + "d", 0) + hex;
        }else{
            return hex;
        }
    }
    
    public static boolean isPublicKeyValid(String originalKey, String storedKey) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        String[] parts = storedKey.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);
         
        PBEKeySpec spec = new PBEKeySpec(originalKey.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] testHash = skf.generateSecret(spec).getEncoded();
         
        int diff = hash.length ^ testHash.length;
        for(int i = 0; i < hash.length && i < testHash.length; i++)
        {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }
    public static byte[] fromHex(String hex) throws NoSuchAlgorithmException
    {
        byte[] bytes = new byte[hex.length() / 2];
        for(int i = 0; i<bytes.length ;i++)
        {
            bytes[i] = (byte)Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }     
    
    /**
     * Generate hashed password-string using SHA256 algorithm
     * @param value
     * @return 
     */
    public static String hash(String value)
    {
        String hashed = null;
        try {
            
            MessageDigest md = MessageDigest.getInstance("SHA-256"); //SHA-256            
            byte[] bytes = md.digest(value.getBytes());                        
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }            
            hashed = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) {
        }
        return hashed;
    }    
    
    
    /**
     * Generate hashed password-string using MD5/SHA-256 algorithm
     * @param value
     * @return 
     */
    public static String hash(byte [] data)
    {
        String hashed = null;
        try {
            // MessageDigest instance for SHA-256
            MessageDigest md = MessageDigest.getInstance("SHA-256"); //SHA-256

            //digest data
            byte[] hashedBytes = md.digest(data);            

            //hashed hex format
            return convertByteArrayToHexString(hashedBytes);
        } 
        catch (NoSuchAlgorithmException e) {
        }
        return hashed; //.toUpperCase();
    }    
    private static String convertByteArrayToHexString(byte[] arrayBytes) {
            //Convert bytes it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< arrayBytes.length ;i++)
            {
                sb.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16).substring(1));
            }            
            return sb.toString();
    }    
    
    public String hashText(final String text) throws UnsupportedEncodingException{
      try {
          MessageDigest md = MessageDigest.getInstance("SHA-256");
          md.update(text.getBytes("UTF-8"), 0, text.length());
          return new BigInteger(1, md.digest()).toString(16);

      } catch (NoSuchAlgorithmException e) {
          logger.warn("Could not find hash algorithm", e);
      } catch (UnsupportedEncodingException e) {
          logger.warn("Could not encode text", e);
      }
      return null;
    }    
    
    
    /**
     * Takes the given fileName string and chops it up into given length chars long directory names 
     * which are appended to the root and then the fileName appended to the dir structure.
     * A fileName of , for instance, MYDIRECTORYPATH  where the path depth is 2 would turn into a string /MY/DI/
     * @param dirDepth
     * @param fileName
     * @return
     */
    
    public static  String createDirectoryPathString(String fileName, int dirDepth){
      if(dirDepth == 0) dirDepth = 2;
      StringBuilder sb = new StringBuilder();
      //sb.append(rootDir);
      sb.append(File.separator);
      for(int x=0; x < dirDepth; x++){
          int i = 2*x;
          sb.append(fileName.substring(i, i+2));
          sb.append(File.separator);
      }
      //sb.append(fileName);
      return sb.toString();
    }                
    
    public static String dirHash(final String dirName)
    {
        int hashcode = dirName.hashCode();
        int mask = 255;
        int firstDir = hashcode & mask;
        int secondDir = (hashcode >> 8) & mask;

        StringBuilder path = new StringBuilder(File.separator);
        path.append(String.format("%03d", firstDir));
        path.append(File.separator);
        path.append(String.format("%03d", secondDir));
        path.append(File.separator);
        path.append(dirName);
        return path.toString();
    }
    
    public static String getFileChecksum(MessageDigest digest, File file) throws IOException
    {
        //Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(file);

        //Create byte array
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        //Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        };
        fis.close();

        byte[] bytes = digest.digest();

        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++)
        {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        //return complete hash
       return sb.toString();
    }    
    
    public static String uuidToBase64(byte[] bytes) throws UnsupportedEncodingException{        
        
        UUID uuid = UUID.randomUUID();
        String base64 = Base64.getEncoder().encodeToString(uuid.toString().getBytes("utf-8"));
        return base64;
    }
    
    public static String uuidToBase64(String value) throws UnsupportedEncodingException{        
        
        UUID uuid = UUID.fromString(value);
        String base64 = Base64.getEncoder().encodeToString(uuid.toString().getBytes("utf-8"));
        return base64;
    }    
    
}
