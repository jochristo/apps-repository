/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.core.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jasminb.jsonapi.ResourceConverter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides generic-related class methods to retrieve runtime info:
 * Methods, fields, etc.
 * @author ic
 */
public abstract class ClassUtils {

    public static <T extends Object> List<String> getDeclaredFieldNames(T t) {
        List<String> list = new ArrayList();
        Field[] fields = t.getClass().getDeclaredFields();
        if (!Utilities.isEmpty(fields)) {
            for (Field f : fields) {
                list.add(f.getName());
            }
        }
        return list;
    }

    public static <T extends Object> Map<String, Object> getFieldNameValuePair(T object, String name) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Map<String, Object> map = null;
        for (Field field : object.getClass().getDeclaredFields()) {
            //Type annotation = field.getAnnotation(com.github.jasminb.jsonapi.annotations.Type.class);
            if (field.getName().equals(name)) {
                map = new HashMap();
                map.put(name, getFieldValue(field, object));
            }
        }
        return map;
    }

    public static <T extends Object> Object getFieldValue(Field field, T t) throws InvocationTargetException {
        // MZ: Find the correct method
        for (Method method : t.getClass().getMethods()) {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (field.getName().length() + 3))) {
                if (method.getName().toLowerCase().endsWith(field.getName().toLowerCase())) {
                    // Method found, run it
                    try {
                        try {
                            return method.invoke(t);
                        } catch (IllegalArgumentException | InvocationTargetException ex) {
                            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (IllegalAccessException e) {
                        Logger.getLogger("Could not determine method: " + method.getName());
                    }
                }
            }
        }
        return null;
    }
    
        /**
     * Converts given instance of given type into JSON API 1.0 string format.
     * @param <T> 
     * @param t Given object
     * @param type 
     * @return String The JSON API formatted string.
     * @throws java.lang.IllegalAccessException
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    public static <T extends Object> String serialized(T t, Class<T> type) throws IllegalAccessException, JsonProcessingException             
    {
        ResourceConverter converter = new ResourceConverter(type);
        return  new String(converter.writeObject(t));        
    }
    

    /**
     * Converts given instance of given type into JSON API 1.0 string format.
     * @param <T> 
     * @param <R> 
     * @param t Given object
     * @param type 
     * @param relationType 
     * @return String The JSON API formatted string.
     * @throws java.lang.IllegalAccessException
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    public static <T,R extends Object> String serializedWithRelatioship(T t, Class<T> type, Class<R> relationType) throws IllegalAccessException, JsonProcessingException             
    {
        ObjectMapper mapper = new ObjectMapper();
        ResourceConverter converter = new ResourceConverter(mapper, type, relationType);
        return new String(converter.writeObject(t));        
    }      
    
    /**
     *
     * @param a - Is the current Class
     * @param b - Is the Superclass;
     * @return
     */
    public static Field[] mergeClassFields(Field[] a, Field[] b) {

        int aLen = a.length;
        int bLen = b.length;

        if (bLen == 0) {
            return a;
        }

        Field[] fields = new Field[aLen + bLen];
        System.arraycopy(a, 0, fields, 0, aLen);
        System.arraycopy(b, 0, fields, aLen, bLen);
        return fields;

    }      
}
