package org.atlas.applications.startup;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.Response.Status;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.atlas.applications.modules.repo.domain.MimeType;
import org.atlas.applications.modules.repo.services.impl.MimeTypeServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Initializes database data source connection for the application and performs any startup one-time operations.
 * @author ic
 */
@DependsOn("MimeTypeServiceImpl")
@Startup
@Singleton
@DataSourceDefinition(
    name="java:global/jdbc/AtlasApplicationsRepo_ds", 
    user="root",
    password="22031987",
    //password="root",
    className="com.mysql.cj.jdbc.Driver",    
    url="jdbc:mysql://localhost:3306/db_atlas_applications_repository?useSSL=false"
        + "&nullNamePatternMatchesAll=true&createDatabaseIfNotExist=true&useUnicode=yes&characterEncoding=utf-8"
            + "&useLegacyDatetimeCode=false&serverTimezone=Europe/Athens")
public class DatasourceInitialization
{
    @EJB
    private MimeTypeServiceImpl mimeTypeServiceImpl;       
    
    private static final Logger logger = LoggerFactory.getLogger(DatasourceInitialization.class);  
    private static final String CATALINA_BASE = System.getProperty("catalina.base");
        
    @PostConstruct
    @SuppressWarnings("unused")
    public void init()
    {
        List<MimeType> mimeTypes = mimeTypeServiceImpl.findAll();
        if(mimeTypes.isEmpty()){
            // pre load and  save mime types from resource file
            mimeTypeServiceImpl.preInsertMimeTypes();
        }
        
        try {
            // create first-time repository dir path in file system            
            InitialContext initialContext = new javax.naming.InitialContext();
            String repoPath = (String)initialContext.lookup("java:comp/env/Applications.Repository.Path");
            String fullpath = CATALINA_BASE.concat(repoPath);
            File file = new File(fullpath);
            Path path  = Paths.get(file.getAbsolutePath());        
            Files.createDirectories(path);
            logger.info("Creating applications repository directory path..."+ file.getAbsolutePath()); 
        } catch (NamingException | IOException ex) {
            logger.error(DatasourceInitialization.class.getName(), ex);
            throw new RestApplicationException(Status.BAD_REQUEST, "400", ex.getMessage());
        }

    }
    
}
