package org.atlas.applications.jaxrs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.jasminb.jsonapi.exceptions.DocumentSerializationException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.atlas.applications.api.ApiEndpoint;
import org.atlas.applications.api.annotations.PATCH;
import org.atlas.applications.api.annotations.RequiredParam;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.extensions.JSONApiIncludeType;
import org.atlas.applications.core.jsonapi.helpers.TopLevelLinksProperties;
import org.atlas.applications.core.utilities.Utilities;
import org.atlas.applications.modules.core.IApplicationResponse;
import org.atlas.applications.modules.core.IncludedDataParser;
import org.atlas.applications.modules.jsonapi.ApplicationJsonApiDocumentCreator;
import org.atlas.applications.modules.repo.domain.Application;
import org.atlas.applications.modules.repo.domain.ApplicationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.atlas.applications.modules.jsonapi.IJsonApiDocumentCreator;
import org.atlas.applications.modules.core.BaseApplicationServiceImpl;
import org.atlas.applications.modules.core.IBaseApplicationService;

/**
 * Contains restful methods to serve application repository endpoints using API version V2.
 * Supports operations to create, update, delete, disable, patch and list application data.
 * @author ic
 */
@Path(ApiEndpoint.API_PATH_V2)
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ApplicationRepositoryControllerApiV2
{    
    @EJB private IApplicationResponse applicationResponseService;          
    @Context HttpServletRequest httpServletRequest;    
    @EJB private IBaseApplicationService baseApplicationService;
    
    private static final Logger logger = LoggerFactory.getLogger(ApplicationRepositoryControllerApiV2.class); 
    private static String PAGE_NUMBER_PARAM = "page[number]";
    private static String PAGE_SIZE_PARAM = "page[number]";    

    @GET    
    @Path(ApiEndpoint.APPLICATIONS)
    @Produces(MediaType.APPLICATION_JSON)
    public <T extends IJsonApiDocumentCreator> JSONApiDocument getAllApplications(
        @RequiredParam @QueryParam(value = "filter") String filter, @QueryParam(value = "page[number]") int page, @QueryParam(value = "page[size]") int size) throws InstantiationException, IllegalAccessException
    {        
        List<Application> applications = new ArrayList();
        int count = 0;
        int isPagedMode = page>0 && size>0 ? 1 : 0;
        if(!Utilities.isEmpty(filter))
        {            
            switch(filter.toLowerCase()){
                case "all":      
                    switch (isPagedMode)
                    {
                        case 1:
                            applications = baseApplicationService.findAllApplications(page, size);
                            count = baseApplicationService.getCount();
                            break;
                        case 0:
                            applications = baseApplicationService.findAllApplications();
                            break;
                    }                    
                    break;
                case "enabled":                    
                    switch (isPagedMode) 
                    {
                        case 1:
                            applications = baseApplicationService.findApplications(ApplicationStatus.ENABLED, page, size);
                            count = baseApplicationService.getCount(ApplicationStatus.ENABLED);
                            break;
                        case 0:
                            applications = baseApplicationService.findApplications(ApplicationStatus.ENABLED);
                            break;
                    }                    
                    break;
                case "disabled":                    
                    switch (isPagedMode)
                    {
                        case 1:
                            applications = baseApplicationService.findApplications(ApplicationStatus.DISABLED, page, size);
                            count = baseApplicationService.getCount(ApplicationStatus.DISABLED);
                            break;
                        case 0:
                            applications = baseApplicationService.findApplications(ApplicationStatus.DISABLED);
                            break;
                    }                    
                    break;
                case "deleted":                      
                    switch (isPagedMode)
                    {
                        case 1:
                            applications = baseApplicationService.findApplications(ApplicationStatus.DELETED, page, size);
                            count = baseApplicationService.getCount(ApplicationStatus.DELETED);
                            break;
                        case 0:
                            applications = baseApplicationService.findApplications(ApplicationStatus.DELETED);
                            break;
                    }                    
                    break;                   
            }
        }                          
        
        // set top-level links
        TopLevelLinksProperties toplevelLinksProperties  = new TopLevelLinksProperties(count, page, size, BaseApplicationServiceImpl.getCompleteServerRequestUrl(httpServletRequest));                        
        return baseApplicationService.getApplicationsWithLatestVersion(applications, ApplicationJsonApiDocumentCreator.class, toplevelLinksProperties);
    }        
   
    @GET    
    @Path(ApiEndpoint.APPLICATION_TRANSACTIONS)    
    @Produces(MediaType.APPLICATION_JSON)
    public JSONApiDocument getApplication(@RequiredParam @PathParam(value = "id") long id, @QueryParam(value="version") String version, @QueryParam(value="include") String include) 
            throws DocumentSerializationException, SocketException, JsonProcessingException, InstantiationException, IllegalAccessException
    {   
        Application application = baseApplicationService.findApplication(id);
        if (application == null) {
            throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND);            
        }
        // no inclusion of versions
        if(Utilities.isEmpty(include))
        {                        
            return baseApplicationService.getApplicationWithVersion(application, version, ApplicationJsonApiDocumentCreator.class);               
        }
        // include versions
        else
        {   
            // parse include parameters
            IncludedDataParser includeParser = new IncludedDataParser(include);
            JSONApiIncludeType includeType =  includeParser.getIncludeType();                           
            if(includeParser.getIncludeType() != null){                                                
                return baseApplicationService.getApplicationIncludingData(application, ApplicationJsonApiDocumentCreator.class, includeType); 
            }
            else
            {
                throw new RestApplicationException(Response.Status.BAD_REQUEST, ApiError.INVALID_RELATIONSHIP_PATH,  ": " + include); 
            }
        }
    }    
    
    @DELETE
    @Path(ApiEndpoint.APPLICATION_TRANSACTIONS)
    @Produces(MediaType.APPLICATION_JSON)
    public JSONApiDocument deleteApplication(@RequiredParam @PathParam(value = "id") long id, @QueryParam(value="version") String version) throws SocketException, DocumentSerializationException, InstantiationException, IllegalAccessException
    {
        Application application = baseApplicationService.findApplication(id);
        if (application == null) {
            throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND);
        }        
        return baseApplicationService.setStatus(application, version, ApplicationStatus.DELETED, ApplicationJsonApiDocumentCreator.class);
    }       
        
    @GET
    @Path(ApiEndpoint.APPLICATION_DISABLE)
    @Produces(MediaType.APPLICATION_JSON)   
    public JSONApiDocument disableApplication(@RequiredParam @PathParam(value = "id") long id, @QueryParam(value="version") String version) throws SocketException, DocumentSerializationException, InstantiationException, IllegalAccessException
    {
        Application application = baseApplicationService.findApplication(id);
        if (application == null) {
            throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND);
        }                
        return baseApplicationService.setStatus(application, version, ApplicationStatus.DISABLED, ApplicationJsonApiDocumentCreator.class);
    }     
    
    @GET
    @Path(ApiEndpoint.APPLICATION_ENABLE)
    @Produces(MediaType.APPLICATION_JSON)  
    public JSONApiDocument enableApplication(@RequiredParam @PathParam(value = "id") long id, @QueryParam(value="version") String version) throws SocketException, DocumentSerializationException, InstantiationException, InstantiationException, InstantiationException, IllegalAccessException
    {
        Application application = baseApplicationService.findApplication(id);
        if (application == null) {
            throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND);
        }        
        return baseApplicationService.setStatus(application, version, ApplicationStatus.ENABLED, ApplicationJsonApiDocumentCreator.class);
    }     
    
    @POST 
    @Path(ApiEndpoint.APPLICATIONS)        
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public JSONApiDocument uploadApplication(@Context HttpServletRequest httpServletRequest) 
            throws DocumentSerializationException, UnsupportedEncodingException, FileUploadException, IOException, ServletException, InstantiationException, IllegalAccessException
    {            
        // get multiparts
        List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(httpServletRequest);   

        // upload to server
        baseApplicationService.uploadApplication(multiparts);
        return baseApplicationService.getApplicationWithVersion(baseApplicationService.getApplication(), baseApplicationService.getApplicationContent().getVersion(), ApplicationJsonApiDocumentCreator.class);          
    }       
    
    @PUT    
    @Path(ApiEndpoint.APPLICATION_TRANSACTIONS)         
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public JSONApiDocument updateApplication(@RequiredParam @PathParam(value = "id") long id, @Context HttpServletRequest httpServletRequest) 
        throws DocumentSerializationException, SocketException, FileUploadException, InstantiationException, IllegalAccessException
    {        
        Application application = baseApplicationService.findApplication(id);
        if(application == null){
            throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND);
        } 
        
        // get multiparts
        List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(httpServletRequest);           
        
        // upload to server
        baseApplicationService.setApplication(application);
        baseApplicationService.updateApplication(multiparts);
        return baseApplicationService.getApplicationWithVersion(baseApplicationService.getApplication(), baseApplicationService.getApplicationContent().getVersion(), ApplicationJsonApiDocumentCreator.class);   
    }           
    
    @PATCH
    @Path(ApiEndpoint.APPLICATION_TRANSACTIONS)         
    @Produces(MediaType.APPLICATION_JSON)    
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public JSONApiDocument patchApplication(@RequiredParam @PathParam(value = "id") long id,
    @Context HttpServletRequest httpServletRequest) throws FileUploadException, DocumentSerializationException, InstantiationException, IllegalAccessException
    {        
        Application application = baseApplicationService.findApplication(id);
        if(application == null){
            throw new RestApplicationException(Response.Status.NOT_FOUND, ApiError.APPLICATION_NOT_FOUND);
        }                        
        
        // get multiparts
        List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(httpServletRequest);        
        
        // upload to server
        baseApplicationService.setApplication(application);
        baseApplicationService.patchApplication(multiparts);
        return baseApplicationService.getApplicationWithVersion(baseApplicationService.getApplication(), baseApplicationService.getApplicationContent().getVersion(), ApplicationJsonApiDocumentCreator.class);   
    }         
     
    @GET
    @Path(ApiEndpoint.APPLICATION_DOWNLOAD)    
    public Response downloadApplication(@RequiredParam @PathParam(value = "id") long id, @QueryParam(value = "version") String version) throws UnsupportedEncodingException
    {
        if(Utilities.isEmpty(version)){
            return applicationResponseService.getFile(id);
        }
        return applicationResponseService.getFile(id, version);
    }  
    
}
