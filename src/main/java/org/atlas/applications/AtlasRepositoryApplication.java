package org.atlas.applications;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.atlas.applications.api.providers.filters.RequestLoggingFilter;
import org.atlas.applications.api.providers.writers.JSONAPIDocumentWriter;
import org.atlas.applications.api.providers.mappers.ApplicationExceptionMapper;
import org.atlas.applications.api.providers.writers.JSONApiDocumentBodyWriter;
import org.atlas.applications.api.providers.mappers.PersistenceExceptionMapper;
import org.atlas.applications.api.providers.filters.RequestParameterFilter;
import org.atlas.applications.api.providers.filters.ResponseLoggingFilter;
import org.atlas.applications.api.providers.mappers.RestApplicationExceptionMapper;
import org.atlas.applications.api.providers.mappers.ValidationExceptionMapper;
import org.atlas.applications.api.providers.mappers.RuntimeExceptionMapper;
import org.atlas.applications.core.security.OAuthContainerRequestFilter;
import org.atlas.applications.jaxrs.ApplicationRepositoryControllerApiV1;
import org.atlas.applications.jaxrs.ApplicationRepositoryControllerApiV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main starting point for atlas applications repository application.
 * @author ic
 */
@ApplicationPath("")
public class AtlasRepositoryApplication extends Application
{
    private static final Logger logger = LoggerFactory.getLogger(AtlasRepositoryApplication.class);    
    
    @Override
    public Set<Class<?>> getClasses()
    {
        Set<Class<?>> resources = new HashSet<>();
        
        logger.info("Adding ApplicationRepositoryControllerApiV1 class to AtlasRepositoryApplication application...");
        resources.add(ApplicationRepositoryControllerApiV1.class);
        
        logger.info("Adding ApplicationRepositoryControllerApiV2 class to AtlasRepositoryApplication application...");
        resources.add(ApplicationRepositoryControllerApiV2.class);      
                                       
        logger.info("Adding RequiredParameterFilter class to AtlasRepositoryApplication application...");
        resources.add(RequestParameterFilter.class);
        
        logger.info("Adding RequestLoggingFilter class to AtlasRepositoryApplication application...");
        resources.add(RequestLoggingFilter.class);    
        
        logger.info("Adding ResponseLoggingFilter class to AtlasRepositoryApplication application...");
        resources.add(ResponseLoggingFilter.class);        
                
        logger.info("Adding ApplicationExceptionMapper class to AtlasRepositoryApplication application...");
        resources.add(ApplicationExceptionMapper.class);
        
        logger.info("Adding ValidationExceptionMapper class to AtlasRepositoryApplication application...");
        resources.add(ValidationExceptionMapper.class);    
        
        logger.info("Adding OAuthContainerRequestFilter class to AtlasRepositoryApplication application...");
        resources.add(OAuthContainerRequestFilter.class);    
        
        logger.info("Adding PersistenceExceptionMapper class to AtlasRepositoryApplication application...");
        resources.add(PersistenceExceptionMapper.class);  
        
        logger.info("Adding JSONApiMessageBodyWriter class to AtlasRepositoryApplication application...");
        resources.add(JSONAPIDocumentWriter.class);
        
        logger.info("Adding JSONApiDocumentBodyWriter class to AtlasRepositoryApplication application...");
        resources.add(JSONApiDocumentBodyWriter.class);        
        
        logger.info("Adding RestApplicationExceptionMapper class to AtlasRepositoryApplication application...");
        resources.add(RestApplicationExceptionMapper.class);      
        
        logger.info("Adding RuntimeExceptionMapper class to AtlasRepositoryApplication application...");
        resources.add(RuntimeExceptionMapper.class);     
        
        //logger.info("Adding ParamConverterProvider class to AtlasRepositoryApplication application...");
        //resources.add(ParamConverterProvider.class);        
        
        //logger.info("Adding ExtendedJsonApiDocumentWriter class to AtlasRepositoryApplication application...");
        //resources.add(ExtendedJsonApiDocumentWriter.class);          
        
        logger.info("Getting root resource classes for AtlasRepositoryApplication application...", resources);
        
        return resources;

    }  
    
  
        
}
