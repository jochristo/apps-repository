package org.atlas.applications.api;

/**
 * Defines the RESTful API endpoints of the application.
 * @author ic
 */
public abstract class ApiEndpoint
{    
    /**
     * Rest API prefix
     */
    public static final String API_PATH = "/rest";
    
    public static final String API_PATH_V1 = "/rest/v1";
    
    public static final String API_PATH_V2 = "/rest/v2";

    public static final String AUTHENTICATION_LOGIN = "/auth/login";

    public static final String AUTHENTICATION_LOGOUT = "/auth/logout";

    public static final String AUTHENTICATION_SIGNUP = "/auth/signup";

    public static final String APPLICATIONS = "/applications"; // GET, POST

    public static final String APPLICATION_TRANSACTIONS = "/applications/{id}"; // GET, PUT, DELETE, PATCH

    public static final String APPLICATION_DOWNLOAD = "/applications/{id}/download"; // GET
    
    public static final String APPLICATION_DISABLE = "/applications/{id}/disable"; // GET
    
    public static final String APPLICATION_ENABLE = "/applications/{id}/enable"; // GET
    
}
