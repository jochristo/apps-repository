/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.api.providers.converters;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Admin
 */

@Provider
public class ParamConverterProvider implements ParamConverter<Object>
{

    @Override
    public Object fromString(String string) {
        String source = string;
        try {
            source = URLDecoder.decode(string, "UTF-8");
            return source;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ParamConverterProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        return source;
    }

    @Override
    public String toString(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
