package org.atlas.applications.api.providers.filters;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.atlas.applications.modules.core.BaseApplicationServiceImpl;

/**
 *
 * @author ic
 */
@Provider
public class ResponseLoggingFilter implements ContainerResponseFilter
{
    private static final Logger logger = LogManager.getLogger(org.atlas.applications.api.logging.RequestLoggingFilter.class); 
    @Context HttpServletRequest httpServletRequest;
    
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        
        MultivaluedMap<String, String> queryParams = requestContext.getUriInfo().getQueryParameters();
        String queryString = "?" + httpServletRequest.getQueryString();
        final StringBuilder logMessage = new StringBuilder("Request ended: [")
            //.append("HTTP METHOD: [")
            //.append(requestContext.getRequest().getMethod())
            //.append("] PATH INFO: [")
            .append(BaseApplicationServiceImpl.getCompleteServerRequestUrl(httpServletRequest) + "]");                
            //.append(requestContext.getUriInfo().getAbsolutePath().toString());
            //if(queryString != "?null"){
            //    logMessage.append(queryString);
            //}
            //.append("] REQUEST PARAMETERS: ")
            //.append(queryParams)
            logMessage.append(" with HTTP status ")    
            .append(responseContext.getStatus());
        logger.info(logMessage.toString());
        
        
    }
    
}
