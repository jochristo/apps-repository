/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.api.providers.mappers;

import com.github.jasminb.jsonapi.JSONAPIDocument;
import com.github.jasminb.jsonapi.ResourceConverter;
import com.github.jasminb.jsonapi.exceptions.DocumentSerializationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.atlas.applications.api.errors.ApiError;

/**
 *
 * @author Admin
 */
@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException>
{

    @Override
    public Response toResponse(NotFoundException ex) {
        try {
            // Create JsonApi object
            JSONAPIDocument document = new JSONAPIDocument();
            List<com.github.jasminb.jsonapi.models.errors.Error> errors = new ArrayList();
            
            // create error object
            com.github.jasminb.jsonapi.models.errors.Error error = new com.github.jasminb.jsonapi.models.errors.Error();
            error.setStatus(ApiError.RESOURCE_NOT_FOUND.getHttpCode());
            error.setCode(ApiError.RESOURCE_NOT_FOUND.getAppCode());
            error.setDetail(ex.getMessage());
            errors.add(error);
            document = JSONAPIDocument.createErrorDocument(errors);
            
            // create string response from json error
            ResourceConverter converter = new ResourceConverter();
            byte[] serializedObject = converter.writeDocument(document);
            String serializedAsString = new String(serializedObject);
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(serializedAsString)
                    .build();
        } catch (DocumentSerializationException ex1) {
            Logger.getLogger(ApplicationExceptionMapper.class.getName()).log(Level.SEVERE, null, ex1);
        }
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(ex.getMessage())
                    .build();
    }
    
}
