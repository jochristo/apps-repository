/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.api.providers.mappers;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.atlas.applications.api.exceptions.RestApplicationException;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.JSONApiErrors;

/**
 *
 * @author ic
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class RestApplicationExceptionMapper implements ExceptionMapper<RestApplicationException>
{
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Response toResponse(RestApplicationException ex) {
        
        try {
            
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                    
        // Create JsonApi object        
        JSONApiDocument document = new JSONApiDocument();
        
        // create list of errors
        List<JSONApiErrors> errors = new ArrayList();
        
        // create error object        
        JSONApiErrors error = new JSONApiErrors();        
        error.setStatus(String.valueOf(ex.getStatus().getStatusCode()));
        error.setCode(ex.getAppCode());
        error.setDetail(ex.getDetails());        
        errors.add(error);
        
        // set document error list
        document.setErrors((ArrayList<JSONApiErrors>) errors);
        
        // create json string
        String json = mapper.writeValueAsString(document);
        return Response.status(ex.getStatus().getStatusCode()).entity(json).build();
        //return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (JsonProcessingException ex1) {
            Logger.getLogger(RestApplicationExceptionMapper.class.getName()).log(Level.SEVERE, null, ex1);
        }   
        
        return Response.status(ex.getStatus()).entity(ex.getDetails()).build();        
    }
    
    
}
