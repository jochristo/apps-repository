package org.atlas.applications.api.providers.mappers;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import static org.atlas.applications.api.errors.ApiError.CONSTRAINT_VIOLATION;
import org.atlas.applications.core.jsonapi.JSONApiDocument;
import org.atlas.applications.core.jsonapi.JSONApiErrors;

/**
 * ConstraintViolationException mapper.
 * @author ic
 */
@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ConstraintViolationException> { 

    private final ObjectMapper mapper = new ObjectMapper();   
    
    @Override
    public Response toResponse(ConstraintViolationException ex) {
            try {
            
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        
        // Create JsonApi object        
        JSONApiDocument document = new JSONApiDocument();
        
        // create list of errors
        List<JSONApiErrors> errors = new ArrayList();
        
        StringBuilder sb = new StringBuilder();
        ex.getConstraintViolations().forEach((v) -> {
            
        // create error object        
        JSONApiErrors error = new JSONApiErrors();
        error.setStatus(String.valueOf(Status.BAD_REQUEST.getStatusCode()));
        error.setCode(CONSTRAINT_VIOLATION.getAppCode());
        error.setDetail(v.getMessage());
        errors.add(error);
        });        

        // set document error list
        document.setErrors((ArrayList<JSONApiErrors>) errors);
        
        // create json string
        String json = mapper.writeValueAsString(document);
        return Response.status(Status.BAD_REQUEST).entity(json).build();   
        } catch (JsonProcessingException ex1) {
            Logger.getLogger(RestApplicationExceptionMapper.class.getName()).log(Level.SEVERE, null, ex1);
        }   
        return Response.status(Status.BAD_REQUEST).entity(unwrapException(ex)).build();
    }
    
    protected String unwrapException(Throwable t) {
        StringBuffer sb = new StringBuffer();
        unwrapException(sb, t);
        return sb.toString();
    }    
    
    private void unwrapException(StringBuffer sb, Throwable t) {
        if (t == null) {
            return;
        }
        sb.append(t.getMessage());
        if (t.getCause() != null && t != t.getCause()) {
            sb.append('[');
            unwrapException(sb, t.getCause());
            sb.append(']');
        }
    }  
    
}
