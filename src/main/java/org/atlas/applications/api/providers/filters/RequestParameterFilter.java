package org.atlas.applications.api.providers.filters;

import java.io.IOException;
import java.lang.reflect.Parameter;
import java.net.URLDecoder;
import java.util.Map;
import javax.ws.rs.FormParam;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;
import org.atlas.applications.api.Globals;
import org.atlas.applications.api.annotations.RequiredParam;
import org.atlas.applications.api.errors.ApiError;
import org.atlas.applications.api.exceptions.RestApplicationException;

/**
 * Container request filter class.
 * @author ic
 */
@Provider
public class RequestParameterFilter implements ContainerRequestFilter
{
    @Context
    private ResourceInfo resourceInfo;

    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException
    {                  
        // check method and its content type
        String method = requestContext.getMethod();
        if(method.equals(HttpMethod.POST) || method.equalsIgnoreCase(HttpMethod.PUT) || method.equalsIgnoreCase(Globals.PATCH)){
            MultivaluedMap<String,String> headers = requestContext.getHeaders();
            String contentType = headers
            .getFirst(HttpHeaders.CONTENT_TYPE);            
            if(contentType == null){
                throw new RestApplicationException(Status.BAD_REQUEST, ApiError.MISSING_MULTIPART_PARAMETERS);   
            }
            if(!contentType.startsWith(MediaType.MULTIPART_FORM_DATA)){
                throw new RestApplicationException(Status.BAD_REQUEST, ApiError.INVALID_APPLICATION_FILE_TYPE);  
            }
        }
        
        // Loop through each parameter
        for (Parameter parameter : resourceInfo.getResourceMethod().getParameters()) {
            // Check parameter type
            QueryParam queryAnnotation = parameter.getAnnotation(QueryParam.class);
            PathParam pathParamAnnotation = parameter.getAnnotation(PathParam.class);
            FormParam formParamAnnotation = parameter.getAnnotation(FormParam.class);
                        
            // process params...
            if ((formParamAnnotation != null) && parameter.isAnnotationPresent(RequiredParam.class)) {
                if (!requestContext.getUriInfo().getQueryParameters().containsKey(formParamAnnotation.value())) {
                    RequiredParam requiredParam = parameter.getAnnotation(RequiredParam.class);
                    throw new RestApplicationException(Status.BAD_REQUEST, ApiError.ILLEGAL_ARGUMENT_TYPE.getAppCode(), formParamAnnotation.value() + " " + requiredParam.value());
                }
            }
            if ((queryAnnotation != null) && parameter.isAnnotationPresent(RequiredParam.class)) {
                Map queryParams = requestContext.getUriInfo().getQueryParameters();
                String qannot = URLDecoder.decode(queryAnnotation.value(), "UTF-8");
                if (!requestContext.getUriInfo().getQueryParameters().containsKey(qannot)) {
                    RequiredParam requiredParam = parameter.getAnnotation(RequiredParam.class);
                    throw new RestApplicationException(Status.BAD_REQUEST, ApiError.ILLEGAL_ARGUMENT_TYPE.getAppCode(), queryAnnotation.value() + " " + requiredParam.value());
                }
            }
            if ((pathParamAnnotation != null) && parameter.isAnnotationPresent(RequiredParam.class)) {
                if (!requestContext.getUriInfo().getPathParameters().containsKey(pathParamAnnotation.value())) {
                    RequiredParam requiredParam = parameter.getAnnotation(RequiredParam.class);
                    throw new RestApplicationException(Status.BAD_REQUEST, ApiError.ILLEGAL_ARGUMENT_TYPE.getAppCode(), pathParamAnnotation.value() + " " + requiredParam.value());
                }
            }
        }     
    }
    
  
    
}
