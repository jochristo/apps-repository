package org.atlas.applications.api.providers.mappers;

import com.github.jasminb.jsonapi.JSONAPIDocument;
import com.github.jasminb.jsonapi.ResourceConverter;
import com.github.jasminb.jsonapi.exceptions.DocumentSerializationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.atlas.applications.api.exceptions.ApplicationException;

/**
 * Application exception mapper.
 * @author ic
 */
@Provider
public class ApplicationExceptionMapper implements ExceptionMapper<ApplicationException> 
{
    @Override    
    public Response toResponse(ApplicationException ex)
    {        
        try {
            // Create JsonApi object
            JSONAPIDocument document = new JSONAPIDocument();
            List<com.github.jasminb.jsonapi.models.errors.Error> errors = new ArrayList();
            
            // create error object
            com.github.jasminb.jsonapi.models.errors.Error error = new com.github.jasminb.jsonapi.models.errors.Error();            
            error.setStatus(String.valueOf(ex.getStatus().getStatusCode()));
            error.setCode(ex.getAppCode());
            error.setDetail(ex.getDetails());
            errors.add(error);
            document = JSONAPIDocument.createErrorDocument(errors);
            
            // create string response from json error
            ResourceConverter converter = new ResourceConverter();
            byte[] serializedObject = converter.writeDocument(document);
            String serializedAsString = new String(serializedObject);
            return Response.status(Integer.parseInt(error.getStatus()))
                    .entity(serializedAsString)
                    .build();
        } catch (DocumentSerializationException ex1) {
            Logger.getLogger(ApplicationExceptionMapper.class.getName()).log(Level.SEVERE, null, ex1);
        }
            return Response
                    .status(ex.getStatus())
                    .entity(ex.getDetails())
                    .build();        
    }
}
