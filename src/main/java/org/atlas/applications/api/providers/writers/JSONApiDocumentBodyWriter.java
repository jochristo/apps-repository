/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.api.providers.writers;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import org.atlas.applications.core.jsonapi.JSONApiDocument;

/**
 *
 * @author ic
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class JSONApiDocumentBodyWriter implements MessageBodyWriter<JSONApiDocument>
{
    private final ObjectMapper mapper = new ObjectMapper();

    public JSONApiDocumentBodyWriter() {
        //mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);        
        //mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);        
    }   
    

    @Override
    public boolean isWriteable(Class<?> type, Type type1, Annotation[] antns, MediaType mt) {
        return true;
    }

    @Override
    public long getSize(JSONApiDocument t, Class<?> type, Type type1, Annotation[] antns, MediaType mt) {
        return 1;
    }

    @Override
    public void writeTo(JSONApiDocument t, Class<?> type, Type type1, Annotation[] antns, MediaType mt, MultivaluedMap<String, Object> mm, OutputStream out) throws IOException, WebApplicationException {
        out.write(mapper.writeValueAsBytes(t));
    }
    
}
