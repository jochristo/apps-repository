package org.atlas.applications.api.providers.writers;

import com.github.jasminb.jsonapi.JSONAPIDocument;
import com.github.jasminb.jsonapi.ResourceConverter;
import com.github.jasminb.jsonapi.SerializationFeature;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.exceptions.DocumentSerializationException;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import org.atlas.applications.core.utilities.ClassUtils;
import org.atlas.applications.core.utilities.Utilities;

/**
 * Message body writer for {@link JSONApiDocument} objects.
 * @author ic
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class JSONAPIDocumentWriter implements MessageBodyWriter<JSONAPIDocument>
{    

    @Override
    public boolean isWriteable(Class<?> type, Type type1, Annotation[] antns, MediaType mt) {
        return true;
    }

    @Override
    public long getSize(JSONAPIDocument t, Class<?> type, Type type1, Annotation[] antns, MediaType mt) {
        return 1;
    }

    @Override
    public void writeTo(JSONAPIDocument t, Class<?> type, Type type1, Annotation[] antns, MediaType mt, MultivaluedMap<String, Object> mm, OutputStream out) throws IOException, WebApplicationException {
        
        // try to get type: single or list                      
        try {
            // try read relationship lists
            if (t.get() != null) {
                if (!(t.get() instanceof List)) { // get only single object not list
                    ResourceConverter resourceConverter = new ResourceConverter(t.get().getClass());
                    resourceConverter.enableSerializationOption(SerializationFeature.INCLUDE_LINKS);                                       
                    Object obj = t.get();
                    Field[] fields = obj.getClass().getDeclaredFields();
                    if (!Utilities.isEmpty(fields)) {
                        for (Field f : fields) {
                            if (f.isAnnotationPresent(Relationship.class)) {
                                Object fieldValue = ClassUtils.getFieldValue(f, obj);
                                if (fieldValue instanceof List) {
                                    List list = (List) fieldValue;
                                    if (list.size() > 1) {
                                        resourceConverter.enableSerializationOption(SerializationFeature.INCLUDE_RELATIONSHIP_ATTRIBUTES);                                        
                                    }
                                }
                            }

                        }
                    }
                    out.write(resourceConverter.writeDocument(t));
                } else if (t.get() instanceof List) {
                    List<Object> list = (List<Object>) t.get();
                    ResourceConverter resourceConverter = new ResourceConverter();
                    resourceConverter.enableSerializationOption(SerializationFeature.INCLUDE_LINKS);
                    if (list.size() > 1) {
                        resourceConverter = new ResourceConverter(list.get(0).getClass());
                        resourceConverter.enableSerializationOption(SerializationFeature.INCLUDE_RELATIONSHIP_ATTRIBUTES);
                    }
                    out.write(resourceConverter.writeDocumentCollection(t));
                }
            }

        } catch (InvocationTargetException | DocumentSerializationException ex) {
            Logger.getLogger(JSONAPIDocumentWriter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    
}
