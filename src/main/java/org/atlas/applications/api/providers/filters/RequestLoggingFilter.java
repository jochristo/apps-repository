package org.atlas.applications.api.providers.filters;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.atlas.applications.modules.core.BaseApplicationServiceImpl;


/**
 * @author ic
 */
@Provider
public class RequestLoggingFilter implements ContainerRequestFilter {

    private static final Logger logger = LogManager.getLogger(org.atlas.applications.api.logging.RequestLoggingFilter.class); 
    @Context HttpServletRequest httpServletRequest;
    
    @Override
    public void filter(ContainerRequestContext context) throws IOException {        
        String queryString = "?" + httpServletRequest.getQueryString();
        MultivaluedMap<String, String> queryParams = context.getUriInfo().getQueryParameters();
        final StringBuilder logMessage = new StringBuilder("Request started: ")
            .append("[")
            .append(context.getRequest().getMethod())
            .append("] PATH: [")
            .append(BaseApplicationServiceImpl.getCompleteServerRequestUrl(httpServletRequest) + "]");
            //.append(context.getUriInfo().getAbsolutePath().toString());
            //{
            //    logMessage.append(queryString);
            //}
            //.append("] REQUEST PARAMETERS: ")
            //.append(queryParams);
        logger.info(logMessage.toString());
        
     
    }
    

}