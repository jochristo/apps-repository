/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.api;

/**
 * Contains global constants.
 * @author ic
 */
public interface Globals {
    
    public final static String TRUE = "true";
    public final static String FALSE = "false";    
    public final static int APPS_PATH_DEPTH = 2;
    public final static String PATCH = "PATCH";
}
