package org.atlas.applications.api.logging;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Request filtering with logging.
 * @author ic
 */
public class RequestLoggingFilter implements Filter
{
    private static final Logger log = LoggerFactory.getLogger(RequestLoggingFilter.class); 

    @Override
    public void init(FilterConfig fc) throws ServletException {        
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        
        HttpServletRequest httpServletRequest = (HttpServletRequest) sr;
        Map<String, String> requestMap = this.getTypesafeRequestMap(httpServletRequest);
        final StringBuilder logMessage = new StringBuilder("REST Request - ")
                .append("[HTTP METHOD:")
                .append(httpServletRequest.getMethod())
                .append("] [PATH INFO:")
                .append(httpServletRequest.getRequestURI())
                .append("] [REQUEST PARAMETERS:")
                .append(requestMap)                                       
                .append("] [REMOTE ADDRESS:")
                .append(httpServletRequest.getRemoteAddr())
                .append("]");

        //Get the IP address of client machine.
        String ipAddress = sr.getRemoteAddr();
        //Log the IP address and current timestamp.      
        log.info("IP " + ipAddress + ", Time " + new Date().toString());
        log.info(logMessage.toString());
        fc.doFilter(sr, sr1);
    }

    @Override
    public void destroy() {        
    }
    
    private Map<String, String> getTypesafeRequestMap(HttpServletRequest request) {
        Map<String, String> typesafeRequestMap = new HashMap<String, String>();
        Enumeration<?> requestParamNames = request.getParameterNames();
        while (requestParamNames.hasMoreElements()) {
            String requestParamName = (String) requestParamNames.nextElement();
            String requestParamValue = request.getParameter(requestParamName);
            typesafeRequestMap.put(requestParamName, requestParamValue);
        }
        return typesafeRequestMap;
    }

}