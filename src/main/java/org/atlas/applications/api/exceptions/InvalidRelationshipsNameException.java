/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.api.exceptions;

import javax.ws.rs.core.Response;
import org.atlas.applications.api.errors.ApiError;

/**
 *
 * @author Admin
 */
public class InvalidRelationshipsNameException extends RestApplicationException
{
    public InvalidRelationshipsNameException(ApiError apiError) {
        super(apiError);
    }

    public InvalidRelationshipsNameException(String appCode, String details) {
        super(appCode, details);
    }

    public InvalidRelationshipsNameException(Response.Status status, String appCode) {
        super(status, appCode);
    }

    public InvalidRelationshipsNameException(Response.Status status, String appCode, String details) {
        super(status, appCode, details);
    }

    public InvalidRelationshipsNameException(Response.Status status, ApiError apiError) {
        super(status, apiError);
    }
}
