/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.api.exceptions;

import javax.ws.rs.core.Response;
import org.atlas.applications.api.errors.ApiError;

/**
 *
 * @author Admin
 */
public class RestApplicationException extends RuntimeException{

    private static final long serialVersionUID = -7498621923421702089L;

    protected Response.Status status;
    protected String appCode;
    protected String title;
    protected String details;

    public RestApplicationException(ApiError apiError) {
        this.appCode = apiError.getAppCode();
        this.details = apiError.getDetails();
    }

    public RestApplicationException(String appCode, String details) {
        this.appCode = appCode;
        this.details = details;
    }

    public RestApplicationException(Response.Status status, String appCode) {
        this.status = status;
        this.appCode = appCode;
    }

    public RestApplicationException(Response.Status status, String appCode, String details) {
        this.status = status;
        this.appCode = appCode;
        this.details = details;
    }    
    
    public RestApplicationException(Response.Status status, ApiError apiError) {
        this.status = status;
        this.appCode = apiError.getAppCode();
        this.details = apiError.getDetails();
    }    
    
    public RestApplicationException(Response.Status status, ApiError apiError, String details) {
        this.status = status;
        this.appCode = apiError.getAppCode();        
        this.details = apiError.getDetails() + details;
    }      

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Response.Status getStatus() {
        return status;
    }

    public void setStatus(Response.Status status) {
        this.status = status;
    }
    
    
}
