package org.atlas.applications.api.exceptions;

import javax.ws.rs.core.Response;
import org.atlas.applications.api.errors.ApiError;

/**
 * Base class for application api custom runtime exceptions.
 * @author ic
 */
public class ApplicationException extends RuntimeException{

    private static final long serialVersionUID = -7498621923421702089L;

    protected Response.Status status;
    protected String appCode;
    protected String details;

    public ApplicationException(ApiError apiError) {
        this.appCode = apiError.getAppCode();
        this.details = apiError.getDetails();
    }

    public ApplicationException(String appCode, String details) {
        this.appCode = appCode;
        this.details = details;
    }

    public ApplicationException(Response.Status status, String appCode) {
        this.status = status;
        this.appCode = appCode;
    }

    public ApplicationException(Response.Status status, String appCode, String details) {
        this.status = status;
        this.appCode = appCode;
        this.details = details;
    }    
    
    public ApplicationException(Response.Status status, ApiError apiError) {
        this.status = status;
        this.appCode = apiError.getAppCode();
        this.details = apiError.getDetails();
    }    

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Response.Status getStatus() {
        return status;
    }

    public void setStatus(Response.Status status) {
        this.status = status;
    }
    
    
}
