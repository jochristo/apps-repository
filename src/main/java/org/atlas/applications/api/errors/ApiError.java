package org.atlas.applications.api.errors;

/**
 * Define system global errors
 * 
 * @author Konstantinos Antonopoulos <a href="mailto:konsantonop@gmail.com">Konstantinos Antonopoulos</a>
 * @version 0.1 
 * category System Core ( API Errors )
 * @since 2016-07-27
 **/
public enum ApiError {
	
	/**
	 * General Purpose Error
	 */
	INTERNAL_SERVER_ERROR("500", "500", "INTERNAL_SERVER_ERROR"),//Something goes wrong please retry.
	
	/**
	 * Invalid Token
	 */
	USER_NOT_FOUND("401","401","USER_NOT_FOUND"),//Customer not found
	
	/**
	 * Wrong Customer credentials
	 */
	USER_WRONG_CREDENTIALS("401", "401", "USER_WRONG_CREDENTIALS"),//Invalid Login Credentials
        
	/**
	 * Wrong Customer credentials
	 */
	INVALID_ACCESS_TOKEN("401", "401","Invalid access token, please signin..."),//Invalid ACCESS TOKEN      
	
	/**
	 * Constrain Violation
	 */
	CONSTRAINT_VIOLATION("1001","1001", "CONSTRAINT_VIOLATION"),
	
	/**
	 * Process already processed, No further processing needed
	 */
	PROCESS_ALREADY_PROCESSED("1002","1002","PROCESS_ALREADY_PROCESSED"),//Already processed
	
	/**
	 * General Resource not found exception
	 */
	RESOURCE_NOT_FOUND("404","404", "RESOURCE_NOT_FOUND"),//Resource not found
	
	/**
	 * Invalid Argument type on URL
	 */
	ILLEGAL_ARGUMENT_TYPE("400","400", "ILLEGAL_ARGUMENT_TYPE"),//Invalid Arguments       
               
	/**
	 * Unable to start session.
	 */
	FORBIDDEN_EXCEPTION("403", "403","FORBIDDEN_EXCEPTION"),//Unable to process request
	
	/**
	 * Data violation
	 */
	DATA_VIOLATION_EXCEPTION("422", "422", "DATA_VIOLATION_EXCEPTION"),//Data violation
	
	/**
	 * Unable to start session.
	 */
	UNABLE_TO_START_SESSION("403", "403", "UNABLE_TO_START_SESSION"),//Unable to start session
	
	/**
	 * Exception on the Media Type of the Request
	 */
        UNSUPPORTED_MEDIA_TYPE("415", "415", "UNSUPPORTED_MEDIA_TYPE"),        
        
	/**
	 * Application id not found exception
	 */
	APPLICATION_NOT_FOUND("404", "2001", "APPLICATION_ID_NOT_FOUND"),//Application not found        
        
	/**
	 * Application is not enabled
	 */
	APPLICATION_NOT_ENABLED("400", "2002", "APPLICATION_NOT_ENABLED"),//Application not enabled         
        
	/**
	 * Application version is not enabled
	 */
	APPLICATION_VERSION_NOT_ENABLED("400", "2003","APPLICATION_VERSION_NOT_ENABLED"),//Application version not enabled                     
        
	/**
	 * Application file exists in repository with the same file name
	 */
        APPLICATION_FILE_EXISTS("400", "2004", "APPLICATION_FILE_EXISTS"),//Invalid Arguments          
        
	/**
	 * Application file does not exist in repository
	 */
        APPLICATION_FILE_NOT_FOUND("400", "2005", "APPLICATION_FILE_NOT_FOUND"),//Invalid Arguments          
        
	/**
	 * Application version not found
	 */
        APPLICATION_VERSION_NOT_FOUND("404", "2006", "APPLICATION_VERSION_NOT_FOUND"),       
        
	/**
	 * Application version exists
	 */
        APPLICATION_VERSION_EXISTS("400", "2007", "APPLICATION_VERSION_EXISTS_CANNOT_UPDATE_CONTENTS"),       
        
	/**
	 * Invalid application version: Application version should be greater than stored version
	 */
        INVALID_APPLICATION_VERSION("400", "2008", "INVALID_APPLICATION_VERSION"),         
        
	/**
	 * Application has no versions - contents
	 */
        APPLICATION_VERSIONS_NOT_PRESENT("400", "2009", "APPLICATION_VERSIONS_NOT_PRESENT"),        
        
	/**
	 * Application name cannot be changed
	 */
        APPLICATION_NAME_UPDATE_DENIED("400", "2010", "APPLICATION_NAME_UPDATE_DENIED"),
        
	/**
	 * Invalid application relationships name
	 */
	INVALID_RELATIONSHIP_PATH("400", "2011", "INVALID_RELATIONSHIP_PATH"),         
        
	/**
	 * bad checksum verification
	 */
        BAD_FILE_CHECKSUM("400", "2012", "BAD_FILE_CHECKSUM")   ,        
        
	/**
	 * ufile parameter is missing
	 */
        MISSING_UFILE_PARAMETER("400", "1001", "MISSING_UFILE_PARAMETER"),//Invalid Arguments          
        
	/**
	 * isPatch parameter is missing: (Indicates if attached ufile param will override existing version file)
	 */
        MISSING_ISPATCH_PARAMETER("400", "1001", "MISSING_IS_PATCH_PARAMETER"),//Invalid Arguments                    
        
	/**
	 * main parameter must not be empty: (isExec param value = true)
	 */
        MISSING_MAIN_PARAMETER("400", "1001", "MISSING_MAIN_PARAMETER"),//Invalid Arguments     
        
	/**
	 * isExec parameter is missing
	 */
        MISSING_ISEXEC_PARAMETER("400", "1001", "MISSING_ISEXEC_PARAMETER"),//Invalid Arguments        
        
	/**
	 * name parameter is missing
	 */
        MISSING_NAME_PARAMETER("400", "1001", "MISSING_NAME_PARAMETER"),//Invalid Arguments     
        
	/**
	 * repository parameter is missing
	 */
        MISSING_REPOSITORY_PARAMETER("400", "1001", "MISSING_REPOSITORY_PARAMETER"),//Invalid Arguments     
        
	/**
	 * ver parameter is missing
	 */
        MISSING_VER_PARAMETER("400", "1001", "MISSING_VER_PARAMETER"),//Invalid Arguments         
	
	/**
	 * Lang parameter is missing
	 */
        MISSING_LANG_PARAMETER("400", "1001", "MISSING_LANG_PARAMETER"),//Invalid Arguments          
	
	/**
	 * Multi-part parameters are missing
	 */
        MISSING_MULTIPART_PARAMETERS("400", "1001", "MISSING_MULTIPART_PARAMETERS"),        
        
	/**
	 * Non-patch parameters are missing: Non-patch parameters are missing: main or lang or name
	 */
        MISSING_NON_PATCH_MULTIPART_PARAMETERS("400", "1001", "MISSING_NON_PATCH_MULTIPART_PARAMETERS"),         
        
        /**
	* isPatch parameter is invalid
	*/
        INVALID_ISPATCH_PARAMETER("400", "1001", "INVALID_ISPATCH_PARAMETER"),//Invalid Arguments           
        
	/**
	 * Repository path exists
	 */
        REPOSITORY_EXISTS("400", "1001", "REPOSITORY_EXISTS"),//Invalid Arguments                                                       
        
	/**
	 * Invalid application file type: Invalid application file type: Only zipped files allowed (.zip)
	 */
        INVALID_APPLICATION_FILE_TYPE("400", "1001", "INVALID_APPLICATION_FILE_TYPE"),
        
	/**
	 * Invalid content type: Only zipped files allowed (application/x-compressed, application/x-zip-compressed, or application/zip)
	 */
        INVALID_CONTENT_TYPE("400", "1001", "INVALID_CONTENT_TYPE")        
        ,
        

        // 20xx Application/Contents related error codes
        // 30xx multipart-related error codes 
        //1001 MULTIPART-RELATED CONSTRAINT VIOLATIONS
                
        ;                 
        
	private final String appCode;
	private final String details;
        private final String httpCode;
	
        /*
	private ApiError(String appCode, String details) {
        this.appCode = appCode;
        this.details = details;
        }
        */
        
	private ApiError(String httpCode, String appCode, String details) {
        this.appCode = appCode;
        this.details = details;
        this.httpCode = httpCode;
        }        
	
	public String getAppCode() {
		return appCode;
	}

	public String getDetails() {
		return details;
	}

        public String getHttpCode() {
            return httpCode;
        }
        
        // CONSTANTS for violation description in annotations
        public static final String MISSING_UFILE_PARAMETER_VIOLATION = "MISSING_UFILE_PARAMETER";
        
        public static final String MISSING_VER_PARAMETER_VIOLATION = "MISSING_VER_PARAMETER";
        
        public static final String INVALID_VER_PARAMETER_VIOLATION = "INVALID_SEMANTIC_VERSIONING_VALUE";
        
        public static final String MISSING_NAME_PARAMETER_VIOLATION = "MISSING_NAME_PARAMETER";
        
        public static final String MISSING_LANG_PARAMETER_VIOLATION = "MISSING_LANG_PARAMETER";
        
        public static final String MISSING_ISEXEC_PARAMETER_VIOLATION = "MISSING_ISEXEC_PARAMETER";
        
        public static final String INVALID_ISEXEC_PARAMETER_VIOLATION = "INVALID_ISEXEC_PARAMETER_VALUE";
        
        public static final String MISSING_MAIN_PARAMETER_VIOLATION = "MISSING_MAIN_PARAMETER";
        
        public static final String MISSING_REPOSITORY_PARAMETER_VIOLATION = "MISSING_REPOSITORY_PARAMETER";
        
        public static final String MISSING_IS_PATCH_PARAMETER_VIOLATION = "MISSING_IS_PATCH_PARAMETER";
        
        public static final String INVALID_IS_PATCH_PARAMETER_VIOLATION = "INVALID_IS_PATCH_PARAMETER_VALUE";
        

                
        
}
