package org.atlas.applications.api;

/**
 *
 * @author ic
 */
public interface RequestParameters {    
    
    public final static String REPOSITORY_PARAM = "repository";
    public final static String NAME_PARAM = "name";
    public final static String MAIN_PARAM = "main";
    public final static String VER_PARAM = "ver";
    public final static String ISEXEC_PARAM = "isExec";
    public final static String LANG_PARAM = "lang";
    public final static String ISBUGFIX_PARAM = "isBugFix";
    public final static String UFILE_PARAM = "ufile";
    public final static String IS_PATCH_PARAM = "isPatch";

    public final static String PARAM_TRUE = "true";
    public final static String PARAM_FALSE = "false";
    
    public final static int APPS_PATH_DEPTH = 2;
}
