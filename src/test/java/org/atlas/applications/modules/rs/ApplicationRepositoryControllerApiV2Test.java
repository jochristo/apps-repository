/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.applications.modules.rs;

import junit.framework.Assert;

/**
 *
 * @author johnny
 */
/**
 * Arquillian will start the container, deploy all @Deployment bundles, then run all the @Test methods.
 *
 * A strong value-add for Arquillian is that the test is abstracted from the server.
 * It is possible to rerun the same test against multiple adapters or server configurations.
 *
 * A second value-add is it is possible to build WebArchives that are slim and trim and therefore
 * isolate the functionality being tested.  This also makes it easier to swap out one implementation
 * of a class for another allowing for easy mocking.
 *
 */
//@RunWith(Arquillian.class)
public class ApplicationRepositoryControllerApiV2Test  extends Assert 
{
    /**
     * ShrinkWrap is used to create a war file on the fly.
     *
     * The API is quite expressive and can build any possible
     * flavor of war file.  It can quite easily return a rebuilt
     * war file as well.
     *
     * More than one @Deployment method is allowed.
     */
    //@Deployment(testable = false)
    //public static WebArchive createDeployment() {
    //    return ShrinkWrap.create(WebArchive.class).addClasses(ApplicationRepositoryControllerApiV2.class, ApplicationHandlerImpl.class, ApplicationResponseService.class);
    //}

    /**
     * This URL will contain the following URL data
     *
     *  - http://<host>:<port>/<webapp>/
     *
     * This allows the test itself to be agnostic of server information or even
     * the name of the webapp
     *
     */
    //@ArquillianResource
    //private URL webappUrl;    
    
    //@Test
    public void getAllApplications() throws Exception {

        //final WebClient webClient = WebClient.create(webappUrl.toURI());
        //webClient.accept(MediaType.APPLICATION_JSON);
        //Response response = webClient.path(ApiEndpoint.API_PATH_V2 +"/applications?filter=all").get();
        //assertEquals(200, 200);
        
    }    
}
