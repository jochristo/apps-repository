/*
Navicat MySQL Data Transfer

Source Server         : mariadb-10.1.21
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_atlas_applications_repository

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-09-18 12:12:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for application
-- ----------------------------
DROP TABLE IF EXISTS `application`;
CREATE TABLE `application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `createdAt` datetime NOT NULL,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `hash_val` char(64) NOT NULL,
  `isCompressed` tinyint(1) NOT NULL,
  `lang` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `repository` varchar(255) NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for application_contents
-- ----------------------------
DROP TABLE IF EXISTS `application_contents`;
CREATE TABLE `application_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `checksum` char(64) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `isBugFree` tinyint(1) NOT NULL DEFAULT '1',
  `isExecutable` tinyint(1) NOT NULL,
  `main` varchar(255) DEFAULT NULL,
  `size` bigint(20) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `storedname` varchar(255) NOT NULL,
  `version` varchar(20) NOT NULL,
  `applicationId` int(11) NOT NULL,
  `mimeTypeId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_application_contents_applicationId` (`applicationId`),
  KEY `FK_application_contents_mimeTypeId` (`mimeTypeId`),
  CONSTRAINT `FK_application_contents_applicationId` FOREIGN KEY (`applicationId`) REFERENCES `application` (`id`),
  CONSTRAINT `FK_application_contents_mimeTypeId` FOREIGN KEY (`mimeTypeId`) REFERENCES `mime_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for application_transactions
-- ----------------------------
DROP TABLE IF EXISTS `application_transactions`;
CREATE TABLE `application_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime NOT NULL,
  `info` varchar(2500) NOT NULL,
  `transactionBy` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(2) NOT NULL,
  `applicationId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_application_transactions_applicationId` (`applicationId`),
  CONSTRAINT `FK_application_transactions_applicationId` FOREIGN KEY (`applicationId`) REFERENCES `application` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for mime_types
-- ----------------------------
DROP TABLE IF EXISTS `mime_types`;
CREATE TABLE `mime_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `encoding` varchar(255) DEFAULT NULL,
  `extension` varchar(255) NOT NULL,
  `mime` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=643 DEFAULT CHARSET=latin1;
